#include "bitset.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*File for bitset operations
 * Union, difference, set, clear, in, get, print, init
 */
BITSET *initBITSET(int totalbits){
    if (totalbits % 8 != 0){
		return 0;
    }
    BITSET *ret = NEW(BITSET);
    ret->size = totalbits;
    ret->bits = (char*) Malloc(totalbits/8);
    memset(ret->bits, '\0', totalbits/8);
    return ret;
}

int clearBITSET(BITSET *set){
	
	memset(set->bits, '\0', set->size/8);
	return 0;
	
}

int setBIT(BITSET *set, int bitPos){
	int cur;
	int offset;
	if(bitPos < 0 || bitPos >= set->size){
		return -1;
	}
	cur = bitPos/8;
	offset = bitPos%8;  
	set->bits[cur] |= (1<<offset);  
	return 0;  
}

int clearBIT(BITSET *set, int bitPos){
	int cur;
	int offset;
	if(bitPos < 0 || bitPos >= set->size){
		return -1;
	}
	cur = bitPos/8;
	offset = bitPos%8;	
	set->bits[cur] &= ~(1<<offset);	
	return 0;
}



int unionBit(BITSET *target, BITSET *other){
	int i;
	int *c = getBits(target);
	int s = 0;
	for(i = 0; i < target->size; i++){
		target->bits[i/8] |= other->bits[i/8];
		if(c[i] != isBitSet(target,i)){
			s = 1;		
		}
	}
	return s;
}

int diffBit(BITSET *target, BITSET *other){
	int i;
	for(i = 0; i < target->size; i++){
		target->bits[i/8] = target->bits[i/8] ^( target->bits[i/8] &  
other->bits[i/8]);
	}	
	return 0;	
}

int isBitSet(BITSET *set, int bitPos){
    int element;
    int offset;
    if (bitPos < 0 || bitPos >= set->size){
		return -1;
	}
	element = bitPos/8;
	offset = bitPos%8;
	if (set->bits[element] & (1<<offset)) {
		return 1;
	} else {
		return 0;
    }
}


int *getBits(BITSET *set){
	int *bits = (int*)Malloc(set->size*sizeof(int));
	int i;
	for(i = 0; i < set->size; i++){
		bits[i] = 0;
		if(isBitSet(set, i)){
			bits[i] = 1;
		}
	}
	return bits;
}

void printBits(BITSET *set){
	int *bits;
	bits = getBits(set);
	int i;
	for(i = 0; i < set->size; i++){
	}
}

int getBitSum(BITSET *set){
	int i;
	int j = 0;
	for(i = 0; i < set->size; i++){
		if(isBitSet(set, i)){
			j++;
		}
	}
	return j;
}


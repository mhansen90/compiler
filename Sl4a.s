        .text
 
form: 
        .string "%d\n" 
 
formFalse: 
        .string "false\n"
 
formTrue: 
        .string "true\n"
 
        .data
 
heap: 
        .long 0
 
retVal: 
        .long 0
 
F0: 
        pushl %ebp 
        movl %esp,%ebp 
        subl $8,%esp 
        movl $2,%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $2,%eax 
        leave 
        ret 
Fend0: 
.globl main 
main: 
        movl %esp,retVal 
        pushl %ebp 
        movl %esp,%ebp 
        movl $4000000,(%esp) 
        call malloc 
        movl %eax,heap 
        movl %ebp,%esp 
        subl $240,%esp 
        movl $0,-4(%ebp) 
        movl $0,-8(%ebp) 
        movl $0,-12(%ebp) 
        movl $0,-16(%ebp) 
        movl $4,-20(%ebp) 
        pushl %eax 
        movl -20(%ebp),%eax 
        movl %eax,-4(%ebp) 
        popl %eax 
        pushl %ebx 
        movl heap,%ebx 
        movl %ebx,-16(%ebp) 
        popl %ebx 
        movl $5,-24(%ebp) 
        cmp $0,-24(%ebp) 
        jg lengthOK1 
        movl $4,%eax 
        movl retVal,%esp 
        ret 
lengthOK1: 
        pushl %eax 
        movl -24(%ebp),%eax 
        movl %eax,-28(%ebp) 
        popl %eax 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl -28(%ebp),%ebx 
        movl %ebx,(%eax) 
        popl %eax 
        addl $1,-28(%ebp) 
        pushl %eax 
        pushl %ebx 
        movl -28(%ebp),%ebx 
        imul $4,%ebx 
        movl %ebx,-28(%ebp) 
        popl %ebx 
        popl %eax 
        pushl %ebx 
        movl -28(%ebp),%ebx 
        addl %ebx,heap 
        popl %ebx 
        movl $2,-32(%ebp) 
        movl $0,-36(%ebp) 
        cmp $0,-36(%ebp) 
        jge indexNotNeg2 
        movl $2,%eax 
        movl retVal,%esp 
        ret 
indexNotNeg2: 
        pushl %eax 
        movl -36(%ebp),%eax 
        movl %eax,-40(%ebp) 
        popl %eax 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-44(%ebp) 
        popl %eax 
        pushl %eax 
        movl -44(%ebp),%eax 
        pushl %ebx 
        movl (%eax),%ebx 
        cmp %ebx,-40(%ebp) 
        popl %ebx 
        popl %eax 
        jl indexOK3 
        movl $2,%eax 
        movl retVal,%esp 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        ret 
indexOK3: 
        pushl %eax 
        movl -36(%ebp),%eax 
        movl %eax,-48(%ebp) 
        popl %eax 
        addl $1,-48(%ebp) 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-52(%ebp) 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        movl -48(%ebp),%ebx 
        imul $4,%ebx 
        movl %ebx,-48(%ebp) 
        popl %ebx 
        popl %eax 
        pushl %eax 
        movl -48(%ebp),%eax 
        addl %eax,-52(%ebp) 
        popl %eax 
        movl -52(%ebp),%eax 
        pushl %eax 
        movl -52(%ebp),%eax 
        movl -32(%ebp),%ebx 
        movl %ebx,(%eax) 
        popl %eax 
        movl $0,-56(%ebp) 
        cmp $0,-56(%ebp) 
        jge indexNotNeg4 
        movl $2,%eax 
        movl retVal,%esp 
        ret 
indexNotNeg4: 
        pushl %eax 
        movl -56(%ebp),%eax 
        movl %eax,-60(%ebp) 
        popl %eax 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-64(%ebp) 
        popl %eax 
        pushl %eax 
        movl -64(%ebp),%eax 
        pushl %ebx 
        movl (%eax),%ebx 
        cmp %ebx,-60(%ebp) 
        popl %ebx 
        popl %eax 
        jl indexOK5 
        movl $2,%eax 
        movl retVal,%esp 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        ret 
indexOK5: 
        pushl %eax 
        movl -56(%ebp),%eax 
        movl %eax,-68(%ebp) 
        popl %eax 
        addl $1,-68(%ebp) 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-72(%ebp) 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        movl -68(%ebp),%ebx 
        imul $4,%ebx 
        movl %ebx,-68(%ebp) 
        popl %ebx 
        popl %eax 
        pushl %eax 
        movl -68(%ebp),%eax 
        addl %eax,-72(%ebp) 
        popl %eax 
        pushl %eax 
        movl -72(%ebp),%eax 
        pushl %ebx 
        movl (%eax),%ebx 
        movl %ebx,-76(%ebp) 
        popl %ebx 
        popl %eax 
        pushl -76(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $3,-80(%ebp) 
        pushl %eax 
        movl -80(%ebp),%eax 
        movl %eax,-12(%ebp) 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        pushl %ecx 
        pushl %edx 
        pushl %ebp 
        call F0 
        addl $4,%esp 
        movl %eax,-84(%ebp) 
        popl %edx 
        popl %ecx 
        popl %ebx 
        popl %eax 
        pushl %eax 
        movl -84(%ebp),%eax 
        movl %eax,-8(%ebp) 
        popl %eax 
        pushl %eax 
        movl -4(%ebp),%eax 
        movl %eax,-88(%ebp) 
        popl %eax 
        pushl -88(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        pushl %eax 
        movl -8(%ebp),%eax 
        movl %eax,-92(%ebp) 
        popl %eax 
        pushl -92(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $3,%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl -12(%ebp),%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $0,%eax 
        leave 
        ret 

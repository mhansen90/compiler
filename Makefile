CC = gcc

CFLAGS = -Wall -ansi -pedantic -g


main:	y.tab.o lex.yy.o main.o tree.h tree.o pretty.h pretty.o error.h \
 error.o collect.h collect.o mem.h mem.o symbol.h symbol.o weed.h weed.o \
 error.h error.o semant.h semant.o type.h type.o \
 assem.h assem.o peep.h peep.o fprint.h fprint.o push.h push.o bitset.h \
 bitset.o live.h live.o prints.h prints.o

	$(CC) lex.yy.o y.tab.o tree.o pretty.o mem.o prints.o main.o error.o \
 collect.o symbol.o weed.o semant.o type.o assem.o peep.o \
 fprint.o push.o bitset.o live.o -o vitaly -lfl

y.tab.c y.tab.h:  bisonfile.y
	          bison -y -d --report=itemset bisonfile.y

lex.yy.c:         flexfile.l y.tab.h 
	          flex flexfile.l


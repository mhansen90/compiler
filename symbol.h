#define HashSize 10

typedef struct SYMBOL {
  char *name;
  struct Ty_ty *type;
  int var;
  int length;
  struct SYMBOL *next;
  struct SymbolTable *table;
  void *address;
} SYMBOL;

typedef struct SymbolTable {
    SYMBOL *table[HashSize];
    struct SymbolTable *next;
    int level;
} SymbolTable;

typedef struct Ty_ty {
  enum {Ty_record, Ty_int, Ty_bool, Ty_error, 
  Ty_string, Ty_array, Ty_name, Ty_void} kind;
  union{
    struct Ty_field *record;
    struct Ty_ty *array;
    struct {SYMBOL *sym; struct Ty_ty *ty;} name;
  }val;	
} Ty_ty;


typedef struct Ty_field {
  char *name; 
  struct Ty_ty *type;
  struct Ty_field *next;
} Ty_field;


int Hash(char *str);

SymbolTable *initSymbolTable();

SymbolTable *scopeSymbolTable(SymbolTable *t);

SYMBOL *putSymbol(SymbolTable *t, char *name, Ty_ty *type, int var);

SYMBOL *getSymbol(SymbolTable *t, char *name);

Ty_ty *getRecord(Ty_field *type, char *name);

int compareTypes(Ty_ty *var, Ty_ty *exp);

int compareRecords(Ty_field *var, Ty_field *exp);

void dumpSymbolTable(SymbolTable *t);


Ty_ty *Ty_Int();
Ty_ty *Ty_Bool();
Ty_ty *Ty_Void();
Ty_ty *Ty_Error();

Ty_ty *Ty_Record(Ty_field *field);
Ty_ty *Ty_Array(Ty_ty *ty);

Ty_ty *Ty_Name(SYMBOL *sym, Ty_ty *ty); 

Ty_field *Ty_Field(char *name, Ty_ty *ty);

#include "mem.h"
#include "tree.h"
#include <string.h>
 
extern int lineno;

int number = 1;

FUNC *makeFUNChbt(HEAD *head, BODY *body, TAIL *tail)
{ FUNC *e;
  e = NEW(FUNC);
  e->lineno = lineno;
  e->head = NEW(HEAD);
  memcpy(e->head, head, sizeof(HEAD));
  e->body = NEW(BODY);
  memcpy(e->body, body, sizeof(BODY));
  e->tail = NEW(TAIL);
  memcpy(e->tail, tail, sizeof(TAIL));
  return e;
}

HEAD *makeHEADfunc(char *id, PARDL *pardl, TYPE *type)
{ HEAD *e;
  e = NEW(HEAD);
  e->lineno = lineno;
  e->id = strdup(id);
  if (pardl == NULL) {
    e->pardl = NULL;
  } else {
  e->pardl = NEW(PARDL);
  memcpy(e->pardl, pardl, sizeof(PARDL));
  }
  e->type = NEW(TYPE);
  memcpy(e->type, type, sizeof(TYPE));
  return e;
}

TAIL *makeTAILend(char *id)
{ TAIL *e;
  e = NEW(TAIL);
  e->lineno = lineno;
  e->id = strdup(id);
  return e;
}

TYPE *makeTYPEid(char *id)
{ TYPE *e;
  e = NEW(TYPE);
  e->lineno = lineno;
  e->kind = TidK;
  e->val.id = strdup(id);
  return e;
}

TYPE *makeTYPEint()
{ TYPE *e;
  e = NEW(TYPE);
  e->lineno = lineno;
  e->kind = TintK;
  return e;
}

TYPE *makeTYPEbool()
{ TYPE *e;
  e = NEW(TYPE);
  e->lineno = lineno;
  e->kind = TboolK;
  return e;
}

TYPE *makeTYPEarray(TYPE *type)
{ TYPE *e;
  e = NEW(TYPE);
  e->lineno = lineno;
  e->kind = TarrayK;
  e->val.type = NEW(TYPE);
  memcpy(e->val.type, type, sizeof(TYPE));
  return e;
}

TYPE *makeTYPErecord(VARDL *vardl)
{ TYPE *e;
  e = NEW(TYPE);
  e->lineno = lineno;
  e->kind = TrecordK;
  e->val.vardl = NEW(VARDL);
  memcpy(e->val.vardl, vardl, sizeof(VARDL));
  return e;
}

PARDL *makePARDLvardl(VARDL *vardl)
{ PARDL *e;
  e = NEW(PARDL);
  e->lineno = lineno;
  e->vardl = NEW(VARDL);
  memcpy(e->vardl, vardl, sizeof(VARDL));
  return e;
}

VARDL *makeVARDLvardl(VARDL *vardl, VART *vart)
{ VARDL *e;
  e = NEW(VARDL);
  e->lineno = lineno;
  e->kind = varcl_vartK;
  e->val.s.vardl = NEW(VARDL);
  memcpy(e->val.s.vardl, vardl, sizeof(VARDL));
  e->val.s.vart = NEW(VART);
  memcpy(e->val.s.vart, vart, sizeof(VART));
  return e;
}

VARDL *makeVARDLvart(VART *vart)
{ VARDL *e;
  e = NEW(VARDL);
  e->lineno = lineno;
  e->kind = vartK;
  e->val.vart = NEW(VART);
  memcpy(e->val.vart, vart, sizeof(VART));
  return e;
}

VART *makeVARTid(char *id, TYPE *type)
{ VART *e;
  e = NEW(VART);
  e->lineno = lineno;
  e->id = strdup(id);
  e->type = NEW(TYPE);
  memcpy(e->type, type, sizeof(TYPE));
  return e;
}

BODY *makeBODYds(DECL *decll, STATEL *statel)
{ BODY *e;
  e = NEW(BODY);
  e->lineno = lineno;
  if (decll == NULL) {
    e->decll = NULL;
  } else {
    e->decll = decll;
  }
  e->statel = NEW(STATEL);
  memcpy(e->statel, statel, sizeof(STATEL));
  return e;
}

DECL *makeDECLLdecll(DECL *decll, DECL *decl)
{ 
  if (decll == NULL){
    return decl;
  } else {
    decl->next = decll;
	return decl;
  }
}

DECL *makeDECLtype(char *id, TYPE *type)
{ DECL *e;
  e = NEW(DECL);
  e->lineno = lineno;
  e->kind = typeK;
  e->val.s.id = strdup(id);
  e->val.s.type = NEW(TYPE);
  memcpy(e->val.s.type, type, sizeof(TYPE));
  e->next = NULL;
  return e;
}

DECL *makeDECLfunc(FUNC *func)
{ DECL *e;
  e = NEW(DECL);
  e->lineno = lineno;
  e->kind = funcK;
  e->val.func = NEW(FUNC);
  memcpy(e->val.func, func, sizeof(FUNC));
  e->next = NULL;
  return e;
}

DECL *makeDECLvar(VARDL *vardl)
{ DECL *e;
  e = NEW(DECL);
  e->lineno = lineno;
  e->kind = varK;
  e->val.vardl = NEW(VARDL);
  memcpy(e->val.vardl, vardl, sizeof(VARDL));
  e->next = NULL;
  return e;
}

STATEL *makeSTATELstate(STATE *state)
{ STATEL *e;
  e = NEW(STATEL);
  e->lineno = lineno;
  e->kind = stateK;
  e->val.state = NEW(STATE);
  memcpy(e->val.state, state, sizeof(STATE));
  return e;
}

STATEL *makeSTATELstatel(STATEL *statel, STATE *state)
{ STATEL *e;
  e = NEW(STATEL);
  e->lineno = lineno;
  e->kind = statel_stateK;
  e->val.s.statel = NEW(STATEL);
  memcpy(e->val.s.statel, statel, sizeof(STATEL));
  e->val.s.state = NEW(STATE);
  memcpy(e->val.s.state, state, sizeof(STATE));
  return e;
}
STATE *makeSTATEreturn(EXP *exp)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = returnK;
  e->val.exp = NEW(EXP);
  memcpy(e->val.exp, exp, sizeof(EXP));
  return e;
}

STATE *makeSTATEwrite(EXP *exp)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = writeK;
  e->val.exp = NEW(EXP);
  memcpy(e->val.exp, exp, sizeof(EXP));
  return e;
}

STATE *makeSTATEallocate(VARI *vari, OPTL *optl)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = allocateK;
  e->val.a.vari = NEW(VARI);
  memcpy(e->val.a.vari, vari, sizeof(VARI));
  if (optl == NULL) {
    e->val.a.optl = NULL;
  } else {
  e->val.a.optl = NEW(OPTL);
  memcpy(e->val.a.optl, optl, sizeof(OPTL));
  }
  return e;
}

STATE *makeSTATEvari_exp(VARI *vari, EXP *exp)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = equalK;
  e->val.e.vari = NEW(VARI);
  memcpy(e->val.e.vari, vari, sizeof(VARI));
  e->val.e.exp = NEW(EXP);
  memcpy(e->val.e.exp, exp, sizeof(EXP));
  return e;
}

STATE *makeSTATEif_then(EXP *exp, STATE *state, OPTE *opte)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = ifK;
  e->val.i.exp = NEW(EXP);
  memcpy(e->val.i.exp, exp, sizeof(EXP));
  e->val.i.state = NEW(STATE);
  memcpy(e->val.i.state, state, sizeof(STATE));
  if (opte == NULL) {
    e->val.i.opte = NULL;
  } else {
    e->val.i.opte = NEW(OPTE);
    memcpy(e->val.i.opte, opte, sizeof(OPTE));
  }
  return e;
}

STATE *makeSTATEwhile_do(EXP *exp, STATE *state)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = whileK;
  e->val.w.exp = NEW(EXP);
  memcpy(e->val.w.exp, exp, sizeof(EXP));
  e->val.w.state = NEW(STATE);
  memcpy(e->val.w.state, state, sizeof(STATE));
  return e;
}

STATE *makeSTATEstatel(STATEL *statel)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = statelK;
  e->val.statel = NEW(STATEL);
  memcpy(e->val.statel, statel, sizeof(STATEL));
  return e;
}

STATE *makeSTATEbreak()
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = breakK;
  return e;
}

STATE *makeSTATEcontinue()
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = continueK;
  return e;
}

STATE *makeSTATEfor(VART *vart, STATE *state1,
EXP *exp, STATE *state2, STATE *state3)
{ STATE *e;
  e = NEW(STATE);
  e->lineno = lineno;
  e->kind = forK;
  e->val.f.vart = NEW(STATE);
  memcpy(e->val.f.vart, vart, sizeof(STATE));
  e->val.f.state1 = NEW(STATE);
  memcpy(e->val.f.state1, state1, sizeof(STATE));
  e->val.f.exp = NEW(EXP);
  memcpy(e->val.f.exp, exp, sizeof(EXP));
  e->val.f.state2 = NEW(STATE);
  memcpy(e->val.f.state2, state2, sizeof(STATE));
  e->val.f.state3 = NEW(STATE);
  memcpy(e->val.f.state3, state3, sizeof(STATE));
  return e;
}

STATE *makeSTATEinc(VARI *vari) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = incK;
	e->val.vari = NEW(VARI);
	memcpy(e->val.vari, vari, sizeof(VARI));	
	return e;
}


STATE *makeSTATEdec(VARI *vari) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = decK;
	e->val.vari = NEW(VARI);
	memcpy(e->val.vari, vari, sizeof(VARI));	
	return e;
}

STATE *makeSTATEsplus(VARI *vari, EXP *exp) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = equalK;
	e->val.e.vari = NEW(VARI);
	memcpy(e->val.e.vari, vari, sizeof(VARI));
	e->val.e.exp = NEW(EXP);
	e->val.e.exp->kind = plusK;
	e->val.e.exp->val.s.right = NEW(EXP);
	memcpy(e->val.e.exp->val.s.right, exp, sizeof(EXP));
	e->val.e.exp->val.s.left = NEW(EXP);
	e->val.e.exp->val.s.left->kind = termK;
	e->val.e.exp->val.s.left->val.term = NEW(TERM);
	e->val.e.exp->val.s.left->val.term->kind = variK;
	e->val.e.exp->val.s.left->val.term->val.vari = NEW(VARI);
	memcpy(e->val.e.exp->val.s.left->val.term->val.vari, vari, sizeof(VARI));
	return e;
}

STATE *makeSTATEsminus(VARI *vari, EXP *exp) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = equalK;
	e->val.e.vari = NEW(VARI);
	memcpy(e->val.e.vari, vari, sizeof(VARI));
	e->val.e.exp = NEW(EXP);
	e->val.e.exp->kind = minusK;
	e->val.e.exp->val.s.right = NEW(EXP);
	memcpy(e->val.e.exp->val.s.right, exp, sizeof(EXP));
	e->val.e.exp->val.s.left = NEW(EXP);
	e->val.e.exp->val.s.left->kind = termK;
	e->val.e.exp->val.s.left->val.term = NEW(TERM);
	e->val.e.exp->val.s.left->val.term->kind = variK;
	e->val.e.exp->val.s.left->val.term->val.vari = NEW(VARI);
	memcpy(e->val.e.exp->val.s.left->val.term->val.vari, vari, sizeof(VARI));
	return e;
}

STATE *makeSTATEstimes(VARI *vari, EXP *exp) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = equalK;
	e->val.e.vari = NEW(VARI);
	memcpy(e->val.e.vari, vari, sizeof(VARI));
	e->val.e.exp = NEW(EXP);
	e->val.e.exp->kind = timesK;
	e->val.e.exp->val.s.right = NEW(EXP);
	memcpy(e->val.e.exp->val.s.right, exp, sizeof(EXP));
	e->val.e.exp->val.s.left = NEW(EXP);
	e->val.e.exp->val.s.left->kind = termK;
	e->val.e.exp->val.s.left->val.term = NEW(TERM);
	e->val.e.exp->val.s.left->val.term->kind = variK;
	e->val.e.exp->val.s.left->val.term->val.vari = NEW(VARI);
	memcpy(e->val.e.exp->val.s.left->val.term->val.vari, vari, sizeof(VARI));
	return e;
}

STATE *makeSTATEsdiv(VARI *vari, EXP *exp) {
	STATE *e;
	e = NEW(STATE);
	e->lineno = lineno;
	e->kind = equalK;
	e->val.e.vari = NEW(VARI);
	memcpy(e->val.e.vari, vari, sizeof(VARI));
	e->val.e.exp = NEW(EXP);
	e->val.e.exp->kind = divK;
	e->val.e.exp->val.s.right = NEW(EXP);
	memcpy(e->val.e.exp->val.s.right, exp, sizeof(EXP));
	e->val.e.exp->val.s.left = NEW(EXP);
	e->val.e.exp->val.s.left->kind = termK;
	e->val.e.exp->val.s.left->val.term = NEW(TERM);
	e->val.e.exp->val.s.left->val.term->kind = variK;
	e->val.e.exp->val.s.left->val.term->val.vari = NEW(VARI);
	memcpy(e->val.e.exp->val.s.left->val.term->val.vari, vari, sizeof(VARI));
	return e;
}

OPTL *makeOPTLlength(EXP *exp)
{ OPTL *e;
  e = NEW(OPTL);
  e->lineno = lineno;
  e->exp = NEW(EXP);
  memcpy(e->exp, exp, sizeof(EXP));
  return e;
}
OPTE *makeOPTEelse(STATE *state)
{ OPTE *e;
  e = NEW(OPTE);
  e->lineno = lineno;
  e->state = NEW(STATE);
  memcpy(e->state, state, sizeof(STATE));
  return e;
}
VARI *makeVARIid(char *id)
{ VARI *e;
  e = NEW(VARI);
  e->lineno = lineno;
  e->kind = idK;
  e->val.id = strdup(id);
  return e;
}

VARI *makeVARIvari_exp(VARI *vari, EXP *exp)
{ VARI *e;
  e = NEW(VARI);
  e->lineno = lineno;
  e->kind = vari_expK;
  e->val.e.vari = NEW(VARI);
  memcpy(e->val.e.vari, vari, sizeof(VARI));
  e->val.e.exp = NEW(EXP);
  memcpy(e->val.e.exp, exp, sizeof(EXP));
  return e;
}

VARI *makeVARIvari_id(VARI *vari, char *id)
{ VARI *e;
  e = NEW(VARI);
  e->lineno = lineno;
  e->kind = vari_idK;
  e->val.i.vari = NEW(VARI);
  memcpy(e->val.i.vari, vari, sizeof(VARI));
  e->val.i.id = strdup(id);
  return e;
}
EXP *makeEXPterm(TERM *term)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = termK;
  e->val.term = NEW(TERM);
  memcpy(e->val.term, term, sizeof(TERM)); 
  return e;
}

EXP *makeEXPtimes(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = timesK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPdiv(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = divK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPplus(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = plusK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPminus(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = minusK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPcmp(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = cmpK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}


EXP *makeEXPncmp(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = ncmpK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPhigh(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = highK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPlow(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = lowK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPhigheq(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = higheqK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPloweq(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = loweqK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPand(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = andK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

EXP *makeEXPor(EXP *left, EXP *right)
{ EXP *e;
  e = NEW(EXP);
  e->lineno = lineno;
  e->kind = orK;
  e->val.s.left = NEW(EXP);
  memcpy(e->val.s.left, left, sizeof(EXP));
  e->val.s.right = NEW(EXP);
  memcpy(e->val.s.right, right, sizeof(EXP));
  return e;
}

TERM *makeTERMid_act(char *id, ALIST *alist)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = id_actK;
  e->val.s.id = strdup(id);
  if (alist == NULL) {
    e->val.s.alist = NULL;
  } else {
  e->val.s.alist = NEW(ALIST);
  memcpy(e->val.s.alist, alist, sizeof(ALIST));
  }
  return e;
}

TERM *makeTERMnull()
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = nullK;
  return e;
}

TERM *makeTERMnot(TERM *term)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = notK;
  e->val.term = NEW(TERM);
  memcpy(e->val.term, term, sizeof(TERM)); 
  return e;
}

TERM *makeTERMp(EXP *exp)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = parK;
  e->val.exp = NEW(EXP);
  memcpy(e->val.exp, exp, sizeof(EXP)); 
  return e;
}

TERM *makeTERMb(EXP *exp)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = barK;
  e->val.exp = NEW(EXP);
  memcpy(e->val.exp, exp, sizeof(EXP)); 
  return e;
}

TERM *makeTERMnumconst(int numconst)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = numconstK;
  e->val.num = numconst;
  return e;
}

TERM *makeTERMbooleanconst(int booleanconst)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = booleanconstK;
  e->val.num = booleanconst;
  return e;
}

TERM *makeTERMvari(VARI *vari)
{ TERM *e;
  e = NEW(TERM);
  e->lineno = lineno;
  e->kind = variK;
  e->val.vari = NEW(VARI);
  memcpy(e->val.vari, vari, sizeof(VARI)); 
  return e;
}

ALIST *makeALISTelist(ELIST *elist) { 
  ALIST *e;
  e = NEW(ALIST);
  e->lineno = lineno;
  e->elist=NEW(ELIST);
  memcpy(e->elist, elist, sizeof(ELIST));
  return e;
}

ELIST *makeELISTexp(EXP *exp) {
  ELIST *e;
  e = NEW(ELIST);
  e->lineno = lineno;
  e->kind = expK;
  e->val.exp=NEW(EXP);
  memcpy(e->val.exp, exp, sizeof(EXP));
  return e;
}

ELIST *makeELISTelist(ELIST *elist, EXP *exp) { 
  ELIST *e;
  e = NEW(ELIST);
  e->lineno = lineno;
  e->kind = elist_expK;
  e->val.s.elist = NEW(ELIST);
  memcpy(e->val.s.elist, elist, sizeof(ELIST));
  e->val.s.exp = NEW(EXP);
  memcpy(e->val.s.exp, exp, sizeof(EXP));
  return e;
}

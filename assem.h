#ifndef ASSEM_H
#define ASSEM_H
#include "tree.h"
#include "error.h"
#include "mem.h"
#include <stdio.h>
#include <string.h>
#include "bitset.h"

typedef struct ASSEM{
	enum {CallK,MovK,DivK,MulK,AddK,SubK,CmpK,NotK,AndK,OrK,XOrK,LShiftK,
RShiftK,JmpK,JmpleqK,JmpgrqK,JmpleK,JmpgrK,JmpneqK,JmpeqK,LblK,PushK,PopK,
RetK,LeaveK,MainK,IncK,DecK} kind;
  struct ASSEM *next; 
  struct ASSEM *nextFail;
  struct ASSEM *prevFail;
  struct VAL *left;
  struct VAL *right;
  struct LABEL *label; 
  struct ASSEM *previous;
  struct BITSET *succ;
  struct BITSET *pred;
  struct BITSET *in;
  struct BITSET *out;
  struct BITSET *use;
  struct BITSET *def;
  int block;
  int numTMP;
  int args;
} ASSEM;

typedef struct REG{
	enum {eaxK,ebxK,ecxK,edxK,espK,ebpK,esiK,ediK}kind;
	int offset;
	int isOffset;
}REG;


typedef struct VAL{
	enum {ConstK,RegK, LabelK,RRegK,NewlineK,ScopeK,TrueK,FalseK}kind;
	int Const;
	struct Temp_o *reg;
	struct LABEL *label;
	struct REG *rreg;
	struct SYMBOL *s;
	int def;
}VAL;


typedef struct LABEL{
	char *name;
	int number;
} LABEL;

typedef struct Temp_o{
  enum{EaxK, EbxK, EcxK, NpreK}precolor;
	int number;
	int deref;
} Temp_o;

typedef struct HEAPCONTROL{
	int next_offset;
	VAL *ourHeapStart;
} HEAPCONTROL;

ASSEM *assemINIT(BODY *e, SymbolTable *sym);
ASSEM *assemFUNC(FUNC *e,SymbolTable *sym);
ASSEM *assemHEAD(HEAD *e,SymbolTable *sym);
ASSEM *assemTAIL(TAIL *e,SymbolTable *sym);
ASSEM *assemTYPE(TYPE *e,SymbolTable *sym);
ASSEM *assemPARDL(PARDL *e,SymbolTable *sym);
ASSEM *assemVARDL(VARDL *e,SymbolTable *sym);
ASSEM *assemVARDLfunc(VARDL *e, SymbolTable *sym);
ASSEM *assemVART(VART *e,SymbolTable *sym);
ASSEM *assemVARTfunc(VART *e,SymbolTable *sym);
ASSEM *assemBODY(BODY *e,SymbolTable *sym);
ASSEM *assemDECL(DECL *e,SymbolTable *sym);
ASSEM *assemSTATEL(STATEL *e,SymbolTable *sym, LABEL *n1, LABEL *n2);
ASSEM *assemSTATE(STATE *e,SymbolTable *sym, LABEL *n1, LABEL *n2);
ASSEM *assemDECLfunc(DECL *e,SymbolTable *sym);
ASSEM *assemOPTL(OPTL *e,SymbolTable *sym);
ASSEM *assemOPTE(OPTE *e,SymbolTable *sym, LABEL *n1, LABEL *n2);
ASSEM *assemVARI(VARI *e,SymbolTable *sym);
ASSEM *assemEXP(EXP *e,SymbolTable *sym);
ASSEM *assemTERM(TERM *e,SymbolTable *sym);
ASSEM *assemALIST(ALIST *e,SymbolTable *sym);
ASSEM *assemELIST(ELIST *e,SymbolTable *sym);
ASSEM *createFUNClabel(DECL *e, SymbolTable *sym);


ASSEM *makeCALL( VAL *left);
ASSEM *makeMOV( VAL *left, VAL *right );
ASSEM *makeDIV( VAL *left);
ASSEM *makeMUL( VAL *left, VAL *right );
ASSEM *makeADD( VAL *left, VAL *right );
ASSEM *makeSUB( VAL *left, VAL *right );
ASSEM *makeCMP( VAL *left, VAL *right );
ASSEM *makePRNT( VAL *left, VAL *right );
ASSEM *makeNOT( VAL *left);
ASSEM *makeAND( VAL *left, VAL *right );
ASSEM *makeOR( VAL *left, VAL *right );
ASSEM *makeXOR( VAL *left, VAL *right);
ASSEM *makeLSHIFT( VAL *left, VAL *right);
ASSEM *makeRSHIFT( VAL *left, VAL *right);
ASSEM *makeRET();
ASSEM *makeLEAVE();
ASSEM *makeINC(VAL *left);
ASSEM *makeDEC(VAL *left);

ASSEM *makeJMP(  );
ASSEM *makeJMPLEQ(  );	
ASSEM *makeJMPGRQ(  );	
ASSEM *makeJMPLE(  );	
ASSEM *makeJMPGR(  );	
ASSEM *makeJMPNEQ(  );	
ASSEM *makeJMPEQ(  );	

ASSEM *makeEMPTY(LABEL *lb);

ASSEM *makePUSH( VAL *left);
ASSEM *makePOP( VAL *left);

ASSEM *makeMain();


VAL *makeVALconst( int Const );
VAL *makeVALreg( );
VAL *makeVALlabel(LABEL *label);
VAL *makeVALnewline();
VAL *makeVALfalse();
VAL *makeVALtrue();
VAL *makeVALrreg( );
VAL *makeVALheap( );
VAL *makeVALfindheap(SYMBOL *s);
Temp_o *makeTEMP( );
LABEL *makeLABEL(char *name);
LABEL *makeLABELend(char *name, int num);
LABEL *makePRINT();
LABEL *makeMALLOC();

void makelinked(ASSEM *assem);
REG *makeREG(int off);

#endif 

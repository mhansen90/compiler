#include "assem.h"
ASSEM *peepAndDestroy(ASSEM *assem);
int checkAssem(ASSEM *assem);
int removeAssem(ASSEM *assem, int i);
int checkSameVal(VAL *left, VAL *right);
VAL *copyVal(VAL *val);
int checkNeeded(ASSEM *assem);
int alreadyInReg(ASSEM *assem, ASSEM *next);

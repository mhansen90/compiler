#include "tree.h"

void prettyFUNC(FUNC *e);
void prettyHEAD(HEAD *e);
void prettyTAIL(TAIL *e);
void prettyTYPE(TYPE *e);

void prettyPARDL(PARDL *e);
void prettyVARDL(VARDL *e);
void prettyVART(VART *e);
void prettyBODY(BODY *e);

void prettyDECL(DECL *e);

void prettySTATEL(STATEL *e);
void prettySTATE(STATE *e);

void prettyOPTL(OPTL *e);
void prettyOPTE(OPTE *e);
void prettyVARI(VARI *e);

void prettyEXP(EXP *e);
void prettyTERM(TERM *e);
void prettyALIST(ALIST *e);
void prettyELIST(ELIST *e);







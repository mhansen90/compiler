#include <stdio.h>
#include "pretty.h"

int indent = 0;

void prettyFUNC(FUNC *e){
    prettyHEAD(e->head);
	indent++;
    prettyBODY(e->body);
	indent--;
    prettyTAIL(e->tail);
}
void prettyHEAD(HEAD *e){
    printf("func %s ( ", e->id);
	if (e->pardl != NULL) {
      prettyPARDL(e->pardl);
	}
    printf(" ) :");
    prettyTYPE(e->type);
}
void prettyTAIL(TAIL *e){
    printf("end %s \n", e->id);
}
void prettyTYPE(TYPE *e){
switch (e->kind) {
    case TidK:
         printf(" %s", e->val.id);
         break;
    case TintK:
         printf(" int" );
         break;
    case TboolK:
         printf(" bool");
         break;
    case TarrayK:
         printf("[");
         prettyTYPE(e->val.type);
         printf(" ]");
         break;
    case TrecordK:
         printf(" record of {");
         prettyVARDL(e->val.vardl);
         printf(" }");
         break;
    }
}

void prettyPARDL(PARDL *e){
    prettyVARDL(e->vardl);
}
void prettyVARDL(VARDL *e){
switch (e->kind) {
    case vartK:
         prettyVART(e->val.vart);
         break;
    case varcl_vartK:
         prettyVARDL(e->val.s.vardl);
		 printf(", ");
         prettyVART(e->val.s.vart);
         break;
    }
}
void prettyVART(VART *e){
    printf("%s :", e->id);
    prettyTYPE(e->type);
}
void prettyBODY(BODY *e){
    printf("\n");
    if (e->decll != NULL){        
        prettyDECL(e->decll);
    }
    prettySTATEL(e->statel);
}
void prettyDECL(DECL *e){
    if (e->next != NULL){
         prettyDECL(e->next);
         }
switch (e->kind) {
	int m;
    case typeK:
         printf("type %s =", e->val.s.id); 
         prettyTYPE(e->val.s.type);
         printf(";\n");
         break;
    case funcK:
         prettyFUNC(e->val.func);
         break;
    case varK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("var ");
         prettyVARDL(e->val.vardl);
         printf(";\n");
         break;
    }
}
void prettySTATEL(STATEL *e){
switch (e->kind) {
    case statel_stateK: 
         prettySTATEL(e->val.s.statel);
         prettySTATE(e->val.s.state);
         break;
    case stateK:
         prettySTATE(e->val.state);
         break;
    }
}
void prettySTATE(STATE *e){
	int m;
switch (e->kind) {
    case returnK: ;
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
        printf("return");
        prettyEXP(e->val.exp);
        printf(";\n");
		break;
    case writeK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("write");
         prettyEXP(e->val.exp);
         printf(";\n");
         break;
    case allocateK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("allocate ");
         prettyVARI(e->val.a.vari);
	 if (e->val.a.optl != NULL) {
         	prettyOPTL(e->val.a.optl);
	 }
         printf(";\n");
         break;
    case equalK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         prettyVARI(e->val.e.vari);
         printf(" =");
         prettyEXP(e->val.e.exp);
         printf(";\n");
         break;
    case ifK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("if(");
         prettyEXP(e->val.i.exp);
		 printf(" )");
		 printf(" then \n");
		 indent++;
         prettySTATE(e->val.i.state);
		 indent--;
         if (e->val.i.opte != NULL){
            prettyOPTE(e->val.i.opte);
         }
         break;
    case whileK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("while(");
         prettyEXP(e->val.w.exp);
		 printf(" ) ");
         printf("do\n");
		 indent++;
         prettySTATE(e->val.w.state);
		 indent--;
         break;
    case statelK:
		for ( m = 0;m < indent;m++){
			printf("    ");
		}
         printf("{\n");
         prettySTATEL(e->val.statel);
		 for ( m = 0;m < indent;m++){
			 printf("    ");
		 }
         printf("}\n");
         break;
    }
}

void prettyOPTL(OPTL *e){
    printf(" of length");
    prettyEXP(e->exp);
}

void prettyOPTE(OPTE *e){
	int m;
    for ( m = 0;m < indent;m++){
		printf("    ");
	}
    printf(" else\n");
	indent++;
    prettySTATE(e->state);
	indent--;
}

void prettyVARI(VARI *e){
switch (e->kind) {
    case idK:
         printf("%s", e->val.id);
         break;
    case vari_expK:
         prettyVARI(e->val.e.vari);
         printf("[");
         prettyEXP(e->val.e.exp);
         printf(" ]");
         break;
    case vari_idK:
         prettyVARI(e->val.i.vari);
         printf(".");
         printf("%s", e->val.i.id);
         break;
    }
}

void prettyEXP(EXP *e){
 switch (e->type->kind) {
    case Ty_record:
         printf(" record");
         break;
    case Ty_int:
         printf(" Int");
         break; 
    case Ty_bool:
         printf(" Bool");
         break;    
    case Ty_error:
         printf(" NULL");
         break;
    case Ty_array:
         printf(" Array");
         break;
    case Ty_void:
         printf(" Void");
         break; 
  } 
} 
void prettyTERM(TERM *e){
  switch (e->kind) {
    case variK:
        prettyVARI(e->val.vari);
        break;
    case id_actK:
        printf("%s", e->val.s.id);
        printf("(");
		if (e->val.s.alist != NULL){
			prettyALIST(e->val.s.alist);
		}
        printf(")");
        break;
    case numconstK: 
        printf("%d", e->val.num);
        break;
    case booleanconstK:
	if (e->val.num==1) {
		printf("true"); 
	} else {
		printf("false");
	}

        break; 
    case nullK:
        printf("null");
        break; 
    case notK:
        printf("!");
        prettyTERM(e->val.term);
        break;
    case parK:
        printf("(");
        prettyEXP(e->val.exp);
        printf(")");
        break;
    case barK:
        printf("|");
        prettyEXP(e->val.exp);
        printf("|");
        break;
  }
}
void prettyALIST(ALIST *e){

    prettyELIST(e->elist);
}
void prettyELIST(ELIST *e){
  switch (e->kind) {
    case expK:
        prettyEXP(e->val.exp);
        break;
    case elist_expK: 
        prettyELIST(e->val.s.elist);
        printf(",");
        prettyEXP(e->val.s.exp);
        break;
  }
}


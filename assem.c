#include "fprint.h"


ASSEM *current;
int currentnumber;
int labelnumber;
int allocnum;
int parnum;
VAL *heap;
VAL *retV;
Ty_field *myGlobalField;
Ty_ty *myGlobalArray;


/*
 * assemINIT is the method that, as well as serving as our initial 
 * BODY in grammar, initializes several necessary components of our
 * intermediate representation. Fx: storing basepointer, allocating
 * approximately 4MB of heapspace, letting main scope functions be
 * declared first and making sure that the return value of the 
 * program input is returned to the OS.
 */
ASSEM *assemINIT(BODY *e, SymbolTable *sym) {	 
	heap = makeVALlabel(makeLABELend("heap", -1));
	retV = makeVALlabel(makeLABELend("retVal", -1));
	ASSEM *start = NEW(ASSEM);
	current = start;
	currentnumber =  0;
	labelnumber = 0;
	allocnum = 4000000;
	
	if(e->decll != NULL){
		createFUNClabel(e->decll, sym);
		assemDECLfunc(e->decll, sym);
	}
	makeMain();
	
	VAL *myEbp = makeVALrreg();
	myEbp->rreg->kind = ebpK;
	VAL *myEsp = makeVALrreg();
	myEsp->rreg->kind = espK;
	makeMOV(myEsp, retV);
	ASSEM *st = makePUSH(myEbp);
	makeMOV(myEsp,myEbp);
	st->block = 1;
	
	VAL *absoluteESP = makeVALrreg();
	absoluteESP->rreg->kind = espK;
	absoluteESP->rreg->isOffset = 1;
	makeMOV(makeVALconst(allocnum),absoluteESP);
	makeCALL(makeVALlabel(makeMALLOC()));
	
	VAL *absoluteEAX = makeVALreg();
	absoluteEAX->reg->precolor = EaxK;
	
	makeMOV(absoluteEAX,heap);
	makeMOV(myEbp,myEsp);	
	ASSEM *alloc = makeSUB(NULL,makeVALrreg());
	
	if (e->decll != NULL){        
		assemDECL(e->decll, sym);
	}
	
	alloc->right->rreg->kind = espK;
	ASSEM *lastAssem = assemSTATEL(e->statel, sym, NULL, NULL);
	lastAssem->block = -1;
	alloc->left = makeVALconst(currentnumber*4);
	start->next->numTMP = currentnumber;
	ASSEM *ret = makeMOV(makeVALconst(0),makeVALrreg());
	ret->right->rreg->kind = eaxK;
	makeLEAVE();
	makeRET();
	
	
	return start->next;
}
/*
 * In assemFUNC, we retrive the function in the symbol table in order to
 * get its label, and then create the instruction for the label.
 * 
 * Prior to entering the body of the function, we make sure to retrieve and
 * construct all functions we have declared within ourselves.
 */
ASSEM *assemFUNC(FUNC *e, SymbolTable *sym){
	SymbolTable *table = e->table;
	SYMBOL *s = getSymbol(table, e->head->id);
	VAL *v = (VAL*)s->address;
	LABEL *l ;
	if(v != NULL){
		l= v->label;
	}
	assemHEAD(e->head, table);
	if(e->body->decll != NULL){
		createFUNClabel(e->body->decll, table);
		assemDECLfunc(e->body->decll, sym);
	}
	
	makeEMPTY(l);
	assemBODY(e->body, table);
	return assemTAIL(e->tail, table);
}
ASSEM *assemHEAD(HEAD *e, SymbolTable *sym){
	if (e->pardl != NULL) {
		assemPARDL(e->pardl, sym);
	}
	assemTYPE(e->type, sym);
}
/*
 * In assemTAIL we make the label that marks the end of the function.
 */
ASSEM *assemTAIL(TAIL *e, SymbolTable *sym){
	SYMBOL *s = getSymbol(sym, e->id);
	VAL *l = (VAL*)s->address;	
	char *f = "Fend";
	LABEL *ff = makeLABELend(f, l->label->number);
	return makeEMPTY(ff);
}
ASSEM *assemTYPE(TYPE *e, SymbolTable *sym){

	switch (e->kind) {
		case TidK:
			break;
		case TintK:
			break;
		case TboolK:
			break;
		case TarrayK:
			assemTYPE(e->val.type, sym);
			break;
		case TrecordK:
			break;
	}
}

/*All parameters will be at a known offset from the function.
 * Within assemVARTfunc we save these offsets as VALs in the
 * symbols representing the parameters.
 */
ASSEM *assemPARDL(PARDL *e, SymbolTable *sym){
	parnum = e->length+2;
	return assemVARDLfunc(e->vardl, sym);
}

ASSEM *assemVARDL(VARDL *e, SymbolTable *sym){
	ASSEM *assem ;
	switch (e->kind) {
		case vartK:
			assem = assemVART(e->val.vart, sym);
			return assem;
		case varcl_vartK:
			assemVARDL(e->val.s.vardl, sym);
			return assemVART(e->val.s.vart, sym);
	}		  
}
ASSEM *assemVARTfunc(VART *e, SymbolTable *sym){
	SYMBOL *s = getSymbol(sym, e->id);
	VAL *l = makeVALrreg();
	s->address = l;
	l->rreg->kind = ebpK;
	l->rreg->offset = (parnum)*4;
	parnum = parnum-1;
	return makeMOV(makeVALconst(0),l );
}

ASSEM *assemVARDLfunc(VARDL *e, SymbolTable *sym){
  ASSEM *assem ;
	switch (e->kind) {
		case vartK:
			assem = assemVARTfunc(e->val.vart, sym);
			current = current->previous;
			return assem;
		case varcl_vartK:
			assemVARDLfunc(e->val.s.vardl, sym);
			assem = assemVARTfunc(e->val.s.vart, sym);
			current = current->previous;
			return assem;
	}		  
}
/*
 * In assemVART we initialize non-parameter variables by making a
 * temporary which will have its offset from base pointer stored
 * in the SYMBOL in our SymbolTable structure. Whenever we use this
 * variable we can then retrieve this temporary and use it.
 */
ASSEM *assemVART(VART *e, SymbolTable *sym){
	SYMBOL *s = getSymbol(sym, e->id);
	VAL *v =  makeVALreg();	
	v->def = 1;
	s->address =v;
	return makeMOV(makeVALconst(0), s->address);
}
/*
 * Whenever we enter assemBODY, we are entering a new scope, so we 
 * store base pointer and stack pointer, mark the start of a new code block,
 * then subtract from stack pointer the amount of variables 
 * that we declare in this new scope.
 * The variable declarations will then be handled in assemDECL.
 */
ASSEM *assemBODY(BODY *e, SymbolTable *sym){
	VAL *myEbp = makeVALrreg();
	myEbp->rreg->kind = ebpK;
	VAL *myEsp = makeVALrreg();
	myEsp->rreg->kind = espK;
	
	ASSEM *st = makePUSH(myEbp);
	makeMOV(myEsp,myEbp);
	st->block = 1;
	ASSEM *alloc = makeSUB(NULL,makeVALrreg());
	alloc->right->rreg->kind = espK;
	int localtemps = currentnumber;
	if (e->decll != NULL){        
		assemDECL(e->decll, sym);
	}
	 ASSEM *assem = assemSTATEL(e->statel, sym, NULL, NULL);
	 localtemps = currentnumber-localtemps;
	 alloc->left = makeVALconst(localtemps*4);
	 assem->block = -1;
 	return assem;
}
/*
 * Creates and collects labels for each function, so they are known
 * when we get to the statement list.
 * The label is saved within the symbol, so we can later look up the
 * function based on its id and get its label.
 */
ASSEM *createFUNClabel(DECL *e, SymbolTable *sym){
	SYMBOL *s;
	char *f;
	if (e->next != NULL){
		createFUNClabel(e->next, sym);
	}
	switch (e->kind) {
		case typeK:
			return NULL;
		case funcK:
		      s = getSymbol(sym, e->val.func->head->id);
		      if(s != NULL){
			f = "F";
			LABEL *l = makeLABEL(f);
			s->address = makeVALlabel(l);
		      }
		      return NULL;
		case varK:
			return NULL;
	}
}
/*
 * Iterates only through functions, such that they are placed in sequence,
 * instead of within eachother, if inputted as such.
 */
ASSEM *assemDECLfunc(DECL *e, SymbolTable *sym){
	if (e->next != NULL){
		assemDECLfunc(e->next, sym);
	}
	switch (e->kind) {
		case typeK:
			return NULL;
		case funcK:
			return assemFUNC(e->val.func, sym);
		case varK:
			return NULL;
	}
}
/*
 * Here we go through the declarations involving types and variables.
 * Functions have already been dealt with earlier, in order to make sure
 * their placement in the final assembly code was correct.
 */
ASSEM *assemDECL(DECL *e, SymbolTable *sym){
	if (e->next != NULL){
		assemDECL(e->next, sym);
	}
	switch (e->kind) {
		case typeK:
			return assemTYPE(e->val.s.type, sym);
		case funcK:
			return NULL;
		case varK:
			return assemVARDL(e->val.vardl, sym);
	}
}
ASSEM *assemSTATEL(STATEL *e, SymbolTable *sym, LABEL *l1, LABEL *l2){
	ASSEM *assem = NEW(ASSEM);
	switch (e->kind) {
		case statel_stateK: 
			assemSTATEL(e->val.s.statel, sym, l1, l2);
			return assemSTATE(e->val.s.state, sym, l1, l2);
		case stateK:			
			return assemSTATE(e->val.state, sym, l1, l2);
	}
}

/*
 * A switch case handles the different kind of statements expressed
 * in the input program.
 * 
 * returnK EXP will leave a scope and return to the
 * scope underneath / terminate the program at end of input.
 * 
 * writeK EXP will push the value recieved from EXP on stack
 * and call the C printf() function, then add to stackpointer.
 * Boolean values will push the string "true" or "false".
 * 
 * allocateK VARI OPTL will recieve a pointer from VARI and store
 * the heappointer there, then store at the heappointer location 
 * the size of the record or array, then increment
 * the heappointer by size according to (1+size)4 bytes.
 * 
 * EqualK VARI EXP, this case will take a variable, determine whether
 * it is record, array or a simple variable, then store the expression
 * output at the dereferenced location given by VARI.
 *
 * ifK and whileK will make the intermediate representations, as 
 * they are described on the templates for the project.
 *
 * forK, breakK, continueK, incK and decK are simple language extensions
 * that handle for loops, decrementing, incrementing and... HENRIK ? 
 */
ASSEM *assemSTATE(STATE *e, SymbolTable *sym, LABEL *l1, LABEL *l2){
	ASSEM *assem;
	ASSEM *assem2;
	ASSEM *assem3;
	ASSEM *assem4;
	ASSEM *c;
	LABEL *n;
	LABEL *n2;
	char *name;
	char *name2;
	SYMBOL *s;
	VAL *v;
	VAL *vv;
	VAL *vvv;
	VAL *vvvv;
	switch (e->kind) {
		case returnK:
			c = assemEXP(e->val.exp, sym);
			assem = makeMOV(c->right,makeVALrreg());
			assem->right->rreg->kind = eaxK;
			makeLEAVE();
			assem = makeRET();
			return assem;
		case writeK:
			assem = assemEXP(e->val.exp, sym);


			if ( e->val.exp->type->kind == Ty_bool){
				n = makeLABEL("end");
				n2 = makeLABEL("isFalse");
				
				v = makeVALreg();
				c = makeMOV(assem->right,v);
				makeCMP(makeVALconst(1),v);
				makeJMPNEQ(n2);
				makePUSH(makeVALtrue());
				makeJMP(n);
				
				makeEMPTY(n2);
				makePUSH(makeVALfalse());
				
				makeEMPTY(n);
				makeCALL(makeVALlabel(makePRINT()));
				vv = makeVALconst(4);	
				vvv = makeVALrreg();
				vvv->rreg->kind = espK;
				assem = makeADD(vv,vvv);
				return assem;
				
			} else {
				c = makePUSH(assem->right);
				makePUSH(makeVALnewline());
				makeCALL(makeVALlabel(makePRINT()));
				vv = makeVALconst(8);	
				vvv = makeVALrreg();
				vvv->rreg->kind = espK;
				assem = makeADD(vv,vvv);
				return assem;
			}
		
		case allocateK:
			assem = assemVARI(e->val.a.vari, sym);
			if ( e->val.a.optl != NULL){
					v = makeVALrreg();
					v->rreg->kind = ecxK;
					v->rreg->isOffset = 0;
					vv = makeVALrreg();
					vv->rreg->kind = ecxK;
					vv->rreg->isOffset = 1;
					vvv = makeVALreg();
					vvv->reg->number = assem->left->reg->number;
					vvv->reg->deref = 0;
					vvvv = makeVALreg();
					vvvv->reg->number = assem->left->reg->number;
					vvvv->reg->deref = 1;
					current = current->previous;
					
					if (e->val.a.vari->kind == idK){
						makeMOV(heap,assem->left);
						c = assemOPTL(e->val.a.optl,sym);
						ASSEM *b = makeMOV(c->right,makeVALreg());
						makeMOV(b->right,vvvv);
						makeADD(makeVALconst(1),b->right);
						makeMUL(makeVALconst(4),b->right);
						return makeADD(b->right,heap);	
					
					} else {
						makeMOV(heap,vvvv);
						c = assemOPTL(e->val.a.optl,sym);
						ASSEM *b = makeMOV(c->right,makeVALreg());
						assem3 = makeMOV(vvvv,vvv);
						
						
						makeMOV(b->right,vvvv);
						
						makeADD(makeVALconst(1),b->right);
						makeMUL(makeVALconst(4),b->right);
						return makeADD(b->right,heap);	
					
					}
			} else {
				v = makeVALrreg();
				v->rreg->kind = ecxK;
				vv = makeVALrreg();
				vv->rreg->kind = eaxK;
				vvv = makeVALrreg();
				vvv->rreg->kind = v->rreg->kind;
				vvv->rreg->isOffset = 1;
				VARI *myVari = e->val.a.vari;
				Ty_field *myField;
				int offset = 1;
				if (myVari->kind == vari_expK){
					current = current->previous;
					myGlobalField = myGlobalArray->val.record;
					while (myGlobalField != NULL) {
						offset++;
						myGlobalField = myGlobalField->next;
					}
					makeMOV(heap,v);
					makeMOV(v,assem->left);
					makeMOV(makeVALconst(offset),vv);
					makeMUL(makeVALconst(4),vv);
					return makeADD(vv,heap);
				} else if (e->val.a.vari->kind == idK ){
					s = getSymbol(sym, myVari->val.id);
					myField = s->type->val.record;
					
					VAL *length = makeVALconst(0);
					while (myField != NULL) {
						myField = myField->next;
						length->Const++;
					}
					makeMOV(heap,v);
					makeMOV(v,assem->left);
					makeMOV(makeVALconst(1 + length->Const),vv);
					makeMUL(makeVALconst(4),vv);
					return makeADD(vv,heap);
				} else {
					current = current->previous;
					while (myGlobalField != NULL) {
						offset++;
						myGlobalField = myGlobalField->next;
					}
					makeMOV(heap,v);
					makeMOV(v,assem->left);
					makeMOV(makeVALconst(offset),vv);
					makeMUL(makeVALconst(4),vv);
					return makeADD(vv,heap);					
				}
			}
			break;
		case equalK:;
			VARI *ourID;
			ourID = e->val.e.vari;
			while (ourID->kind != idK){
				if (e->val.e.vari->kind == vari_expK){
					ourID = ourID->val.e.vari;
				} else if (e->val.e.vari->kind == vari_idK){
					ourID = ourID->val.e.vari;
				}
			}
			s = getSymbol(sym, ourID->val.id);
			if (s->type->kind == Ty_array) {		
				assem2 = assemEXP(e->val.e.exp, sym);
				assem = assemVARI(e->val.e.vari, sym);
				current = current->previous;
				

				if (e->val.e.exp->type->kind == Ty_void){
					return makeMOV(makeVALconst(0),assem->right);
				}
				if (e->val.e.exp->kind == termK){
					if (e->val.e.exp->val.term->kind == variK){
						return makeMOV(assem2->right,assem->left);						
					}
				}

				
				assem->left->reg->deref = 0;
				assem = makeMOV(assem->left, makeVALreg());
				vvv = makeVALreg();
				vvv->reg->number = assem->left->reg->number;
				vvv->reg->deref = 1;
				
				
				return makeMOV(assem2->right,vvv );
			} else if (s->type->kind == Ty_record) {				
				assem2 = assemEXP(e->val.e.exp, sym);
				assem = assemVARI(e->val.e.vari, sym);
				current = current->previous;
				
				if (e->val.e.exp->type->kind == Ty_void){
					return makeMOV(makeVALconst(0),assem->right);
				}
				if (e->val.e.exp->kind == termK){
					if (e->val.e.exp->val.term->kind == variK){
						return makeMOV(assem2->right,assem->left);						
					}
				}
				
				assem->left->reg->deref = 0;
				assem = makeMOV(assem->left, makeVALreg());
				vvv = makeVALreg();
				vvv->reg->number = assem->right->reg->number;
				vvv->reg->deref = 1;
				return makeMOV(assem2->right,vvv );
			} else {
				  
				  ASSEM *exp = assemEXP(e->val.e.exp, sym);	
				  
				  assem = assemVARI(e->val.e.vari, sym); 
				  
				  current = current->previous;
				  vvv = makeVALreg();
				  vvv->reg->number = assem->left->reg->number;
				  if(s->table->level != sym->level){
					vvv->reg->deref = 1;
				  }
				  assem->left->reg->deref = 0;
 				  return makeMOV(exp->right,assem->left);

			}
			break;
			
			
			
		case ifK:;
			n = makeLABEL("endif");
			n2 = makeLABEL("elsepart");
			c = assemEXP(e->val.i.exp, sym);
			makeCMP(makeVALconst(1),c->right);
			
			if (e->val.i.opte != NULL){
				makeJMPNEQ(n2);
				assemSTATE(e->val.i.state, sym, l1, l2);
				makeJMP(n);
				makeEMPTY(n2);
				assem = assemOPTE(e->val.i.opte, sym, l1, l2);
				return makeEMPTY(n);
			} else {
				makeJMPNEQ(n);
				assemSTATE(e->val.i.state, sym, l1, l2);			
				return makeEMPTY(n);	
			}
		case whileK:
			n = makeLABEL("whilestart");
			n2 = makeLABEL("whileend");
			
			makeEMPTY(n);
			
			c = assemEXP(e->val.w.exp, sym);
			
			makeMOV(c->right, makeVALreg());
			makeCMP(makeVALconst(1),c->right);
			makeJMPNEQ(n2);
			assemSTATE(e->val.w.state, sym, n, n2);
			makeJMP(n);
			return makeEMPTY(n2);
		case statelK:
			assemSTATEL(e->val.statel, sym, l1, l2);
			break;
		case forK:
			assemVART(e->val.f.vart,e->table);

			assemSTATE(e->val.f.state1, e->table, l1, l2);

			n = makeLABEL("for");
			n2 = makeLABEL("end");
			makeEMPTY(n);
			c = assemEXP(e->val.f.exp, e->table);
			makeCMP(makeVALconst(1),c->right);
			makeJMPNEQ(n2);
			assem = assemSTATE(e->val.f.state3, e->table, l1, l2);
			assemSTATE(e->val.f.state2, e->table, l1, l2);
			makeJMP(n);
			makeEMPTY(n2);
			return assem;
		case breakK:
			if (l2 == NULL) {
				break;
			}
			return makeJMP(l2);
		case continueK:
			if (l1 == NULL) {
				break;
			}
			return makeJMP(l1);
		case incK:
			assem = assemVARI(e->val.vari, sym); 
			current = current->previous;
 			return makeINC(assem->left);			
		case decK:
			assem = assemVARI(e->val.vari, sym); 
			current = current->previous;
 			return makeDEC(assem->left);
	}
}

/*
 * assemOPTL return the expression length possibly given to allocate
 * and makes a check to ensure that a positive length is given.
 */
ASSEM *assemOPTL(OPTL *e, SymbolTable *sym){
	ASSEM *assem;
	ASSEM *assem2;
	VAL *v;
	VAL *vv = makeVALreg();
	vv->reg->deref = 0;
	assem = assemEXP(e->exp, sym);

	makeCMP(makeVALconst(0),assem->right);
	LABEL *nerr = makeLABEL("lengthOK");
	makeJMPGR(nerr);
	v = makeVALrreg();
	v->rreg->kind = eaxK;
	makeMOV(makeVALconst(4),v);
	VAL *myESP = makeVALrreg();
	myESP->rreg->kind = espK;
	makeMOV(retV, myESP);
	makeRET();
	makeEMPTY(nerr);
	
	return assem;
}

ASSEM *assemOPTE(OPTE *e, SymbolTable *sym, LABEL *l1, LABEL *l2){
	return assemSTATE(e->state, sym, l1, l2);
}
/*
 * assemVARI handles calls from EXP and STATE, returning both
 * pointers to memory locations and values. (values are stored
 * at the location).
 * the switch case handles idK, vari_expK and vari_idK.
 * 
 * idK: The base case is a variable in our own scope. 
 * Here we will always return a address to an offset from
 * the base pointer. If we are dealing with a different scope,
 * we retrieve the static link and copy its contents into a temporary.
 * Based on how far we go back, we do an iteration of dereferencing to 
 * get the previous static link and moving
 * until we reach the "level" of the scope in which the variable is declared.
 * Then finally we mark the temporary as a ScopeK to mark that we defer adding
 * an offset till later and include the symbol of the variable 
 * so we can retrieve this information.
 * 
 * vari_expK: This is the case when the grammar notes an array. It 
 * works differently depending on the last seen VARI. If it is
 * a idK kind, we set our global array symbol, Ty_array, to point to
 * the SYMBOLs record type.
 * if it is a vari_idK, we set the global array symbol to point to the
 * global record symbols array in its field.
 * If it is another vari_expK kind, we dereference the pointer to given
 * (its a pointer into a space in the heap).
 * Finally, for all cases, we increment the pointer to point to the 
 * correct heap spot.
 * 
 * vari_idK: When we encounter a VARI.id, we look at the last VARI to
 * determine what variable in the VARI record we need to return.
 * in the same way as with vari_exp, depending on the previous VARI,
 * we determine how to set the global record.
 * Then we search through the fields of the record, finding the variable
 * whose name matches that of the .id, then return its location.
 */
ASSEM *assemVARI(VARI *e, SymbolTable *sym){
	VAL *v;
	VAL *vv;
	VAL *vvv;
	VAL *vvvv;
	VAL *vvvvv;
	VAL *v6;
	VAL *v7;
	ASSEM *assem;
	ASSEM *assem2;
	ASSEM *assem3;
	SYMBOL *s;
	switch (e->kind) {
		case idK:
			s = getSymbol(sym, e->val.id);
			int off = (sym->level-s->table->level);
			if(off != 0){
			  VAL *tmp = makeVALrreg();
			  tmp->rreg->kind = ebpK;
			  tmp->rreg->offset = 8;
			  ASSEM *temp = makeMOV(tmp, makeVALreg());
			  tmp = makeVALreg();
			  tmp->reg->deref = 1;
			  tmp->reg->number = temp->right->reg->number;
			  off = off -1 ;
			  while(off > 0){
			    temp = makeMOV(tmp, makeVALreg());
				temp->right->reg->number = tmp->reg->number;
				tmp = makeVALreg();
				tmp->reg->deref = 1;
				tmp->reg->number = temp->right->reg->number;
			    off = off -1;
			  }
			  tmp->reg->deref = 1;
			  tmp->s = s;
			  tmp->kind = ScopeK;
			  return makeMOV(tmp, makeVALreg());
			}else{
			VAL *v = (VAL*)s->address;		
			
			 return makeMOV(s->address, makeVALreg());
			}
			break;

		case vari_expK:
			v = makeVALreg();
			v->reg->deref = 0;
			vv = makeVALreg();
			vv->reg->number = v->reg->number;
			vv->reg->deref = 1;
			vvv = makeVALreg();
			vvv->reg->deref = 0;
			vvvv = makeVALreg();
			vvvv->reg->number = vvv->reg->number;
			vvvv->reg->deref = 1;
			
			assem2 = assemEXP(e->val.e.exp, sym);	
			assem = assemVARI(e->val.e.vari, sym);
			
			current = current->previous;
			

			vvvvv = makeVALrreg();
			vvvvv->rreg->kind = eaxK;
			VAL *myESP = makeVALrreg();
			myESP->rreg->kind = espK;

			makeCMP(makeVALconst(0),assem2->right);
			LABEL *nerr = makeLABEL("indexNotNeg");
			makeJMPGRQ(nerr);
			makeMOV(makeVALconst(2),vvvvv);
			makeMOV(retV, myESP);
			makeRET();
			makeEMPTY(nerr);

			if (e->val.e.vari->kind == idK) {
				s = getSymbol(sym,e->val.i.vari->val.id);
				myGlobalArray = s->type->val.array;	
					
				
				v6 = makeVALreg();
				v6->reg->deref = 0;
				v7 = makeVALreg();
				v7->reg->deref = 1;
				v7->reg->number = v6->reg->number;
				VAL *v8 = makeVALreg();
				v8->reg->number = assem->left->reg->number;
				
				ASSEM *temp = makeMOV(assem2->right,makeVALreg());
				ASSEM *temp2 = makeMOV(v8,v6);
				temp2->left->reg->deref = 0;
				makeCMP(v7,temp->right);
				LABEL *nerr3 = makeLABEL("indexOK");
				makeJMPLE(nerr3);
				makeMOV(makeVALconst(2),vvvvv);
				makeMOV(retV, myESP);
				makePUSH(vvvvv);
				makePUSH(makeVALnewline());
				makeCALL(makeVALlabel(makePRINT()));
				makeADD(makeVALconst(8),myESP);
				makeRET();
				makeEMPTY(nerr3);	
				
				
				
			} else if (e->val.e.vari->kind == vari_idK){
				myGlobalArray = myGlobalField->type->val.array;

			} else {
				
				makeMOV(assem->left,vvv);
				makeMOV(vvvv,assem->left);
				
				v6 = makeVALreg();
				v6->reg->deref = 0;
				v7 = makeVALreg();
				v7->reg->deref = 1;
				v7->reg->number = v6->reg->number;
				
				ASSEM *temp = makeMOV(assem2->right,makeVALreg());
				makeMOV(assem->left,v6);
				makeCMP(v7,temp->right);
				LABEL *nerr3 = makeLABEL("indexOK");
				makeJMPLE(nerr3);
				makeMOV(makeVALconst(2),vvvvv);
				makeMOV(retV, myESP);
				makePUSH(vvvvv);
				makePUSH(makeVALnewline());
				makeCALL(makeVALlabel(makePRINT()));
				makeADD(makeVALconst(8),myESP);
				makeRET();
				makeEMPTY(nerr3);				
			}
			ASSEM *r = makeMOV(assem2->right,makeVALreg());

			makeADD(makeVALconst(1),r->right);
			
			assem = makeMOV(assem->left, v); 
				
			makeMUL(makeVALconst(4),r->right);
			makeADD(r->right,v);
			return makeMOV(vv,makeVALreg());

		case vari_idK:
			v = makeVALreg();
			v->reg->deref = 0;
			vv = makeVALreg();
			vv->reg->number = v->reg->number;
			vv->reg->deref = 1;

			assem = assemVARI(e->val.i.vari, sym);
			current = current->previous;
			int offset = 1;
			
			if (e->val.i.vari->kind == idK) {
				s = getSymbol(sym,e->val.i.vari->val.id);
				myGlobalField = s->type->val.record;
			} else if (e->val.i.vari->kind == vari_expK) {
				
				myGlobalField = myGlobalArray->val.record;				
			} else {
				
				myGlobalField = myGlobalField->type->val.record;				
			}

			while (myGlobalField != NULL && !(strcmp(myGlobalField->name,
e->val.i.id) == 0) ){
				myGlobalField = myGlobalField->next;
				offset++;
			}
			
			makeMOV(assem->left,v);		
			ASSEM *offs = makeMOV(makeVALconst(offset),makeVALreg());
			makeMUL(makeVALconst(4),offs->right);
			makeADD(offs->right,v);
			return makeMOV(vv,v); 
	}
}



/*
 * Here we handle Expressions. Simple arithmetic expressions are handled
 * trivially, except for the division operator, which is customized to
 * make use of the esi,edi and eax registers.
 * we also handle boolean operations by comparing the subexpressions 
 * and returning a 1 or 0 by jumping to labels, depending on the EFLAG
 * set by the compare instruction.
 * The OR and AND expressions are simply handled with a custom register
 * use and an AND/OR instruction.
 * Lastly, we return simple terms directly.
 */
ASSEM *assemEXP(EXP *e, SymbolTable *sym){
	ASSEM *assem;
	ASSEM *l;
	ASSEM *r;
	LABEL *eb;
	LABEL *lb;
	LABEL *nerr;
	VAL *v;
	VAL *vv;
	VAL *vvv;
	VAL *m;
	VAL *k;
	VAL *p;
	VAL *myESP;
	char *name;
	char *em;
	switch (e->kind) {
	case timesK:;
		l = assemEXP(e->val.s.left, sym);
		r = assemEXP(e->val.s.right, sym); 	
		return makeMUL(l->right,r->right);
	case divK:;
		m = makeVALrreg();
		m->rreg->kind = edxK;
		ASSEM *sa = makeMOV(m, makeVALreg());
		l = assemEXP(e->val.s.left, sym);
		p = makeVALrreg();
		p->rreg->kind = esiK;
		makeMOV(l->right,p);
		r = assemEXP(e->val.s.right, sym);
		makeCMP(makeVALconst(0),r->right);
		nerr = makeLABEL("notZ");
		makeJMPNEQ(nerr);
		v = makeVALrreg();
		v->rreg->kind = eaxK;
		makeMOV(makeVALconst(3),v);
		myESP = makeVALrreg();
		myESP->rreg->kind = espK;
		l = makeMOV(retV, myESP);
		makePUSH(v);
		makePUSH(makeVALnewline());
		makeCALL(makeVALlabel(makePRINT()));
		vv = makeVALconst(8);	
		makeADD(vv,myESP);
		makeRET();
		makeEMPTY(nerr);
		k = makeVALrreg();
		k->rreg->kind = ediK;
		makeMOV(r->right,k);

		makeMOV(p,v);
		makeMOV(makeVALconst(0),m);
		assem = makeDIV(k);
		assem->right = v;
		makeMOV(sa->right, m);
		return makeMOV(assem->right, makeVALreg());	  
	case plusK:;
		l = assemEXP(e->val.s.left, sym);
		r = assemEXP(e->val.s.right, sym);
		return makeADD(l->right,r->right);
	case minusK:;
		l = assemEXP(e->val.s.left, sym);
		r = assemEXP(e->val.s.right, sym);
		return makeSUB(r->right,l->right);
	case termK:;
		return assemTERM(e->val.term, sym);
	case andK:;
		l = assemEXP(e->val.s.left, sym);
		r = assemEXP(e->val.s.right, sym);
		v = makeVALrreg();
		v->rreg->kind = ecxK;
		makeMOV(l->right,v);
		makeAND(r->right,v);
		return makeMOV(v,makeVALreg());
	case orK:;
		l = assemEXP(e->val.s.left, sym);
		r = assemEXP(e->val.s.right, sym);
		v = makeVALrreg();
		v->rreg->kind = ecxK;
		makeMOV(l->right,v);
		makeOR(r->right,v);
		return makeMOV(v,makeVALreg());
	
	case cmpK:;
		lb = makeLABEL("neq");
		eb = makeLABEL("end");
		m = makeVALreg();
		l = assemEXP(e->val.s.left, sym);
		makeMOV(l->right,m);
		r = assemEXP(e->val.s.right, sym);
		makeCMP(m, r->right);
		makeJMPNEQ(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);
	case ncmpK:;
		lb = makeLABEL("eq");
		eb = makeLABEL("end");
		m = makeVALreg();
		l = assemEXP(e->val.s.left, sym);
		makeMOV(l->right,m);
		r = assemEXP(e->val.s.right, sym);
		makeCMP(m, r->right);
		makeJMPEQ(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);
	case highK:;
		lb = makeLABEL("hi");
		eb = makeLABEL("end");
		m = makeVALreg();
		r = assemEXP(e->val.s.right, sym);
		makeMOV(r->right,m);
		l = assemEXP(e->val.s.left, sym);
		makeCMP(m, l->right);
		makeJMPLEQ(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);   
	case lowK:;
		lb = makeLABEL("lo");
		eb = makeLABEL("end");
		m = makeVALreg();
		r = assemEXP(e->val.s.right, sym);
		makeMOV(r->right,m);
		l = assemEXP(e->val.s.left,sym);
		makeCMP(m, l->right);
		makeJMPGRQ(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);
	case higheqK:;
		
		lb = makeLABEL("hiq");
		eb = makeLABEL("end");
		
		m = makeVALreg();
		r = assemEXP(e->val.s.right, sym);
		makeMOV(r->right,m);
		
		l = assemEXP(e->val.s.left,sym);
		makeCMP(m, l->right);
		makeJMPLE(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);
		
	case loweqK:;
		lb = makeLABEL("loq");
		eb = makeLABEL("end");
		
		m = makeVALreg();
		
		r = assemEXP(e->val.s.right, sym);
		makeMOV(r->right,m);
		
		l = assemEXP(e->val.s.left, sym);
		makeCMP(m, l->right);
		makeJMPGR(lb);
		v = makeVALreg();
		makeMOV(makeVALconst(1),v);
		makeJMP(eb);
		makeEMPTY(lb);
		makeMOV(makeVALconst(0),v);
		makeEMPTY(eb);
		return makeMOV(v,m);
	}
} 
/*
 * assemTERM performs on several cases;
 * 
 * Calls VARI to recieve a value stored at some variable location in
 * the heap or from the base pointer.
 * 
 * In the case "id_act" we make sure to save all our registers(caller-save)
 * by pushing them onto the stack. Then we go through the arglist and push 
 * parameters onto the stack. Then we lookup the function id. If it is
 * within our own scope, we just push our own base pointer. If not,
 * then we need to use our own static link to go back and retrieve the 
 * correct static link in the scope of the function we are calling.
 * Finally, we get the label of the function from its symbol and make
 * the call ASSEM struct. After this is done, we copy the contents of eax
 * over to some temp, remove the parameters from the stack and restore 
 * all our registers. 
 * 
 * Return an isntruction holding a simple integer/boolean value.
 *
 * The !term, which makes a NOT instruction on the term output.
 * 
 * The absolute value of an expression will make instructions that fetch
 * the size of an array or evaluates the absolute value of an integer 
 * expression.
 * 
 */
ASSEM *assemTERM(TERM *e, SymbolTable *sym){
	ASSEM *assem;
	ASSEM *term;
	ASSEM *caller;
	ASSEM *assem2;
	ASSEM *assem3;
	VAL *v;
	VAL *vv;
	LABEL *n;
	switch (e->kind) {
		case variK:
			assem = assemVARI(e->val.vari, sym);
			
			return assem;

		case id_actK:
			caller = makePUSH(makeVALreg());
			caller->left->reg->precolor= EaxK;
			caller = makePUSH(makeVALreg());
			caller->left->reg->precolor = EbxK;
			caller = makePUSH(makeVALreg());
			caller->left->reg->precolor = EcxK;
			caller = makePUSH(makeVALrreg());
			caller->left->rreg->kind = edxK;
			if (e->val.s.alist != NULL) {
				assemALIST(e->val.s.alist, sym);
			}
			SYMBOL *s = getSymbol(sym,e->val.s.id);
			if(s->address == NULL){
			}
			if(s->table->level < sym->level){
			  int off = sym->level - s->table->level;
			  VAL *tmp = makeVALrreg();
			  tmp->rreg->kind = ebpK;
			  tmp->rreg->offset = 8;
			  ASSEM *temp;
			  temp = makeMOV(tmp, makeVALreg());
			  tmp  = makeVALreg();
			  tmp->reg->number = temp->right->reg->number;
			  tmp->reg->deref = 1;
			  off = off -1; 
			  while(off > 0){
			    temp = makeMOV(tmp, makeVALreg());
			    tmp  = makeVALreg();
			    tmp->reg->number = temp->right->reg->number;
			    tmp->reg->deref = 1;
			    off = off -1; 
			  }
			  tmp->reg->deref = 0;
			  makePUSH(tmp);
			}else{
			 VAL *tmp = makeVALrreg();
			 tmp->rreg->kind = ebpK;
			 makePUSH(tmp);
			}
			VAL *l = (VAL*)s->address;
			makeCALL(l);
			VAL *k = makeVALrreg();
			k->rreg->kind = espK;
			
			if(e->val.s.alist != NULL){
			  
				makeADD(makeVALconst((e->val.s.alist->length*4)+4),k);
			}else{
				makeADD(makeVALconst(4),k);
			}
			v = makeVALreg();
			v->reg->precolor = eaxK;
			assem = makeMOV(v, makeVALreg());
			caller = makePOP(makeVALrreg());
			caller->left->rreg->kind = edxK;
			caller = makePOP(makeVALreg());
			caller->left->reg->precolor= EcxK;
			caller = makePOP(makeVALreg());
			caller->left->reg->precolor = EbxK;
			caller = makePOP(makeVALreg());
			caller->left->reg->precolor = EaxK;
			return assem;
		case numconstK:
			return makeMOV(makeVALconst(e->val.num), makeVALreg());
		case booleanconstK:
			return makeMOV(makeVALconst(e->val.num),makeVALreg());
		case nullK:			
			break; 
		case notK:;
		v = makeVALreg();
		v->reg->precolor = eaxK;
		term = assemTERM(e->val.term, sym);
		makeMOV(term->right,v);
		makeNOT(v);
		return makeMOV(v, makeVALreg());
		case parK:
			return assemEXP(e->val.exp, sym);
		case barK:
			assem = assemEXP(e->val.exp, sym);
			assem2 = makeMOV(assem->right,makeVALreg());
			n = makeLABEL("end");
			
			v = makeVALreg();
			v->reg->deref = 0;
			vv = makeVALreg();
			vv->reg->deref = 1;
			vv->reg->number = v->reg->number;
			
			if (e->val.exp->kind == termK) {
				if (e->val.exp->val.term->kind == variK){					
					s = getSymbol(sym,e->val.exp->val.term->val.vari->val.id);
					if (s != NULL) {
						if (s->type->kind == Ty_array ){
							
							makeMOV(assem2->right,v);
							return makeMOV(vv,makeVALreg());
						} else {
							makeMOV(assem2->right,v);
							makeCMP(makeVALconst(0),v);
							makeJMPGR(n);
							makeMUL(makeVALconst(-1),v);
							makeEMPTY(n);
							return makeMOV(v,makeVALreg());	
						}						
					} else {
						if (e->val.exp->val.term->val.vari->kind == vari_expK){
							
							makeMOV(assem2->right,v);
							return makeMOV(vv,makeVALreg());
						}
						
					}					
				}	else {
				makeMOV(assem2->right,v);
				makeCMP(makeVALconst(0),v);
				makeJMPGR(n);
				makeMUL(makeVALconst(-1),v);
				makeEMPTY(n);
				return makeMOV(v,makeVALreg());
				
				}
			} else {
				makeMOV(assem2->right,v);
				makeCMP(makeVALconst(0),v);
				makeJMPGR(n);
				makeMUL(makeVALconst(-1),v);
				makeEMPTY(n);
				return makeMOV(v,makeVALreg());
				
			}
	}
}

ASSEM *assemALIST(ALIST *e, SymbolTable *sym){
	return assemELIST(e->elist, sym);
}
ASSEM *assemELIST(ELIST *e, SymbolTable *sym){
	switch (e->kind) {
		case expK:
			
			return makePUSH(assemEXP(e->val.exp, sym)->right);
		case elist_expK: 
			assemELIST(e->val.s.elist, sym);
			return makePUSH(assemEXP(e->val.s.exp, sym)->right);
	}
}


/*
 * BELOW is a list of functions that make a linked list of intermediate
 * representation of assembly instructions.
 */
ASSEM *makeCALL( VAL *left){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = CallK;
	assem->left = left;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeMOV( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = MovK;
	assem->left = left;
	assem->right = right;
	makelinked(assem);
	assem->args = 2;
	return assem;
}
ASSEM *makeDIV( VAL *left ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = DivK;
	assem->left = left;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeMUL( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = MulK;
	assem->left = left;
	assem->right = right;
	makelinked(assem);
	assem->args = 2;
	return assem;
}
ASSEM *makeADD( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = AddK;
	assem->left = left;
	assem->right = right;
	makelinked(assem);
	assem->args = 2;
	return assem;
}
ASSEM *makeSUB( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = SubK;
	assem->left = left;
	assem->right = right;
	makelinked(assem);
	assem->args = 2;
	return assem;
}
ASSEM *makeCMP( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = CmpK;	
	makelinked(assem);
	assem->args = 2;
	return assem;
}

ASSEM *makeNOT( VAL *left ){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->kind = NotK;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeAND( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = AndK;
	makelinked(assem);
	assem->args = 2;
	return assem;	
}
ASSEM *makeOR( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = OrK;
	makelinked(assem);
	assem->args = 2;
	return assem;	
}
ASSEM *makeXOR( VAL *left, VAL *right ){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = XOrK;
	makelinked(assem);
	assem->args = 2;
	return assem;	
}

ASSEM *makeLSHIFT( VAL *left, VAL *right){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = LShiftK;
	makelinked(assem);
	assem->args = 2;
	return assem;
}
ASSEM *makeRSHIFT( VAL *left, VAL *right){
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->right = right;
	assem->kind = RShiftK;
	makelinked(assem);
	assem->args = 2;
	return assem;
}




ASSEM *makeJMPGR(LABEL *lb ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpgrK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMPLE(LABEL *lb ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpleK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMPLEQ(LABEL *lb){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpleqK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMPGRQ( LABEL *lb){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpgrqK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMPNEQ(LABEL *lb  ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpneqK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMPEQ( LABEL *lb ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpeqK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}
ASSEM *makeJMP( LABEL *lb ){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = JmpK;
	assem->left = makeVALlabel(lb);
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}



ASSEM *makeEMPTY(LABEL *lb){
	ASSEM *assem = NEW(ASSEM);
	assem->label = lb;
	assem->kind = LblK;
	assem->left = NULL;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 0;
	return assem;
}

ASSEM *makePUSH(VAL *left){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = PushK;
	assem->left = left;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}


ASSEM *makePOP(VAL *left){
	ASSEM *assem = NEW(ASSEM);
	assem->kind = PopK;
	assem->left = left;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}

ASSEM *makeMain() {
	ASSEM *assem = NEW(ASSEM);
	assem-> kind = MainK;
	makelinked(assem);
	assem->args = 0;
	return assem;
}


ASSEM *makeRET(){
	ASSEM *assem = NEW(ASSEM);
	assem-> kind = RetK;
	makelinked(assem);
	assem->args = 0;
	return assem;
}
ASSEM *makeLEAVE(){
	ASSEM *assem = NEW(ASSEM);
	assem-> kind = LeaveK;
	makelinked(assem);
	assem->args = 0;
	return assem;
}

ASSEM *makeINC(VAL *left) {
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->kind = IncK;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}


ASSEM *makeDEC(VAL *left) {
	ASSEM *assem = NEW(ASSEM);
	assem->left = left;
	assem->kind = DecK;
	assem->right = NULL;
	makelinked(assem);
	assem->args = 1;
	return assem;
}

VAL *makeVALconst( int Const){
	VAL *val = NEW(VAL);
	val->kind = ConstK;
	val->Const = Const;
	val->def = 0;
	return val;	
}
VAL *makeVALreg( ){
	VAL *val = NEW(VAL);
	val->kind = RegK;
	val->reg = makeTEMP();
	val->def = 0;
	return val;	
}

VAL *makeVALlabel(LABEL *label){
	VAL *val = NEW(VAL);
	val->kind = LabelK;
	val->label = label;
	val->def = 0;
	return val;
}

VAL *makeVALnewline(){
	VAL *val = NEW(VAL);
	val->kind = NewlineK;
	val->def = 0;
	return val;
}
VAL *makeVALfalse(){
	VAL *val = NEW(VAL);
	val->kind = FalseK;
	val->def = 0;
	return val;
}
VAL *makeVALtrue(){
	VAL *val = NEW(VAL);
	val->kind = TrueK;
	val->def = 0;
	return val;
}

VAL *makeVALrreg(){
	VAL *val = NEW(VAL);
	val->kind = RRegK;
	val->rreg = makeREG(NULL);
	val->def = 0;
	return val;
}
Temp_o *makeTEMP() {	
	Temp_o *temp = NEW(Temp_o);
	temp->precolor = NpreK;
	temp->deref = 0;
	temp->number = currentnumber++;
	return temp;
}

void makelinked(ASSEM *assem){
	assem->previous = current;
	assem->block = 0;
	current->next = assem;
	current = assem;
	current->next = NULL;
}

LABEL *makeLABEL(char *name){
	LABEL *label = NEW(LABEL);
	label->name = name;
	label->number = labelnumber++;
	return label;  
}

LABEL *makeLABELend(char *name, int num){
  LABEL *label = NEW(LABEL);
  label->name = name;
  label->number = num;
  return label;  
}

LABEL *makePRINT(){
	LABEL *label = NEW(LABEL);
	label->name = "printf";
	label->number = -1;
	return label;  
}

LABEL *makeMALLOC(){
	LABEL *label = NEW(LABEL);
	label->name = "malloc";
	label->number = -1;
	return label;  
}

REG *makeREG(int off){
	if (off == NULL){
	}
	REG *r = NEW(REG); 
	r->isOffset = 0;
	r->offset = off;
	return r;
}

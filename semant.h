#include "tree.h"
#include "error.h"
#include <stdio.h>


void transFUNC(SymbolTable *venv, FUNC *e);
void transHEAD(SymbolTable *venv, HEAD *e);
void transTAIL(SymbolTable *venv, TAIL *e);
void transTYPE(SymbolTable *tenv, TYPE *e);

void transPARDL(SymbolTable *venv, PARDL *e);
void transVARDL(SymbolTable *venv, VARDL *e);
void transVART(SymbolTable *venv, VART *e);
void transBODY(SymbolTable *venv, BODY *e);

void transDECL(SymbolTable *venv, DECL *e);

void transSTATEL(SymbolTable *venv, STATEL *e);
void transSTATE(SymbolTable *venv, STATE *e);

void transOPTL(SymbolTable *venv, OPTL *e);
void transOPTE(SymbolTable *venv, OPTE *e);
void transVARI(SymbolTable *venv, VARI *e);

void transEXP(SymbolTable *venv, EXP *e);
void transTERM(SymbolTable *venv, TERM *e);
void transALIST(SymbolTable *venv, ALIST *e);
void transELIST(SymbolTable *venv, ELIST *e);

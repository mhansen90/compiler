#include "semant.h"

void transTYPE(SymbolTable *venv, TYPE *e){
 
}


void transFUNC(SymbolTable *venv, FUNC *e){
    transHEAD(e->table,e->head);
    transBODY(e->table,e->body);
    transTAIL(e->table,e->tail);
}
void transHEAD(SymbolTable *venv,HEAD *e){
	if (e->pardl != NULL) {
		transPARDL(venv,e->pardl);
	}
    transTYPE(venv,e->type);
}
void transTAIL(SymbolTable *venv,TAIL *e){
}

void transPARDL(SymbolTable *venv,PARDL *e){
    transVARDL(venv,e->vardl);
}
void transVARDL(SymbolTable *venv,VARDL *e){
switch (e->kind) {
    case vartK:
         transVART(venv,e->val.vart);
         break;
    case varcl_vartK:
         transVARDL(venv,e->val.s.vardl);
         transVART(venv,e->val.s.vart);
         break;
    }
}
void transVART(SymbolTable *venv,VART *e){
    transTYPE(venv,e->type);
}
void transBODY(SymbolTable *venv,BODY *e){
    if (e->decll != NULL){        
        transDECL(venv,e->decll);
    }
    transSTATEL(venv,e->statel);
}
void transDECL(SymbolTable *venv,DECL *e){
    if (e->next != NULL){
         transDECL(venv,e->next);
         }
switch (e->kind) {
    case typeK:
         transTYPE(venv,e->val.s.type);
         break;
    case funcK:
         transFUNC(venv,e->val.func);
         break;
    case varK:
         transVARDL(venv,e->val.vardl);
         break;
    }
}
void transSTATEL(SymbolTable *venv,STATEL *e){
switch (e->kind) {
    case statel_stateK: 
         transSTATEL(venv,e->val.s.statel);
         transSTATE(venv,e->val.s.state);
         break;
    case stateK:
         transSTATE(venv,e->val.state);
         break;
    }
}
void transSTATE(SymbolTable *venv, STATE *e){
	switch(e->kind){
		case whileK:
			transEXP(venv, e->val.w.exp);
			transSTATE(venv, e->val.w.state);
			break;
		case returnK: 
			transEXP(venv, e->val.exp);
			break;
		case writeK:
			transEXP(venv,e->val.exp);
			break;
		case allocateK:
			transVARI(venv,e->val.a.vari);
			if (e->val.a.optl != NULL) {
				transOPTL(venv,e->val.a.optl);
			}
			break;
		case equalK:
			transVARI(venv,e->val.e.vari);
			transEXP(venv,e->val.e.exp);
			break;
		case ifK:
			transEXP(venv,e->val.i.exp);
			transSTATE(venv,e->val.i.state);
			if (e->val.i.opte != NULL){
				transOPTE(venv,e->val.i.opte);
			}
			break;
		case statelK:
				transSTATEL(venv,e->val.statel);
				break;
		case breakK:
				break;
		case continueK:
				break;
		case forK:
			transVART(e->table, e->val.f.vart);
			transSTATE(e->table, e->val.f.state1);
			transEXP(e->table, e->val.f.exp);
			transSTATE(e->table, e->val.f.state2);
			transSTATE(e->table, e->val.f.state3);
			break;
		case incK:
			 transVARI(venv, e->val.vari);
			 break;
		case decK:
			 transVARI(venv, e->val.vari);
			 break;
	}
}


void transOPTL(SymbolTable *venv,OPTL *e){
    if (e->exp == NULL){            
        return;
    }
    transEXP(venv,e->exp);
}

void transOPTE(SymbolTable *venv,OPTE *e){
    if (e->state == NULL){
        return;
    }
    transSTATE(venv,e->state);
}

/*
Will save the type of the variable.
*/
void transVARI(SymbolTable *venv,VARI *e){
SYMBOL *temp;
switch (e->kind) {
    case idK:
	 temp = getSymbol(venv, e->val.id);
	 if (temp == NULL || temp->var == 0) {
		 EM_error3(e->lineno, EM_MISSID, e->val.id);
		 e->type = Ty_Error();
		 break;
	 }
	 e->type = temp->type;
	     break;
    case vari_expK:
         transVARI(venv,e->val.e.vari);
	 if (e->val.e.vari->type->kind == Ty_error) {
		e->type = e->val.e.vari->type;
	 } if( e->val.e.vari->type->kind == Ty_array){
	    e->type = e->val.e.vari->type->val.array;
	 }else{
	   EM_error(e->lineno,EM_NULLPOINTER);
	 }
         transEXP(venv,e->val.e.exp);
	 if(e->val.e.exp->type->kind != Ty_int){
	    EM_error(e->val.e.exp->lineno, EM_INT);
	 }
         break;
    case vari_idK:
       	transVARI(venv,e->val.i.vari);
	if (e->val.i.vari->type->kind == Ty_void) {
		e->type = e->val.i.vari->type;
		break;
	}
	if (e->val.i.vari->type->kind != Ty_record) {
		e->type = Ty_Void();
	    EM_error(e->lineno,EM_NULLPOINTER);
		break;
	}
	e->type = getRecord(e->val.i.vari->type->val.record, e->val.i.id);
	if (e->type->kind == Ty_void) {
		EM_error3(e->lineno, EM_MISSRECORDVAR, e->val.i.id);
	}
    break;
    }
}
/*
Will save the type it produces.
*/
void transEXP(SymbolTable *venv, EXP *e){
	EXP *right;
	EXP *left;
	TERM *t;
	switch(e->kind){
		case timesK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Int();
			break;
			
		case divK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			if(e->val.s.right->kind == termK){
			  if(e->val.s.right->val.term->kind == numconstK){
				if(e->val.s.right->val.term->val.num == 0){
				 EM_error(e->lineno,EM_DIV0); 
				}
			  }
			}
			e->type = Ty_Int();
			break;
		case plusK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Int();
			break;
			
		case minusK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Int();
			break;
			
		case termK:
			t = e->val.term;
			transTERM(venv, e->val.term);
			e->type = t->type;
			if (e->type->kind == Ty_bool){
				printf("a boolean was set!\n");
			}
			break;
			
		case cmpK:
			left = e->val.s.left;
			right = e->val.s.right;
			transEXP(venv, left);
			transEXP(venv, right);
			if(left->type->kind != right->type->kind){
				EM_error(e->lineno, EM_BMISS);
			}
			if((left->type->kind != Ty_int && left->type->kind != Ty_bool) 
				|| (right->type->kind != Ty_int 
				&& right->type->kind != Ty_bool)){
				
				EM_error(e->lineno, EM_BOTH);
			}
				
			e->type = Ty_Bool();
			
			break;
			
		case ncmpK:

			left = e->val.s.left;
			right = e->val.s.right;
			transEXP(venv, left);
			transEXP(venv, right);
			if(left->type->kind != right->type->kind){
				EM_error(e->lineno, EM_BMISS);
			}
			if((left->type->kind != Ty_int && left->type->kind != Ty_bool) 
				|| (right->type->kind != Ty_int 
				&& right->type->kind != Ty_bool)){
				
				EM_error(e->lineno, EM_BOTH);
				}
				
			e->type = Ty_Bool();
			
			break;
			
		case highK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Bool();
			break; 
			
		case lowK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Bool();
			break;
			
		case higheqK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Bool();
			break;
			
		case loweqK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_int){
				EM_error(e->lineno, EM_INT);
			}
			if(e->val.s.right->type->kind!=Ty_int){
				EM_error(e->lineno, EM_INT);
				
			}
			e->type = Ty_Bool();
			break;
			
		case andK:

			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind!= Ty_bool){
				EM_error(e->lineno, EM_BOOL);
			}
			if(e->val.s.right->type->kind!=Ty_bool){
				EM_error(e->lineno, EM_BOOL);
				
			}
			e->type = Ty_Bool();
			break;
			
		case orK:
			transEXP(venv, e->val.s.left);
			transEXP(venv, e->val.s.right);
			if(e->val.s.left->type->kind != Ty_bool){
				EM_error(e->lineno, EM_BOOL);
			}
			if(e->val.s.right->type->kind!=Ty_bool){
				EM_error(e->lineno, EM_BOOL);
				
			}
			e->type = Ty_Bool();
			break;
	}
}
/*
Will save what type it produces.
*/
void transTERM(SymbolTable *venv, TERM *e){
    SYMBOL *temp;
	switch(e->kind){
		
		case variK:
			transVARI(venv, e->val.vari);
			e->type = e->val.vari->type;
			break;
		case id_actK:
			if (e->val.s.alist != NULL) {
				transALIST(venv, e->val.s.alist);
			}
			temp = getSymbol(venv, e->val.s.id);
			e->type = temp->type->val.record->type;
			break;
		case numconstK: 
			e->type = Ty_Int();
			break;
		case booleanconstK:
			e->type = Ty_Bool();
			break; 
		case nullK:
			e->type = Ty_Void();
			break; 
		case notK:
			transTERM(venv,e->val.term);
			e->type = Ty_Bool();
			
			break;
		case parK:
			transEXP(venv,e->val.exp);
			e->type = e->val.exp->type;
			break;
		case barK:
			transEXP(venv, e->val.exp);
			e->type = Ty_Int();
			break;
	}
	
}
void transALIST(SymbolTable *venv, ALIST *e){
    transELIST(venv,e->elist);
}
void transELIST(SymbolTable *venv,ELIST *e){
  switch (e->kind) {
    case expK:
        transEXP(venv,e->val.exp);
        break;
    case elist_expK: 
        transELIST(venv,e->val.s.elist);
        transEXP(venv,e->val.s.exp);
        break;
  }
}


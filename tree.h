#ifndef __tree_h
#define __tree_h
#include "symbol.h"
#include <string.h>

typedef struct FUNC {
  int lineno;
  struct SymbolTable *table;
  struct HEAD *head; 
  struct BODY *body; 
  struct TAIL *tail;
} FUNC;

typedef struct HEAD {
  int lineno;
  char *id; 
  struct PARDL *pardl; 
  struct TYPE *type;
} HEAD;

typedef struct TAIL {
  int lineno;
  char *id;
} TAIL;

typedef struct TYPE {
  int lineno;
  enum {TidK, TintK, TboolK, TarrayK, TrecordK} kind;
  union {
    char *id;
    struct TYPE *type;
    struct VARDL *vardl;
  } val;
} TYPE;

typedef struct PARDL {
  int length;
  int lineno;
  struct VARDL *vardl;
} PARDL;

typedef struct VARDL {
  int lineno;
  enum {vartK, varcl_vartK} kind;
  union {
    struct VART *vart;
    struct {struct VARDL *vardl; struct VART *vart;}s;
  } val;
} VARDL;

typedef struct VART {
  int lineno;
  char *id;
  struct TYPE *type;
} VART;

typedef struct BODY {
  int lineno;
  struct DECL *decll;
  struct STATEL *statel;
} BODY;


typedef struct DECL {
  int lineno;
  enum {typeK, funcK, varK} kind;
  union {
  struct {char *id; struct TYPE *type;}s;
  struct FUNC *func;
  struct VARDL *vardl;
  } val;
  struct DECL *next;
} DECL;

typedef struct STATEL {
  int lineno;
  enum {statel_stateK, stateK} kind;
  union {
  struct {struct STATEL *statel; struct STATE *state;}s;
  struct STATE *state;
  } val;
} STATEL;

typedef struct STATE {
  int lineno;
  struct SymbolTable *table;
  enum {returnK,writeK,allocateK,equalK,ifK,whileK,statelK,breakK,continueK,
forK,incK,decK} kind;
  union {
    struct EXP *exp;
    struct {struct VARI *vari; struct OPTL *optl;}a;
    struct {struct VARI *vari; struct EXP *exp;}e;
    struct {struct EXP *exp; struct STATE *state; struct OPTE *opte;}i; 
    struct {struct EXP *exp; struct STATE *state;}w;
	struct {struct VART *vart; struct STATE *state1; 
struct EXP *exp; struct STATE *state2; struct STATE *state3;}f;
    struct STATEL *statel;
	struct VARI *vari;
  } val;
} STATE;


typedef struct OPTL {
  int lineno;
  struct EXP *exp;
} OPTL;


typedef struct OPTE {
  int lineno;
  struct STATE *state;
} OPTE;


typedef struct VARI {
  int lineno;
  enum {idK, vari_expK, vari_idK} kind;
  union {
    struct {struct VARI *vari; struct EXP *exp;}e;
    struct {struct VARI *vari; char *id;}i;
    char *id; 
  } val;
  struct Ty_ty *type;
} VARI;


typedef struct EXP {
  int lineno;
  enum {timesK,divK,plusK,minusK,termK, cmpK, ncmpK, 
  highK, lowK, higheqK, loweqK, andK, orK} kind;
  union {
    struct {struct EXP *left; struct EXP *right;}s;
    struct TERM *term;
  } val;
  struct Ty_ty *type;
} EXP;

typedef struct TERM {
  int lineno;
  enum {id_actK,numconstK, booleanconstK, nullK, notK, parK, barK, variK} kind;
  union {
    struct {char *id; struct ALIST *alist;}s;
    int num;
    struct TERM *term; 
    struct EXP *exp;
    struct VARI *vari;
  } val;
    struct Ty_ty *type;
} TERM;

typedef struct ALIST {
  int lineno;
  struct ELIST *elist;
  int length;
} ALIST;

typedef struct ELIST {
  int lineno;
  enum {elist_expK,expK} kind;
  union {
    struct {struct ELIST *elist; struct EXP *exp;}s;
    struct EXP *exp;
  } val;
} ELIST;



FUNC *makeFUNChbt(HEAD *head, BODY *body, TAIL *tail);

HEAD *makeHEADfunc(char *id, PARDL *pardl, TYPE *type);

TAIL *makeTAILend(char *id);

TYPE *makeTYPEid(char *id);
TYPE *makeTYPEint();
TYPE *makeTYPEbool();
TYPE *makeTYPEarray(TYPE *type);
TYPE *makeTYPErecord(VARDL *vardl);

PARDL *makePARDLvardl(VARDL *vardl);

VARDL *makeVARDLvardl(VARDL *vardl, VART *vart);
VARDL *makeVARDLvart(VART *vart);

VART *makeVARTid(char *id, TYPE *type);

BODY *makeBODYds(DECL *decll, STATEL *statel);

DECL *makeDECLLdecll(DECL *decll, DECL *decl);

DECL *makeDECLtype(char *id, TYPE *type);
DECL *makeDECLfunc(FUNC *func);
DECL *makeDECLvar(VARDL *vardl);

STATEL *makeSTATELstate(STATE *state); 
STATEL *makeSTATELstatel(STATEL *statel, STATE *state);

STATE *makeSTATEreturn(EXP *exp);
STATE *makeSTATEwrite(EXP *exp);
STATE *makeSTATEallocate(VARI *vari, OPTL *optl);
STATE *makeSTATEvari_exp(VARI *vari, EXP *exp);
STATE *makeSTATEif_then(EXP *exp, STATE *state, OPTE *opte);
STATE *makeSTATEwhile_do(EXP *exp, STATE *state);
STATE *makeSTATEstatel(STATEL *statel);
STATE *makeSTATEbreak();
STATE *makeSTATEcontinue();
STATE *makeSTATEfor(VART *vart, STATE *state1,
EXP *exp, STATE *state2, STATE *state3);
STATE *makeSTATEinc(VARI *vari);
STATE *makeSTATEdec(VARI *vari);
STATE *makeSTATEsplus(VARI *vari, EXP *exp);
STATE *makeSTATEsminus(VARI *vari, EXP *exp);
STATE *makeSTATEstimes(VARI *vari, EXP *exp);
STATE *makeSTATEsdiv(VARI *vari, EXP *exp);

OPTL *makeOPTLlength(EXP *exp);

OPTE *makeOPTEelse(STATE *state);

VARI *makeVARIid(char *id);
VARI *makeVARIvari_exp(VARI *vari, EXP *exp);
VARI *makeVARIvari_id(VARI *vari, char *id);

EXP *makeEXPtimes(EXP *left, EXP *right);
EXP *makeEXPdiv(EXP *left, EXP *right);
EXP *makeEXPplus(EXP *left, EXP *right);
EXP *makeEXPminus(EXP *left, EXP *right);
EXP *makeEXPterm(TERM *term);
EXP *makeEXPcmp(EXP *left, EXP *right);
EXP *makeEXPncmp(EXP *left, EXP *right);
EXP *makeEXPhigh(EXP *left, EXP *right);
EXP *makeEXPlow(EXP *left, EXP *right);
EXP *makeEXPhigheq(EXP *left, EXP *right);
EXP *makeEXPloweq(EXP *left, EXP *right);
EXP *makeEXPand(EXP *left, EXP *right);
EXP *makeEXPor(EXP *left, EXP *right);

TERM *makeTERMid_act(char *id , ALIST *alist);
TERM *makeTERMnull();
TERM *makeTERMnot(TERM *term);
TERM *makeTERMp(EXP *exp);
TERM *makeTERMb(EXP *exp);
TERM *makeTERMnumconst(int numconst);
TERM *makeTERMbooleanconst(int booleanconst);
TERM *makeTERMvari(VARI *vari);

ALIST *makeALISTelist(ELIST *elist);

ELIST *makeELISTelist(ELIST *elist, EXP *exp);
ELIST *makeELISTexp(EXP *exp);

#endif

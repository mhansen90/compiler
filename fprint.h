#include "mem.h"
#include <stdio.h>
#include "assem.h"


int fPrint(char *name);

int fPrint1(char *name, VAL *first);
int fPrint2(char *name, VAL *first, VAL *second);

int fPrintL(LABEL *lbl);
int fPrintM();

int fPrintStart(ASSEM *list);

char *fGetReg(REG *reg);

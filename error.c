#include "error.h"
#include <stdio.h>

int errors = 0;
void EM_error(int lineno, int message){
	errors= errors+1;
	switch(message){
		case EM_INT:
			fprintf(stderr,"Expected integer at line %d.\n",lineno);
			break;
		case EM_BOOL:
			fprintf(stderr,"Expected boolean at line %d.\n",lineno);
			break;
		case EM_STRING:
			fprintf(stderr,"Expected string at line %d.\n",lineno);
			break;
		case EM_BMISS:
			fprintf(stderr,"Type mismatch for boolean operation at line %d.\n",
 lineno);
			break;
		case EM_ARRAY:
			fprintf(stderr,"Expected array at line %d.\n",lineno);
			break;
		case EM_BOTH:
			fprintf(stderr,"Expected boolean or integer at line %d.\n",lineno);
			break;
		case EM_ARRAYRECORD:
			fprintf(stderr,"Expected record or array at line %d.\n",lineno);
			break;
		case EM_IDINTBOOL:
			fprintf(stderr,"Expect id,int or bool at line %d.\n",lineno);
			break;
		case EM_NULLPOINTER:
			fprintf(stderr,"Null pointer at line %d.\n",lineno);
			break;
		case EM_TYPEMISS:
			fprintf(stderr,"Type mismatch at line %d.\n",lineno);
			break;
		case EM_OPTL:
			fprintf(stderr,"Expected length of array at line %d.\n",lineno);
			break;
		case EM_FEWARGS:
			fprintf(stderr,"Too few arguments at line %d.\n",lineno);
			break;
		case EM_MANYARGS:
			fprintf(stderr,"Too many arguments at line %d.\n",lineno);
			break;
		case EM_RET:
			fprintf(stderr,"May lack return statement at line %d.\n", lineno);
			break;
		case EM_DIV0:
			fprintf(stderr,"Division by 0 at line %d.\n", lineno);
			break;
		case EM_FUNCIDMATCH:
			fprintf(stderr,"Id mismatch for function at line %d .\n", lineno);
			break;
		case EM_NOTWHILE:
			fprintf(stderr,"Not in while loop at line %d .\n", lineno);
			break;
		case EM_NOTEQUAL:
			fprintf(stderr,"Statement not an equal at line %d .\n", lineno);
		case EM_RETS:
			fprintf(stderr,"Has return statement in main at line %d.\n", 
lineno);
			break;	
		default:
			fprintf(stderr,"Unrecognizable error at line %d .\n", lineno);
			break;
	}
	
	
}

void EM_error3(int lineno, int message, char *name) {
	errors = errors+1;
	switch(message){
		case EM_MISSRECORDVAR:
			fprintf(stderr,"%s not present in record at line %d.\n",name, 
lineno);
			break;
		case EM_MISSID:
			fprintf(stderr,"%s not defined at line %d.\n",name, lineno);
			break;
		case EM_NOTDECLAREDYET:
			fprintf(stderr,"%s not declared yet at line %d.\n",name, lineno);
			break;
		case EM_INIT:
			fprintf(stderr,"%s may not be initialized at line %d.\n",name,
lineno);
			break;
		case EM_SYNTAX:
			fprintf(stderr,"Syntas error at line %d before symbol %s.\n",
lineno,name);
			break;
		default:
			fprintf(stderr,"Unrecognizable error %s at line %d .\n",name, 
lineno);
			break;
	}
}

int getErrors(){
		return errors;
}

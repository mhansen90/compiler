%{
#include "y.tab.h"
#include <string.h>
#include "mem.h"
#include "error.h"
 
extern int lineno;
int comments = 0;
%}
%x COMMENT

%%
[ \t]+        /* ignore */;
\n              lineno++;

"*"             return '*';
"/"             return '/';
"+"             return '+';
"-"             return '-';
"("             return '(';
")"             return ')';
"["             return '[';
"]"             return ']';
"|"             return '|';
"&"             return '&';
"!"             return '!';
","		return ',';
"."		return '.';
"="		return '=';
"{"		return '{';
"}"		return '}';
":"		return ':';
";"		return ';';
">"     return '>';
"<"     return '<';
"(*"    ++comments;BEGIN(COMMENT); 

<COMMENT>[^("(*"|\n)]* /* ignore everything else*/
<COMMENT>"(*"  ++comments; 
<COMMENT>\n    ++lineno;
<COMMENT>"*)"  if(--comments == 0) BEGIN(0)/*Go back to normal state*/;
<COMMENT>[*)]  /* ignore if it does not match comment seq*/
<COMMENT><<EOF>> {EM_error3(lineno, EM_SYNTAX, yytext); exit(1);}


"$"             yyterminate();

"true"			{ yylval.booleanconst = 1;
			return tBOOLEANCONST; }

"false"			{ yylval.booleanconst = 0;
			return tBOOLEANCONST; } 

						
"null"			return tNULL;	

"return"		return tRETURN;

"write"			return tWRITE;

"break"			return tBREAK;

"continue"		return tCONTINUE;

"allocate"		return tALLOCATE;

"if"			return tIF;

"then"			return tTHEN;

"while"			return tWHILE;

"do"			return tDO;

"else"			return tELSE;

"of"			return tOF;

"length"		return tLENGTH;	

"func"			return tFUNC;

"end"			return tEND;

"int"			return tINT;

"var"         	return tVAR;

"bool"			return tBOOL;

"array"			return tARRAY;

"record"		return tRECORD;

"type"			return tTYPE;

"for"			return tFOR;				
						
0|([1-9][0-9]*)        { yylval.numconst = atoi(yytext);
                       return tNUMCONST; }

								
[a-zA-Z_][a-zA-Z0-9_]* {yylval.stringconst=(char *)malloc(strlen(yytext)+1);
                         sprintf(yylval.stringconst,"%s",yytext); 
                         return tIDENTIFIER; }
						
"#".*\n              lineno++;						
						 						 
.               EM_error3(lineno, EM_SYNTAX, yytext);
%%

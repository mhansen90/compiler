#include "live.h"

int numInstr;
int numTMPS;
int numBlocks;
int dsize;
int colors = 2;
ASSEM *INITlive(ASSEM *start){
	ASSEM *temp = start;
	numBlocks = 0;
	numTMPS = start->numTMP;
	while(temp->next){
		numInstr++;
		if(temp->block == 1){
			numBlocks++;
		}
		temp = temp->next;
	}
	ASSEM *instr[numInstr];
	int i;	
	int blocks[numBlocks];

	int j = 0;
	temp = start;
	int fsize = (8-numInstr%8)+numInstr;
	dsize = (8-numTMPS%8)+numTMPS;
	/*Initialize all sets for each instruction. Collect start instructions
	 * for blocks.*/
	for(i = 0; i < numInstr; i++){
		instr[i] = temp;
		temp->pred = initBITSET(fsize);
		temp->succ = initBITSET(fsize);
		temp->in = initBITSET(dsize);
		temp->out = initBITSET(dsize);
		temp->def = initBITSET(dsize);
		temp->use = initBITSET(dsize);
		if(temp->block == 1){
			blocks[j] = i;
			j++;
		}
		temp = temp->next;	
	}
	/*Do liveness and register allocation for each block.
	 */
	for(i = 0; i < numBlocks; i++){
		FLOWlive(instr, blocks[i]);		
		DATAlive(instr,blocks[i]);		
		INITgraph(instr,blocks[i]);
	}
	return instr;
}

int FLOWlive(ASSEM *instr[], int start){
	int i;
	ASSEM *cur;
	int succInd = 0;
	/*Construct flow graph, and destination instructions for jumps.
	 */
	for(i = start; i < numInstr && instr[i]->block != -1; i++){
		cur = instr[i];
		if(i != 0){
			setBIT(cur->pred, i-1);
		}if(i != numInstr-1){
			setBIT(cur->succ, i+1);
		}
		if(cur->left != NULL){
			if(cur->left->kind == LabelK && cur->kind != CallK && 
				(cur->left->kind == LabelK && cur->kind != PushK)){
				succInd = findSUCC(cur->left->label, instr,start);
				if(succInd >= 0){
					setBIT(cur->succ, succInd); 
					setBIT(instr[succInd]->pred, i);
				}else{
					
				}
			}			
		}
	}
	return 0;
}

int DATAlive(ASSEM *instr[], int start){
	int i = start;
	int kind;
	int changes = 2;
	int k = 0;
	int end = 0;
	ASSEM *cur;
	BITSET *s = initBITSET(instr[0]->out->size);
	BITSET *p = initBITSET(instr[0]->out->size);
	/*Look at temporaries and add them to the sets based on what type 
	 * of instruction. F.eks. a Push instruction only uses a temporary,
	 * while a Mov uses and defines.
	 */
	for(i = start; i < numInstr ; i++){
		if(instr[i]->block == -1){
			end = i;
			break;
		}
		cur = instr[i];
		if(cur->args == 1){
			kind = cur->kind;
			switch(kind){
				case NotK:
					if(cur->left->kind == RegK ){
						setBIT(cur->use,cur->left->reg->number);
					}
					if(cur->left->kind == RegK ){
						setBIT(cur->def,cur->left->reg->number);
					}
					
					break;
					
				default:
				if(cur->left != NULL){
					
					if(cur->left->kind == RegK ){
						setBIT(cur->use,cur->left->reg->number);
					}
				}
				break;
			}
		}if(cur->args == 2){
			kind = cur->kind;
			switch(kind){
				case MovK:
					if(cur->left->kind == RegK ){
						setBIT(cur->use,cur->left->reg->number);
					}
					if(cur->right->kind == RegK 
						&& cur->right->def == 0){
						setBIT(cur->def,cur->right->reg->number);
					
						
					}	else if(cur->right->kind == RegK 
						&& cur->right->def == 1){
						setBIT(cur->use,cur->right->reg->number);
					
						setBIT(cur->def,cur->right->reg->number);
					
					}
					break;
					
				case CmpK:
					if(cur->left->kind == RegK ){
						setBIT(cur->use,cur->left->reg->number);
					}
					if(cur->right->kind == RegK ){
						setBIT(cur->use,cur->right->reg->number);
					}
					break;
				default:
					
				if(cur->left != NULL){
					if(cur->left->kind == RegK ){
						setBIT(cur->use,cur->left->reg->number);
					}
				} if(cur->right != NULL){
					if(cur->right->kind == RegK){
						setBIT(cur->use,cur->right->reg->number);
						setBIT(cur->def,cur->right->reg->number);
					}				
				}
				break;
			}

			
		}
	}
	/*Now construct in and out sets by in = use + (out - def) 
	 * and out = sum(succ->in). 
	 * If any change in any set, go through another iteration.
	 */
	while(changes > 0){
		if(changes == 2){
			changes = changes-1;
		}else{
				changes = 0;
		}
		for(i = end; i > start && instr[i]->block != 1; i--){
			cur = instr[i];

			clearBITSET(s);
			clearBITSET(p);
			unionBit(s, cur->out);
			unionBit(p, cur->in);
			diffBit(s, cur->def);
			unionBit(s,cur->use);	
			clearBITSET(cur->in);
			k = unionBit(p,s);
			changes = changes + k;
			unionBit(cur->in,p);
			
			int j;
			for(j = start; j < numInstr && instr[j]->block != -1; j++){
				if(isBitSet(cur->succ,j)){
					changes = changes +unionBit(cur->out,  instr[j]->in);
					
					
				}
			}	
			
		}
	}
	return 0;
}

/*Function for finding successors for jmps
 * Goes through block and looks for label.
 */
int findSUCC(LABEL *lb, ASSEM *instr[], int start){
	int i;
	for(i = start; i < numInstr&& instr[i]->block != -1; i++){
		if(instr[i]->label != NULL){
			if(instr[i]->label->number == lb->number){
				
				if(strcmp(instr[i]->label->name, lb->name)==0){
					return i+1;
				}				
			}
		}		
	}

	return -1;	
}

int INITgraph(ASSEM *instr[], int start){
	int i,j,c;
	NODE *temps[numTMPS];
	NODE *temp;
	for(i = 0; i < numTMPS; i++){
		temps[i] = NULL;
	}
	int t = -1;
	int ot = -1;
	/*Initialize all node representations for temporaries used in block. 
	 * If already assigned a register, we copy over the precolor value.
	 */
	for(j = start; j < numInstr && instr[j]->block != -1;j++){
		if(instr[j]->left != NULL){
				if(instr[j]->left->kind == RegK){
					t = instr[j]->left->reg->number;
					if(temps[t] ==NULL){
						temp = NEW(NODE);
						temp->connections = initBITSET(dsize);
						temp->number = t;
						temp->color = -1;
						temp->push = 0;
						temps[t] = temp;
						temps[t]->precolor = instr[j]->left->reg->precolor;
					}
				}
				
		}if(instr[j]->right != NULL){
				if(instr[j]->right->kind == RegK){
					t = instr[j]->right->reg->number;
					if(temps[t] ==NULL){
						temp = NEW(NODE);
						temp->connections = initBITSET(dsize);
						temp->number = t;
						temp->color = -1;
						temp->push = 0;
						temps[t] = temp;
						temps[t]->precolor = instr[j]->right->reg->precolor;
					}
				}
				
		}
		
	}
	/*Now construct the graph by looking a interfering variables. 
	 * Interfering if temporaries x & y of an instruction and x in def
	 *and y in out, and instruction is not an assignment.
	 */
	for(j = start; j < numInstr && instr[j]->block != -1;j++){
		if(instr[j]->left != NULL){
				if(instr[j]->left->kind == RegK){
					t = instr[j]->left->reg->number;
						temp = temps[t];
					
					
						for(c = 0; c < numTMPS; c++){
							if(temps[c]==NULL){
								continue;
							}
							if(c != t && isBitSet(instr[j]->out, c)==1 || 
								c!= t && isBitSet(instr[j]->use, c)==1 || 
								isBitSet(temps[c]->connections,t)==1 
								&& c != t ){
								setBIT(temp->connections,c );
								setBIT(temps[c]->connections,t );
								temps[c]->degree =getBitSum(
									temps[c]->connections);
								
							}
						}
						ot = t;
					
					temp->degree = getBitSum(temp->connections);
				}
				
			}if(instr[j]->right != NULL){
				if(instr[j]->right->kind == RegK && instr[j]->args != 1){
					t = instr[j]->right->reg->number;
					temp = temps[t];
					
						for(c = 0; c < numTMPS; c++){
							if(temps[c]==NULL){
								continue;
							}
								
							if(c != t && isBitSet(instr[j]->def, c)==1 ||
								c!= t &&isBitSet(instr[j]->use, c)==1 || 
								isBitSet(temps[c]->connections,t)==1 
								&& c != t){

									setBIT(temp->connections,c );
									setBIT(temps[c]->connections,t);
									temps[c]->degree = getBitSum(
										temps[c]->connections);
							}
						}
					
					if(instr[j]->left->kind == RegK && 
						temps[instr[j]->left->reg->number]!= NULL){
						setBIT(temp->connections, 
							   instr[j]->left->reg->number);
						setBIT(
							temps[instr[j]->left->reg->number]->connections,
							   instr[j]->right->reg->number);
						temps[instr[j]->left->reg->number]->degree = 
						getBitSum(temp->connections);
					}
					temp->degree = getBitSum(temp->connections);
					
					
				}
				
			}
			ot = -1;
	}
	for(i = 0; i < numTMPS; i++){
		if(temps[i]==NULL){
			continue;
		}
		printBits(temps[i]->connections);
		
	}
	/*When done, begin the colouring of the graph.
	 * And then change the temporaries with they were assigned a register
	 */
	SIMPLIFYgraph(temps);
	ASSIGNreg(temps,instr, start); 
	return 0;
}

int SIMPLIFYgraph(NODE *temps[]){
	int canPush = 1;
	int i,j;
	int sum = 0;
	NODE *push = NULL;
	/*Push all temps which can be coloured(degree less than number
	 * of colours), by removing their connection from the temp nodes
	 * left in the graph.
	 */
	while(canPush == 1){
		canPush = 0;
		for(i = 0; i < numTMPS;i++){
			if(temps[i]== NULL){
				continue;
			}
			if(temps[i]->degree < colors && temps[i]->push == 0 
				&& temps[i]->precolor == NpreK){
				if(push == NULL){
					temps[i]->previous = NULL;
					temps[i]->push = 1;
						push = temps[i];
				}else{
					temps[i]->previous = push;
					temps[i]->push = 1;
					push = temps[i];					
				}
				int *cons = getBits(temps[i]->connections);
				printBits(temps[i]->connections);

				for(j = 0; j < numTMPS; j++){
					if(cons[j]==1){
					
						printBits(temps[j]->connections);
						clearBIT(temps[j]->connections,i);
						temps[j]->degree = temps[j]->degree -1;
						if(temps[j]->degree < colors){
							canPush = 1;
						}
					}
				}
					
				
			}
			
			
			
		}
		/*After we can push no more, we make sure to assign the 
		 * pre coloured temps their colours and move on. 
		 * 
		 */
		if(canPush == 0){
			for(i = 0; i < numTMPS;i++){
				if(temps[i]== NULL){
					continue;
				}
				if(temps[i]->precolor != NpreK){
					temps[i]->color = temps[i]->precolor;
					
				}
				
			}
			
		}
	
	}
	int uColor[colors];
	/*Assign colours based on if a colour has been used yet.
	 * If any temp node we are connected to has a colour, we
	 * mark it as used.
	 */ 
	while(push != NULL ){
		for(i = 0; i < colors; i++){
			uColor[i]=0;
		}
		printBits(push->connections);
		for(i = 0; i < numTMPS; i ++){
			
			if(isBitSet(push->connections,i)==1){
				setBIT(temps[i]->connections,push->number);
				if(temps[i]->color != -1){	    					
				
					uColor[temps[i]->color] = 1;
				}

				
				
			}
		}
		for(i = 0; i < colors; i++){
			if(uColor[i]!= 1){
				push->color = i;
				break;
			}
		}
		if(push->color == -1){
			return -1;
			
		}
		push = push->previous;
	}
	return 0;
	
}

int ASSIGNreg(NODE *temps[], ASSEM *instr[], int start){
	int i,j,t;
	/*Assign registers to temporaries by matching the temp nodes
	 * to the temporaries used in each instruction.
	 * 
	 */
	for(i = start; i < numInstr && instr[i]->block != -1; i++){
		if(instr[i]->left != NULL){
			if(instr[i]->left->kind == RegK){
				t = instr[i]->left->reg->number;
				if(temps[t] == NULL){
					
				}
					if(temps[t]->color != -1){
						
						instr[i]->left->kind = RRegK;
						instr[i]->left->rreg = makeREG(NULL);
						instr[i]->left->rreg->kind = 
						getReg(temps[t]->color);
						instr[i]->left->rreg->isOffset = 
						instr[i]->left->reg->deref;
					}


				
			}
		}
		if(instr[i]->right != NULL && instr[i]->args != 1){
			if(instr[i]->right->kind == RegK){
				
				t = instr[i]->right->reg->number;
				if(temps[t] == NULL){
					
				}
				if(temps[t]->color != -1){
						
					instr[i]->right->kind = RRegK;
					instr[i]->right->rreg = makeREG(NULL);
					instr[i]->right->rreg->kind = 
					getReg(temps[t]->color);
					instr[i]->right->rreg->isOffset = 
					instr[i]->right->reg->deref;

						
						
				}	  
			
			}		
		} 
	}
	
	return 0;
}
/*Convert from precolor to real register
 */
int getReg(int color){
	if(color == -1){
		return NpreK;
		
	}
		switch (color) {
			case EaxK:
				return eaxK;
			case EbxK:
				return ebxK;
			case EcxK:			
				return ecxK;
		}
			
			
	
}
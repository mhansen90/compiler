#include "tree.h"

void traversalFUNC(FUNC *e);
void traversalHEAD(HEAD *e);
void traversalTAIL(TAIL *e);
void traversalTYPE(TYPE *e);

void traversalPARDL(PARDL *e);
void traversalVARDL(VARDL *e);
void traversalVART(VART *e);
void traversalBODY(BODY *e);

void traversalDECL(DECL *e);

void traversalSTATEL(STATEL *e);
void traversalSTATE(STATE *e);

void traversalOPTL(OPTL *e);
void traversalOPTE(OPTE *e);
void traversalVARI(VARI *e);

void traversalEXP(EXP *e);
void traversalTERM(TERM *e);
void traversalALIST(ALIST *e);
void traversalELIST(ELIST *e);
#include "tree.h"
#include "error.h"
#include "mem.h"

void typeINIT(SymbolTable *venv,BODY *e);

void typeFUNC(FUNC *e);
void typeHEAD(SymbolTable *venv,HEAD *e);
void typeTAIL(SymbolTable *venv,TAIL *e);
void typeTYPE(SymbolTable *venv,TYPE *e);

void typePARDL(SymbolTable *venv,PARDL *e);
void typeVARDL(SymbolTable *venv,VARDL *e);
void typeVART(SymbolTable *venv,VART *e);
void typeBODY(SymbolTable *venv,BODY *e);

void typeDECL(SymbolTable *venv,DECL *e);

void typeSTATEL(SymbolTable *venv,STATEL *e);
void typeSTATE(SymbolTable *venv,STATE *e);

void typeOPTL(SymbolTable *venv,OPTL *e);
void typeOPTE(SymbolTable *venv,OPTE *e);
void typeVARI(SymbolTable *venv,VARI *e);

void typeEXP(SymbolTable *venv,EXP *e);
void typeTERM(SymbolTable *venv,TERM *e);
void typeALIST(SymbolTable *venv,ALIST *e, Ty_field *field);
void typeELIST(SymbolTable *venv,ELIST *e);
int return_state(STATE *state, HEAD *e );
int return_statel(STATEL *statel, HEAD *e );
Ty_field *recursiveCheck(ELIST *e,Ty_field *field);

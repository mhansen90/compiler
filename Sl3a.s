        .text
 
form: 
        .string "%d\n" 
 
formFalse: 
        .string "false\n"
 
formTrue: 
        .string "true\n"
 
        .data
 
heap: 
        .long 0
 
retVal: 
        .long 0
 
F0: 
        pushl %ebp 
        movl %esp,%ebp 
        subl $8,%esp 
        movl $2,%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $2,%eax 
        leave 
        ret 
Fend0: 
.globl main 
main: 
        movl %esp,retVal 
        pushl %ebp 
        movl %esp,%ebp 
        movl $4000000,(%esp) 
        call malloc 
        movl %eax,heap 
        movl %ebp,%esp 
        subl $104,%esp 
        movl $0,-4(%ebp) 
        movl $0,-8(%ebp) 
        movl $0,-12(%ebp) 
        movl $4,-16(%ebp) 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-4(%ebp) 
        popl %eax 
        movl $3,-20(%ebp) 
        pushl %eax 
        movl -20(%ebp),%eax 
        movl %eax,-12(%ebp) 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        pushl %ecx 
        pushl %edx 
        pushl %ebp 
        call F0 
        addl $4,%esp 
        movl %eax,-24(%ebp) 
        popl %edx 
        popl %ecx 
        popl %ebx 
        popl %eax 
        pushl %eax 
        movl -24(%ebp),%eax 
        movl %eax,-8(%ebp) 
        popl %eax 
        pushl %eax 
        movl -4(%ebp),%eax 
        movl %eax,-28(%ebp) 
        popl %eax 
        pushl -28(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        pushl %eax 
        movl -8(%ebp),%eax 
        movl %eax,-32(%ebp) 
        popl %eax 
        pushl -32(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $3,%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl -12(%ebp),%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $0,%eax 
        leave 
        ret 

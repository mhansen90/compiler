#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symbol.h"
#include "mem.h"
#include "error.h"

/*
When given a string, it will perform a hash and return an int. 
*/
int Hash(char *str){
	int partialValue = 0;
	int len = strlen(str);
	int i;
	for(i = 0; i < len; i++){
		partialValue += (int)str[i];
		partialValue = partialValue << 1;
	}
	partialValue = partialValue >> 1;
	i = partialValue % HashSize;
	if (i<0) {
		return i+HashSize;
	}
	return i;
}

/*
Will create the root symboltable.
*/
SymbolTable *initSymbolTable(){
    SymbolTable *newTable = NEW(SymbolTable);
    newTable->next = NULL;
    newTable->level = 0;
    int j;
    for (j = 0;j < HashSize ;j++){
        newTable->table[j] = NULL;
    }
    return newTable;
}
/*
Will create a new table and remember the parent table. 
*/
SymbolTable *scopeSymbolTable(SymbolTable *t){
    SymbolTable *newTable = initSymbolTable();
    newTable->next = t;
    newTable->level = t->level+1;
    return newTable;
}
/*
Will create a symbol given a name and type and insert it in the table referred 
to. It will save the int which tells if the symbol is a type or variable.
*/ 
SYMBOL *putSymbol(SymbolTable *t, char *name, Ty_ty *type, int var){
    int index = Hash(name); 
    SYMBOL *mySymbol = NEW(SYMBOL);
    mySymbol->name = strdup(name);
    mySymbol->var = var;
    mySymbol->type = type; 
    mySymbol->next = NULL; 
    if(t->table[index] == NULL){
        t->table[index] = mySymbol;
    }  else {
        SYMBOL *firstSymbol = NEW(SYMBOL);
        firstSymbol = t->table[index];
		if(strcmp(firstSymbol->name,name) == 0 ){
        	fprintf(stderr,"The variable name '%s' already exists "
			"in this table.\n",name);
			return NULL;
        }
        while(firstSymbol->next != NULL){
            if(strcmp(firstSymbol->name,name)==0){
                fprintf(stderr,"The variable name '%s' already exists in " 
				"this table.\n", name);
				return NULL;
            }
            firstSymbol = firstSymbol->next;
        }
        firstSymbol->next = mySymbol;
    }
    printf("Inserted '%s' \n", name);
    mySymbol->table = t;
    return mySymbol;
}

/*
When given a record's linked list of names, it will find the name and return 
the type.
*/
Ty_ty *getRecord(Ty_field *type, char *name) {
    if (strcmp(name, type->name)==0) {
	return type->type;
    }
    if (type->next == NULL) {
	return Ty_Void();
    }
    return getRecord(type->next, name);
}
/*
Will compare types, returning 1 if they are the same.
*/
int compareTypes(Ty_ty *var, Ty_ty *exp) {
	if (var->kind != exp->kind && (exp->kind != Ty_void 
	|| ((var->kind == Ty_bool || var->kind == Ty_int)
	&& exp->kind == Ty_void))) {
		return 0;
	}
	if (exp->kind == Ty_void) {
		return 1;
	}
	if (var->kind == Ty_array) {
		return compareTypes(var->val.array, exp->val.array);
	}
    if (var->kind == Ty_record) {
		return compareRecords(var->val.record, exp->val.record);
	}
	return 1;
}

/*
Will check if the records are the same. They names and types have to be in the 
same order.
*/
int compareRecords(Ty_field *var, Ty_field *exp) {
	if (strcmp(var->name, exp->name) != 0 ) {
		return 0;
	}
	if (compareTypes(var->type, exp->type) == 0) {
		return 0;
	}
	if (var->next != NULL && exp->next != NULL) {
		return compareRecords(var->next, exp->next);
	}
	if (var->next == NULL && exp->next == NULL) {
		return 1;
	}
	return 0;
}

/*
Will get a symbol from the referred table if the name is present.
*/
SYMBOL *getSymbol(SymbolTable *t, char *name){
	SymbolTable *curTable;
	SYMBOL *curSymbol;
	int index = Hash(name);
	curTable = t;
	do{
		if(curTable->table[index]== NULL){
			curTable = curTable->next;
			continue;
		}
		curSymbol = curTable->table[index];
		
		do{
			if(strcmp(curSymbol->name,name) == 0){				
				return curSymbol;
			}
			curSymbol = curSymbol->next;

		} while(curSymbol!= NULL);
		
		curTable = curTable->next;

	}while(curTable != NULL);
	return NULL;
}

/*
Prints the entire content of the table
*/
void dumpSymbolTable(SymbolTable *t){
	SymbolTable *curTable;
	SYMBOL *curSymbol;
	curTable = t;
	int i;
	while(curTable != NULL) { 
		for(i = 0; i < HashSize; i++){
			curSymbol = curTable->table[i];
			while (curSymbol != NULL) {
		    	printf("%s \n",curSymbol->name);        
                curSymbol = curSymbol->next;
			}
		}
		curTable = curTable->next;
	}
} 

/*
Makes all kinds of types
*/
Ty_ty *Ty_Int() {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_int;
  return e;
}

Ty_ty *Ty_Bool() {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_bool;
  return e;
}

Ty_ty *Ty_Void() {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_void;
  return e;
}

Ty_ty *Ty_Error() {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_error;
  return e;
}

Ty_ty *Ty_Record(Ty_field *field) {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_record;
  e->val.record = NEW(Ty_field);
  memcpy(e->val.record, field, sizeof(Ty_field));
  return e;
}

Ty_ty *Ty_Array(Ty_ty *ty) {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_array;
  e->val.array = NEW(Ty_ty);
  memcpy(e->val.array, ty, sizeof(Ty_ty));
  return e;
}

Ty_ty *Ty_Name(SYMBOL *sym, Ty_ty *ty) {
  Ty_ty *e;
  e = NEW(Ty_ty);
  e->kind = Ty_name;
  e->val.name.sym = NEW(SYMBOL);
  memcpy(e->val.name.sym, sym, sizeof(SYMBOL));
  e->val.name.ty = NEW(Ty_ty);
  memcpy(e->val.name.ty, ty, sizeof(Ty_ty));
  return e;
}

Ty_field *Ty_Field(char *name, Ty_ty *ty) {
  Ty_field *e;
  e = NEW(Ty_field);
  e->name = strdup(name);
  e->type = NEW(Ty_ty);
  memcpy(e->type, ty, sizeof(Ty_ty));
  return e;
}

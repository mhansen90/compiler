#include "push.h"

int numTMP;
int off;
int index;

ASSEM *pushALL(ASSEM *start){
	ASSEM *temp = start;
	ASSEM *ret = temp;
	ASSEM *fails = NULL;
	int numFails = 0;
	numTMP =0;
	numTMP = temp->numTMP;
	off = -4;
	int offsets[numTMP];
	int i;
	for(i = 0; i < numTMP; i++){
		offsets[i] = -1;
	}
	temp = start;
	while(temp != NULL){
		if(temp->block == 1){
			off = -4;
		}
		if(temp->kind == CallK){
			temp = temp->next;
			continue;
		}
		if (temp->left != NULL){
			VAL *l = temp->left;
			if(temp->left->kind == RegK){

				if(temp->left->reg->precolor == NpreK){
					index = temp->left->reg->number;
					if(offsets[index] != -1){
						temp->left = makeVALrreg();
						temp->left->rreg->kind = ebpK;
						temp->left->rreg->offset = offsets[index];
					} else {
					}
				} else {
					if(temp->left->reg->precolor == EaxK){
						temp->left->kind = RRegK;
						temp->left->rreg = makeREG(NULL);
						temp->left->rreg->kind = eaxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;
					}else if(temp->left->reg->precolor == EbxK){
						temp->left->kind = RRegK;
						temp->left->rreg = makeREG(NULL);
						temp->left->rreg->kind = ebxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;				
					}else if(temp->left->reg->precolor == EcxK){
						temp->left->kind = RRegK;
						temp->left->rreg = makeREG(NULL);
						temp->left->rreg->kind = ecxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;			
					}
				}
				if(l->reg->deref == 1){
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = eaxK;
					ASSEM *cur = pmakeMOV(temp->left, makeVALrreg());
					cur->right->rreg->kind = eaxK;
					temp->left = makeVALrreg();
					temp->left->rreg->kind = eaxK;
					temp->left->rreg->isOffset = 1;
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = eaxK;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					cur->next = temp;
					temp->previous = cur;
					ASSEM *next = temp->next;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
					
				}
			}else if(temp->left->kind == ScopeK){
				int index = temp->left->reg->number;
				VAL *v = temp->left->s->address;
				int index2 = v->reg->number;
				if(offsets[index2]!=-1){
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = eaxK;
					ASSEM *cur = pmakeADD(makeVALconst(offsets[index2]),
temp->left);
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->right= makeVALrreg();
					cur->right->rreg->kind = ebpK;
					cur->right->rreg->offset = offsets[index];		
					cur->previous = save;
					ASSEM *mov = pmakeMOV(cur->right, makeVALrreg());
					mov->right->rreg->kind = eaxK;
					cur->next = mov;
					mov->previous = cur;
					mov->next = temp;
					temp->previous = mov;
					temp->left= makeVALrreg();
					temp->left->rreg->kind = eaxK;
					temp->left->rreg->isOffset = 1;
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = eaxK;
					ASSEM *next = temp->next;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
				}else{
					SYMBOL *s = temp->left->s;
					ASSEM *prev = temp->previous;
					temp->left= makeVALrreg();
					temp->left->rreg->kind = ebpK;
					temp->left->rreg->offset =offsets[index];
					
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = edxK;
					ASSEM *cur = pmakeADD(makeVALconst(offsets[index2]),
temp->left);
					cur->left->s = s;
					cur->left->kind = ScopeK;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					ASSEM *mov = pmakeMOV(temp->left, makeVALrreg());
					mov->right->rreg->kind = edxK;
					cur->next = mov;
					mov->previous = cur;
					mov->next = temp;
					temp->previous = mov;
					temp->left->kind = RRegK;
					temp->left= makeVALrreg();
					temp->left->rreg->kind = edxK;
					temp->left->rreg->isOffset = 1;
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = edxK;
					ASSEM *next = temp->next;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
					
					off -= inc; 
					if(fails != NULL){
						fails->nextFail = cur;
						cur->prevFail = fails;
						fails = cur;
						numFails++;
					}else{
						cur->prevFail = fails;
						fails = cur; 
						numFails++;
					}
					
					
				} 	
			}
			
		}
		if (temp->right != NULL){
			VAL *r = temp->right;
			if(temp->right->kind == RegK){
				if(temp->right->reg->precolor == NpreK){
					index = temp->right->reg->number;
					if(offsets[index] != -1){
						temp->right = makeVALrreg();
						temp->right->rreg->kind = ebpK;
						temp->right->rreg->offset = offsets[index];
					} else {
						temp->right= makeVALrreg();
						temp->right->rreg->kind = ebpK;
						temp->right->rreg->offset = off;		  
						offsets[index] = off;

						off -= inc;					}
				} else {
					if(temp->right->reg->precolor == EaxK){
						temp->right->kind = RRegK;
						temp->right->rreg = makeREG(NULL);
						temp->right->rreg->kind = eaxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;
					}else if(temp->right->reg->precolor == EbxK){
						temp->right->kind = RRegK;
						temp->right->rreg = makeREG(NULL);
						temp->right->rreg->kind = ebxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;				
					}else if(temp->right->reg->precolor == EcxK){
						temp->right->kind = RRegK;
						temp->right->rreg = makeREG(NULL);
						temp->right->rreg->kind = ecxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;			
					}
				}
				if(r->reg->deref == 1){
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = eaxK;
					ASSEM *cur = pmakeMOV(temp->right, makeVALrreg());
					cur->right->rreg->kind = eaxK;
					ASSEM *mov = pmakeMOV(temp->left, makeVALrreg());
					mov->right->rreg->kind = ebxK;
					temp->right = makeVALrreg();
					temp->right->rreg->kind = eaxK;
					temp->right->rreg->isOffset = 1;
					temp->left = makeVALrreg();
					temp->left->rreg->kind = ebxK;
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = eaxK;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					cur->next = mov;
					mov->previous = cur;
					mov->next = temp;					
					temp->previous = mov;		
					ASSEM *next = temp->next;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
				}
			}else if(temp->right->kind == ScopeK){
				VAL *v = temp->right->s->address;
				index = v->reg->number;
				
				if(offsets[index]!=-1){
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = eaxK;					
					ASSEM *cur = pmakeADD(makeVALconst(offsets[index]),
temp->right);
					cur->left->kind = ScopeK;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					ASSEM *mov = pmakeMOV(temp->right, makeVALrreg());
					mov->right->rreg->kind = eaxK;
					cur->next = mov;
					mov->previous = cur;
					mov->next = temp;
					temp->previous = cur;
					temp->right->kind = RRegK;
					temp->right= makeVALrreg();
					temp->right->rreg->kind = eaxK;
					temp->right->rreg->isOffset = 1;	
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = eaxK;
					ASSEM *next = temp->next;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
				}else{
					if(fails != NULL){
						fails->nextFail = temp;
						temp->prevFail = fails;
						fails = temp;
						numFails++;
					}else{
						temp->prevFail = fails;
						fails = temp; 
						numFails++;
					}
				} 	
				
			}
		}
		

		
		if(temp->kind == SubK){
			if (temp->left->kind == RRegK && temp->right->kind == RRegK) {
				if(temp->left->rreg->kind == ebpK && temp->right->rreg->kind ==
 ebpK){
					ASSEM *next = temp->next;
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(makeVALrreg());
					save->left->rreg->kind = eaxK;
					ASSEM *cur = pmakeMOV(temp->right, makeVALrreg());
					cur->right->rreg->kind = eaxK;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					VAL *oldT = temp->right;
					temp->right = cur->right;		
					cur->next = temp;				
					temp->previous = cur;
					ASSEM *last = pmakeMOV(cur->right, oldT);
					ASSEM *restore = pmakePOP(makeVALrreg());
					restore->left->rreg->kind = eaxK;
					last->next = restore;
					temp->next = last;
					last->previous = temp;
					restore->previous = last;
					restore->next = next;
					next->previous = restore;
					
				} 
			}
		} else if (temp->kind == MulK) {

			if (temp->left->kind == RRegK && temp->right->kind == RRegK) {
				if(temp->left->rreg->kind == ebpK && temp->right->rreg->kind ==
 ebpK){
					ASSEM *prev = temp->previous;
					VAL *myEax = makeVALrreg();
					myEax->rreg->kind = eaxK;
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					
					ASSEM *save = pmakePUSH(myEax);
					ASSEM *save2 = pmakePUSH(myEbx);
					ASSEM *lInEax = pmakeMOV(temp->left,myEax);
					ASSEM *rInEbx = pmakeMOV(temp->right,myEbx);
					ASSEM *mulThem = pmakeMOV(myEbx,myEax);
					ASSEM *restore = pmakePOP(myEbx);
					ASSEM *restore2 = pmakePOP(myEax);
					ASSEM *next = temp->next;
					
					mulThem->kind = MulK;
					temp->kind = MovK;
					temp->left = myEax;
					
					prev->next = save;
					save->next = save2;
					save2->next = lInEax;
					lInEax->next = rInEbx;
					rInEbx->next = mulThem;
					mulThem->next = temp;
					temp->next = restore;
					restore->next = restore2;
					restore2->next = next;
					
					next->previous = restore2;
					restore2->previous = restore;
					restore->previous = temp;
					temp->previous = mulThem;
					mulThem->previous = rInEbx;
					rInEbx->previous = lInEax;
					lInEax->previous = save2;
					save2->previous = save;
					save->previous = prev;
					
					
				}
			}else if(temp->left->kind == ConstK && temp->right->kind == RRegK){
				if(temp->right->rreg->kind == ebpK){
					ASSEM *prev = temp->previous;
					ASSEM *next = temp->next;
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					VAL *myEax = makeVALrreg();
					myEax->rreg->kind = eaxK;
					ASSEM *save = pmakePUSH(myEax);
					ASSEM *save2 = pmakePUSH(myEbx);
				 	ASSEM *rInEbx = pmakeMOV(temp->right,myEbx);
					temp->right = myEbx;
					prev->next = save;
					save->previous = prev;
					save->next = save2;
					save2->previous = save;
					save2->next = rInEbx;
					rInEbx->previous = save2;
					rInEbx->next = temp;
					temp->previous = rInEbx;
					ASSEM *last1 = pmakeMOV(myEbx,rInEbx->left);
					ASSEM *restore = pmakePOP(myEbx);
					ASSEM *restore2 = pmakePOP(myEax);
					last1->next = restore;
					temp->next = last1;
					last1->previous = temp;
					restore->next = restore2;
					restore2->previous = restore;
					restore2->next = next;
					next->previous = restore2;
					
					
				}
				
			}
		} else if (temp->args == 2 ){
			if (temp->left->kind == RRegK && temp->right->kind == RRegK) {
				if(temp->left->rreg->kind == ebpK && temp->right->rreg->kind ==
 ebpK){					
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					VAL *myEax = makeVALrreg();
					myEax->rreg->kind = eaxK;
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(myEax);
					ASSEM *next = temp->next;
					VAL *oldT = temp->left;
					temp->left = makeVALrreg();
					temp->left->rreg->kind = eaxK;
					ASSEM *inbetween = pmakeMOV(oldT, temp->left);
					ASSEM *restore = pmakePOP(myEax);
					prev->next = save;
					save->previous = prev;
					save->next = inbetween;
					inbetween->previous = save;
					inbetween->next = temp;
					temp->previous = inbetween;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
				} else if(temp->left->rreg->isOffset == 1 && 
temp->right->rreg->kind == ebpK)
				{
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(myEbx);
					ASSEM *next = temp->next;
					ASSEM *cur = pmakeMOV(temp->left, myEbx);
					ASSEM *restore = pmakePOP(myEbx);
					temp->left = cur->right;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					cur->next = temp;
					temp->previous = cur;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
					
					
				}					
			}else if(temp->left->kind == LabelK && temp->right->kind == RRegK){
				if(temp->right->rreg->kind == ebpK){
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(myEbx);
					ASSEM *next = temp->next;
					ASSEM *cur = pmakeMOV(temp->left, myEbx);
					ASSEM *restore = pmakePOP(myEbx);
					temp->left = cur->right;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					cur->next = temp;
					temp->previous = cur;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
				}
				
				
			}else if(temp->right->kind == LabelK && temp->left->kind == RRegK){
				if(temp->left->rreg->kind == ebpK){
					VAL *myEbx = makeVALrreg();
					myEbx->rreg->kind = ebxK;
					ASSEM *prev = temp->previous;
					ASSEM *save = pmakePUSH(myEbx);
					ASSEM *next = temp->next;
					ASSEM *cur = pmakeMOV(temp->left, myEbx);
					ASSEM *restore = pmakePOP(myEbx);
					temp->left = cur->right;
					prev->next = save;
					save->previous = prev;
					save->next = cur;
					cur->previous = save;
					cur->next = temp;
					temp->previous = cur;
					temp->next = restore;
					restore->previous = temp;
					restore->next = next;
					next->previous = restore;
				}
			}
				else {
				if (temp->left->kind == RegK && temp->left->reg->precolor != 
NpreK) {
					if(temp->left->reg->precolor == EaxK){
						temp->left = makeVALrreg();
						temp->left->rreg = eaxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;
					}else if(temp->left->reg->precolor == EbxK){
						temp->left = makeVALrreg();
						temp->left->rreg = ebxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;				
					}else if(temp->left->reg->precolor == EcxK){
						temp->left = makeVALrreg();
						temp->left->rreg = ecxK;
						temp->left->rreg->isOffset = temp->left->reg->deref;			
					}	
				}
				if (temp->right->kind == RegK && temp->right->reg->precolor != 
NpreK) {
					if(temp->right->reg->precolor == EaxK){
						temp->right = makeVALrreg();
						temp->right->rreg = eaxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;
					}else if(temp->right->reg->precolor == EbxK){
						temp->right = makeVALrreg();
						temp->right->rreg = ebxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;				
					}else if(temp->right->reg->precolor == EcxK){
						temp->right = makeVALrreg();
						temp->right->rreg = ecxK;
						temp->right->rreg->isOffset = temp->right->reg->deref;			
					}
				}
			}
		}

	temp = temp->next;VAL *myEbx = makeVALrreg();
			myEbx->rreg->kind = ebxK;
  }
  temp = fails;
  while(temp != NULL){
    if(temp->left != NULL){
      if(temp->left->kind == ScopeK){
	    VAL *v = (VAL*)temp->left->s->address;
	    index = v->reg->number;
		if(offsets[index]!=-1){
			temp->left->kind = ConstK;
			temp->left->Const = offsets[index];
		}else{
		  
		}
      
	}
      }
     if(temp->right != NULL){
      if(temp->right->kind == ScopeK){
	    VAL *v = temp->right->s->address;
	    	index = v->reg->number;
			int index2 = temp->right->reg->number;
		if(offsets[index]!=-1){
			VAL *myEbx = makeVALrreg();
			myEbx->rreg->kind = ebxK;
			VAL *myEax = makeVALrreg();
			myEax->rreg->kind = eaxK;
		  ASSEM *prev = temp->previous;
		  ASSEM *next = temp->next;
		  temp->right = makeVALrreg();
		  temp->right->rreg->kind = ebpK;
		  temp->right->rreg->offset = offsets[index2];
		  ASSEM *save2 = pmakePUSH(myEax);
		  ASSEM *save = pmakePUSH(myEbx);
			ASSEM *cur = pmakeADD(makeVALconst(offsets[index]),temp->right);
			prev->next = save;
			save->previous = prev;
			save->next = save2;
			save2->previous = save;
			save2->next = cur;
			cur->previous = save2;
			ASSEM *mov = pmakeMOV(temp->left, myEbx);
			cur->next = mov;
			mov->previous = cur;			
			ASSEM *mov2 = pmakeMOV(cur->right, myEax);
			ASSEM *restore = pmakePOP(myEbx);
			ASSEM *restore2 = pmakePOP(myEax);
			mov->next = mov2;
			mov2->previous = mov;
			mov2->next = temp;
			temp->previous = mov2;
			temp->left = mov->right;
			temp->right->kind = RRegK;
			temp->right= makeVALrreg();
			temp->right->rreg->kind = eaxK;
			temp->right->rreg->isOffset = 1;
			temp->next = restore;
			restore->previous = temp;
			restore->next = restore2;
			restore2->previous = restore;
			restore2->next = next;
			next->previous = restore2;
		    numFails--;
		}else{
		  
		}
      
	}
      }
    
    temp = temp->prevFail;
    
  }


  return ret;
  
}



ASSEM *pmakeMOV(VAL *left, VAL *right){
  	ASSEM *assem = NEW(ASSEM);
	assem->kind = MovK;
	assem->left = left;
	assem->right = right;
	assem->args = 2;
	return assem;
  
}

ASSEM *pmakeADD(VAL *left, VAL *right){
  	ASSEM *assem = NEW(ASSEM);
	assem->kind = AddK;
	assem->left = left;
	assem->right = right;
	assem->args = 2;
	return assem;
  
}

ASSEM *pmakePUSH(VAL *left){
  	ASSEM *assem = NEW(ASSEM);
	assem->kind = PushK;
	assem->left = left;
	assem->right = NULL;
	assem->args = 1;
	return assem;
  
}

ASSEM *pmakePOP(VAL *left){
  	ASSEM *assem = NEW(ASSEM);
	assem->kind = PopK;
	assem->left = left;
	assem->right = NULL;
	assem->args = 1;
	return assem;
  
}

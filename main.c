#include "tree.h"
#include "pretty.h"
#include "weed.h"
#include "collect.h"
#include "semant.h"
#include "fprint.h"
#include "type.h"
#include <stdlib.h>
#include "push.h"
#include "peep.h"
#include "prints.h"

int lineno;
ASSEM *firstAssem;

void yyparse();

SymbolTable *collectTable;
BODY *theexpression;

int ret = 0;

void main(int argc, char *argv[])
{ 
	
	lineno = 1;
	yyparse();
	
	ret = getErrors();
	if(ret != 0){
		return -1;
	}
	
	printf("\ntime to weed!\n");
	weedINIT(theexpression);
	
	ret = getErrors();
	if(ret != 0){
		return -1;
	}
	
	printf("\nFinished weed, time to collect!\n");
	collectTable = collectINIT(theexpression);
	
	ret = getErrors();
	if(ret != 0){
		return -1;
	}
	
	printf("\nFinished collect, time to trans!\n");
	transBODY(collectTable, theexpression);
	
	ret = getErrors();
	if(ret != 0){
		return -1;
	}
	
	printf("\nFinished trans, time to type!\n");
	typeBODY(collectTable,theexpression);
	
	ret = getErrors();
	if(ret != 0){
		return -1;
	}
	
	printf("\nFinished type, time to print structure of program!\n");
	prettyBODY(theexpression);
	
	printf("\nFinished print, time to make intermediate representation!\n");
	firstAssem = assemINIT(theexpression,collectTable);
	
	if (argc >= 2) {
		if (strcmp(argv[1],"-l") == 0){
			INITlive(firstAssem);
			
		}
	}

	printf("\nFinished IR, time to convert to assembly!\n");
	firstAssem = pushALL(firstAssem);

	printf("\nFinished converting, time to peep!\n");
	firstAssem = peepAndDestroy(firstAssem);

	printf("\nFinished peep, time to print to file!\n");
	fPrintStart(firstAssem);	
	
	printf("\nFinished printing to file!\n");
	__fpurge(stdout);
	prints();

	
	
	return 0;
	
} 

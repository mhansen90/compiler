#include "mem.h"
typedef struct BITSET{
  char *bits;
  int size;  
}BITSET;

BITSET *initBITSET( int totalbits);
int setBIT(BITSET *set, int bitPos);
int clearBIT(BITSET *set, int bitPos);
int unionBit(BITSET *target, BITSET *other);
int diffBit(BITSET *target, BITSET *other);
int isBitSet(BITSET *set, int bitPos);
int *getBits(BITSET *set);
void printBits(BITSET *set);
int getBitSum(BITSET *set);
int clearBITSET(BITSET *set);

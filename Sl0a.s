        .text
 
form: 
        .string "%d\n" 
 
formFalse: 
        .string "false\n"
 
formTrue: 
        .string "true\n"
 
        .data
 
heap: 
        .long 0
 
retVal: 
        .long 0
 
.globl main 
main: 
        movl %esp,retVal 
        pushl %ebp 
        movl %esp,%ebp 
        movl $4000000,(%esp) 
        call malloc 
        movl %eax,heap 
        movl %ebp,%esp 
        subl $48,%esp 
        movl $0,-4(%ebp) 
        movl $0,-8(%ebp) 
        movl $4,-12(%ebp) 
        pushl %eax 
        movl -12(%ebp),%eax 
        movl %eax,-4(%ebp) 
        popl %eax 
        movl $2,-16(%ebp) 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-8(%ebp) 
        popl %eax 
        pushl %eax 
        movl -4(%ebp),%eax 
        movl %eax,-20(%ebp) 
        popl %eax 
        pushl -20(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        movl -8(%ebp),%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $3,%eax 
        pushl %eax 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $0,%eax 
        leave 
        ret 

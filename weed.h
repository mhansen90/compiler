#include "tree.h"
#include "error.h"

void weedINIT(BODY *e);

void weedFUNC(FUNC *e);
void weedHEAD(HEAD *e);
void weedTAIL(TAIL *e);
void weedTYPE(TYPE *e);

void weedPARDL(PARDL *e);
void weedVARDL(VARDL *e);
void weedVART(VART *e);
void weedBODY(BODY *e);

void weedDECL(DECL *e);

void weedSTATEL(STATEL *e, int a);
void weedSTATE(STATE *e, int a);

void weedOPTL(OPTL *e);
void weedOPTE(OPTE *e, int a);
void weedVARI(VARI *e);

void weedEXP(EXP *e);
void weedTERM(TERM *e);
void weedALIST(ALIST *e);
void weedELIST(ELIST *e);

int has_return_statel(STATEL *statel);
int has_return_state(STATE *state);

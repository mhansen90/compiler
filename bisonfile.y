%{
#include <stdio.h>
#include "tree.h"
#include "error.h"

extern char *yytext;
extern BODY *theexpression;

void yyerror() {
	EM_error3(0, EM_SYNTAX, yytext);
}
%}

%union {
   int numconst;
   int booleanconst;
   char *stringconst;
   struct EXP *exp;
   struct TERM *term;
   struct ALIST *alist;
   struct ELIST *elist;
   struct VARI *vari;
   struct STATE *state;
   struct STATEL *statel;
   struct OPTL *optl;
   struct OPTE *opte;
   struct FUNC *func;
   struct HEAD *head;
   struct TAIL *tail;
   struct TYPE *type;
   struct PARDL *pardl;
   struct VARDL *vardl;
   struct VART *vart;
   struct BODY *body;
   struct DECLL *decll;
   struct DECL *decl;
}

%token <numconst> tNUMCONST
%token <stringconst> tIDENTIFIER 
%token <booleanconst> tBOOLEANCONST
%token tNULL
%token tRETURN
%token tWRITE
%token tALLOCATE
%token tIF
%token tTHEN
%token tWHILE
%token tDO
%token tBREAK
%token tCONTINUE
%token tOF
%token tLENGTH
%token tELSE
%token tFUNC
%token tEND
%token tINT
%token tVAR
%token tBOOL
%token tARRAY
%token tRECORD
%token tTYPE
%token tFOR
%token UMINUS
%token OR
%token ABS

%type <exp> exp
%type <term> term
%type <alist> alist
%type <elist> elist
%type <vari> vari
%type <state> state
%type <statel> statel
%type <func> func
%type <head> head
%type <tail> tail
%type <type> type
%type <vardl> vardl
%type <vart> vart
%type <body> body program
%type <decll> decll
%type <decl> decl

%start program

%right tTHEN tELSE
%right '|'
%left ABS
%left '&'
%left '=''!'
%left '<' '>'
%left '+' '-'
%left '*' '/'
%left UMINUS


%% 
program: body
         { theexpression = $1;}
;

exp : '-' exp %prec UMINUS
      {$$ = makeEXPminus(makeEXPterm(makeTERMnumconst(0)),$2);}
    | exp '*' exp
      {$$ = makeEXPtimes($1,$3);}
    | exp '/' exp
      {$$ = makeEXPdiv($1,$3);}
    | exp '+' exp
      {$$ = makeEXPplus($1,$3);}
    | exp '-' exp
      {$$ = makeEXPminus($1,$3);}
    | term 
      {$$ = makeEXPterm($1);}
	| exp '=''=' exp
	  {$$ = makeEXPcmp($1,$4);}
    | exp '!''=' exp
	  {$$ = makeEXPncmp($1,$4);}
	| exp '>' exp
	  {$$ = makeEXPhigh($1,$3);}
	| exp '<' exp
	  {$$ = makeEXPlow($1,$3);}
	| exp '>''=' exp
	  {$$ = makeEXPhigheq($1,$4);}
	| exp '<''=' exp
	  {$$ = makeEXPloweq($1,$4);}
	| exp '&''&' exp
	  {$$ = makeEXPand($1,$4);}
	| exp '|' '|' exp
	  {$$ = makeEXPor($1,$4);}   
;

term : tIDENTIFIER '(' alist ')'
      {$$ = makeTERMid_act($1, $3);}
    | tNULL
      {$$ = makeTERMnull();}
    | tNUMCONST
      {$$ = makeTERMnumconst($1);}
    | tBOOLEANCONST
      {$$ = makeTERMbooleanconst($1);}
    | '!' term
      {$$ = makeTERMnot($2);}
    | '(' exp ')'
      {$$ = makeTERMp($2);} 
    | vari 
      {$$ = makeTERMvari($1);} 
    | '|' exp '|' %prec ABS
      {$$ = makeTERMb($2); } 
;    
      
vari : tIDENTIFIER
	{$$ = makeVARIid($1);}
    | vari '[' exp ']'
	{$$ = makeVARIvari_exp($1, $3);}
    | vari '.' tIDENTIFIER
	{$$ = makeVARIvari_id($1, $3);}
;
      
alist : elist
	{$$ = makeALISTelist($1);} 
	|  /* epsilon */
	{$$ = NULL;} 
;           
elist : exp
	{$$ = makeELISTexp($1);}
	|elist ',' exp
	{$$ = makeELISTelist($1,$3);}
;

state : tRETURN exp ';'
	{$$ = makeSTATEreturn($2);}
	| tWRITE exp ';'
	{$$ = makeSTATEwrite($2);}
	| tALLOCATE vari tOF tLENGTH exp ';'
	{$$ = makeSTATEallocate($2, makeOPTLlength($5));}
	| tALLOCATE vari';'
	{$$ = makeSTATEallocate($2, NULL);}
	| vari '=' exp  ';'
	{$$ = makeSTATEvari_exp($1, $3);}
	| tIF exp tTHEN state tELSE state
	{$$ = makeSTATEif_then($2, $4, makeOPTEelse($6));}
	| tIF exp tTHEN state
	{$$ = makeSTATEif_then($2, $4, NULL);}
	| tWHILE exp tDO state
	{$$ = makeSTATEwhile_do($2, $4);}
	| '{' statel '}'
	{$$ = makeSTATEstatel($2);}
	| tBREAK ';'
	{$$ = makeSTATEbreak();}
	| tCONTINUE ';'
	{$$ = makeSTATEcontinue();}	
	| tFOR '(' vart ';' state exp ';' state ')' state
	{$$ = makeSTATEfor($3, $5, $6, $8, $10);}
	| vari '+' '+' ';'
	{$$ = makeSTATEinc($1);}
	| vari '-' '-' ';'
	{$$ = makeSTATEdec($1);}
	| vari '+' '=' exp ';'
	{$$ = makeSTATEsplus($1, $4);}
	| vari '-' '=' exp ';'
	{$$ = makeSTATEsminus($1, $4);}
	| vari '*' '=' exp ';'
	{$$ = makeSTATEstimes($1, $4);}
	| vari '/' '=' exp ';'
	{$$ = makeSTATEsdiv($1, $4);}
;

statel : state
	{$$ = makeSTATELstate($1);}
	| statel state
	{$$ = makeSTATELstatel($1, $2);}
;

func : head body tail
	{$$ = makeFUNChbt($1, $2, $3);}
;

head : tFUNC tIDENTIFIER '(' vardl ')' ':' type
	{$$ = makeHEADfunc($2, makePARDLvardl($4), $7);}
	| tFUNC tIDENTIFIER '('  ')' ':' type
	{$$ = makeHEADfunc($2, NULL, $6);}
;

tail : tEND tIDENTIFIER
	{$$ = makeTAILend($2);}
;

type : tIDENTIFIER
	{$$ = makeTYPEid($1);}
	| tINT
	{$$ = makeTYPEint();}
	| tBOOL
	{$$ = makeTYPEbool();}
	| tARRAY tOF type
	{$$ = makeTYPEarray($3);}
	| tRECORD tOF '{' vardl '}'
	{$$ = makeTYPErecord($4);}
;


vardl :
	vardl ',' vart
	{$$ = makeVARDLvardl($1, $3);}
	| vart
	{$$ = makeVARDLvart($1);}
;

vart : tIDENTIFIER ':' type
	{$$ = makeVARTid($1, $3);}
;

body : decll statel
	{$$ = makeBODYds($1, $2);}
;

decll : decll decl
	{$$ = makeDECLLdecll($1, $2);}
	| /* epsilon */
	{$$ = NULL;}
;

decl : tTYPE tIDENTIFIER '=' type ';'
	{$$ = makeDECLtype($2, $4);}
	| func
	{$$ = makeDECLfunc($1);}
	| tVAR vardl ';'
	{$$ = makeDECLvar($2);}
;
%%

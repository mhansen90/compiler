#include "assem.h"

typedef struct NODE{
	int number;
  struct BITSET *connections;
  int color;
  struct NODE *previous;
  int degree;
  int push;
  int precolor;
  
}NODE;

ASSEM *INITlive(ASSEM *start);
int FLOWlive(ASSEM *instr[], int start);
int DATAlive(ASSEM *instr[], int start);

int INITgraph(ASSEM *instr[], int start);
int SIMPLIFYgraph(NODE *temps[]);
int ASSIGNreg( NODE *temps[], ASSEM *instr[], int start);
int findSUCC(LABEL *lb,ASSEM *instr[], int start);
int getReg(int color);
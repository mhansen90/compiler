void EM_error(int lineno, int message);

void EM_error3(int lineno, int message, char *name);

int getErrors();

#define EM_INT 0
#define EM_BOOL 1
#define EM_STRING 2
#define EM_BMISS 3
#define EM_ARRAY 4
#define EM_BOTH 5
#define EM_ARRAYRECORD 6
#define EM_IDINTBOOL 7
#define EM_NULLPOINTER 8
#define EM_TYPEMISS 9
#define EM_OPTL 10
#define EM_FEWARGS 11
#define EM_MANYARGS 12
#define EM_RET 13
#define EM_DIV0 14
#define EM_FUNCIDMATCH 15
#define EM_NOTWHILE 16
#define EM_NOTEQUAL 17
#define EM_RETS 18

#define EM_MISSRECORDVAR 19
#define EM_MISSID 20
#define EM_NOTDECLAREDYET 21
#define EM_INIT 22
#define EM_SYNTAX 23

#include <stdio.h>
#include "weed.h"

/*
Starts the weed and checks if there is a return statement anywhere in main.
*/
void weedINIT(BODY *e){
    int i = has_return_statel(e->statel);
    if (i > 0) {
		EM_error(e->lineno,EM_RETS);
    }
    if (e->decll != NULL){        
        weedDECL(e->decll);
    }
    weedSTATEL(e->statel,0);
}

/*
Checks if the state includes a return statement. If the statement is a if/else
it checks both and return true if both ends with a return. If it is a
statementlist it will check if the last statement is a return.
*/
int has_return_state(STATE *state ){
	if (state == NULL){
		printf("null encounter \n");
		return 0;
	} else {
		if (state->kind == returnK){
			return 1;
		} 
		if (state->kind == statelK){
			return has_return_statel(state->val.statel);
		}
		if (state->kind == ifK) {
			int i;
			i = has_return_state(state->val.i.state);
			if (state->val.i.opte == NULL) {
				return 0;
			}
			i = i+has_return_state(state->val.i.opte->state);
			if (i == 2) {
				return 1;
			}
		}
		return 0;
	}
}

/*
Checks if there is a return state in the last line, but return 2 if it is 
not in the last line.
*/

int has_return_statel(STATEL *statel) {
	int i;
	int z = 0;
	if (statel->kind == stateK){
		i = has_return_state(statel->val.state);		
	} else {
		i = has_return_state(statel->val.s.state);	
		z = has_return_statel(statel->val.s.statel);
	}	
	if (i == 1) {
		return 1;
	} 
	if (z > 0) {
		return 2;
	}
	return 0;
}
/*
Checks if the function has identical head and end ids and if there is a
return in the last line.
*/
void weedFUNC(FUNC *e){
    HEAD *head = e->head;
    TAIL *tail = e->tail;
    if(strcmp(head->id,tail->id) != 0){
		EM_error(e->lineno, EM_FUNCIDMATCH);	
    }
    
    int i = has_return_statel(e->body->statel);
    if (i != 1) {
		EM_error(e->lineno, EM_RET);
	}
	
    
    weedHEAD(e->head);
    weedBODY(e->body);
    weedTAIL(e->tail);
}
void weedHEAD(HEAD *e){

	if (e->pardl != NULL) {
		weedPARDL(e->pardl);
	}
    weedTYPE(e->type);
}
void weedTAIL(TAIL *e){
}
void weedTYPE(TYPE *e){
switch (e->kind) {
    case TidK:
         break;
    case TintK:
         break;
    case TboolK:
         break;
    case TarrayK:
         weedTYPE(e->val.type);
         break;
    case TrecordK:
         weedVARDL(e->val.vardl);
         break;
    }
}

void weedPARDL(PARDL *e){
    weedVARDL(e->vardl);
}
void weedVARDL(VARDL *e){
switch (e->kind) {
    case vartK:
         weedVART(e->val.vart);
         break;
    case varcl_vartK:
         weedVARDL(e->val.s.vardl);
         weedVART(e->val.s.vart);
         break;
    }
}
void weedVART(VART *e){
    weedTYPE(e->type);
}
void weedBODY(BODY *e){
    if (e->decll != NULL){        
        weedDECL(e->decll);
    }
    weedSTATEL(e->statel,0);
}
void weedDECL(DECL *e){
    if (e->next != NULL){
         weedDECL(e->next);
         }
switch (e->kind) {
    case typeK:
         weedTYPE(e->val.s.type);
         break;
    case funcK:
         weedFUNC(e->val.func);
         break;
    case varK:
         weedVARDL(e->val.vardl);
         break;
    }
}
void weedSTATEL(STATEL *e, int a){
switch (e->kind) {
    case statel_stateK: 
         weedSTATEL(e->val.s.statel,a);
         weedSTATE(e->val.s.state,a);
         break;
    case stateK:
         weedSTATE(e->val.state,a);
         break;
    }
}

/*
Checks if break and continue is in a while loop by passing true to all 
statement after a while-loop
*/
void weedSTATE(STATE *e, int a){
switch (e->kind) {
    case returnK: 
         weedEXP(e->val.exp);
         break;
    case writeK:
         weedEXP(e->val.exp);
         break;
    case allocateK:
         weedVARI(e->val.a.vari);
	 	 if (e->val.a.optl != NULL) {
         	weedOPTL(e->val.a.optl);
		 }
         break;
    case equalK:
         weedVARI(e->val.e.vari);
         weedEXP(e->val.e.exp);
         break;
    case ifK:
         weedEXP(e->val.i.exp);
         weedSTATE(e->val.i.state,a);
         if (e->val.i.opte != NULL){
            weedOPTE(e->val.i.opte,a);
         }
         break;
    case whileK:
         weedEXP(e->val.w.exp);
         weedSTATE(e->val.w.state,1);
         break;
    case statelK:
         weedSTATEL(e->val.statel,a);
         break;
    case breakK:
		 if (a == 0) {
			EM_error(e->lineno, EM_NOTWHILE);
		 }
         break;
    case continueK:
		 if (a == 0) {
			EM_error(e->lineno, EM_NOTWHILE);
		 }
         break;
    case forK:
		 weedVART(e->val.f.vart);
         weedSTATE(e->val.f.state1,a);
         weedEXP(e->val.f.exp);
         weedSTATE(e->val.f.state2,a);
         weedSTATE(e->val.f.state3,a);
    	 break;
	case incK:
		 weedVARI(e->val.vari);
		 break;
	case decK:
		 weedVARI(e->val.vari);
		 break;
	}
}

void weedOPTL(OPTL *e){
    if (e->exp == NULL){            
        return;
    }
    weedEXP(e->exp);
}

void weedOPTE(OPTE *e, int a){
    if (e->state == NULL){
        return;
    }
    weedSTATE(e->state, a);
}

void weedVARI(VARI *e){
switch (e->kind) {
    case idK:
         break;
    case vari_expK:
         weedVARI(e->val.e.vari);
         weedEXP(e->val.e.exp);
         break;
    case vari_idK:
         weedVARI(e->val.i.vari);
         break;
    }
}

void weedEXP(EXP *e){
 switch (e->kind) {
    case timesK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
    case divK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break; 
    case plusK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;    
    case minusK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;    
    case termK:
        weedTERM(e->val.term);
        break;
    case cmpK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
    case ncmpK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break; 
    case highK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;    
    case lowK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;    
    case higheqK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
    case loweqK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
    case andK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
    case orK:
         weedEXP(e->val.s.left);
         weedEXP(e->val.s.right);
         break;
  } 
} 
void weedTERM(TERM *e){
  switch (e->kind) {
    case variK:
        weedVARI(e->val.vari);
        break;
    case id_actK:
	if (e->val.s.alist != NULL) {
        	weedALIST(e->val.s.alist);
	}
        break;
    case numconstK: 
        break;
    case booleanconstK:
	if (e->val.num==1) {
	} else {
	}

        break; 
    case nullK:
        break; 
    case notK:
        weedTERM(e->val.term);
        break;
    case parK:
        weedEXP(e->val.exp);
        break;
    case barK:
        weedEXP(e->val.exp);
        break;
  }
}
void weedALIST(ALIST *e){
    weedELIST(e->elist);
}
void weedELIST(ELIST *e){
  switch (e->kind) {
    case expK:
        weedEXP(e->val.exp);
        break;
    case elist_expK: 
        weedELIST(e->val.s.elist);
        weedEXP(e->val.s.exp);
        break;
  }
}


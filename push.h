#include "assem.h"
#define inc 4
ASSEM *pushALL(ASSEM *start);
ASSEM *pmakeMOV(VAL *left, VAL *right);
ASSEM *pmakeADD(VAL *left, VAL *right);	
ASSEM *pmakePUSH(VAL *left);
ASSEM *pmakePOP(VAL *left);
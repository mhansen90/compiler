#include <stdio.h>
#include "type.h"


/*
 * Check that the return type of a function matches the type it is
 * declared as.
 */
int return_state(STATE *state, HEAD *e ){
	if (state == NULL){
		EM_error(e->lineno, EM_NULLPOINTER);
		return 0;
	} else {
		if (state->kind == returnK){
			if (state->val.exp->type->kind != e->type->kind){
				EM_error(state->lineno, EM_TYPEMISS);
			}
			return 1;
		} 
		if (state->kind == statelK){
			return return_statel(state->val.statel, e);
		}
		if (state->kind == ifK) {
			int i = return_state(state->val.i.state, e);
			if (state->val.i.opte == NULL) {
				return 0;
			}
			i = i+return_state(state->val.i.opte->state, e);
			if (i == 2) {
				return 1;
			}
		}
		return 0;
	}
}

int return_statel(STATEL *statel, HEAD *e) {
	if (statel->kind == stateK){
		int i = return_state(statel->val.state, e);
		return i;		
	} else {
		int i = return_state(statel->val.s.state,e);
		if (i == 1) {
			return i;
		}
		i = return_statel(statel->val.s.statel, e);
		return i;		
	}	
}
void typeFUNC(FUNC *e){	
	SymbolTable *venv = e->table;
	
	return_statel(e->body->statel,e->head);

	typeHEAD(venv,e->head);
	typeBODY(venv,e->body);
	typeTAIL(venv,e->tail);
}




void typeHEAD(SymbolTable *venv,HEAD *e){

	if (e->pardl != NULL) {
		typePARDL(venv,e->pardl);
	}
	typeTYPE(venv,e->type);
}
void typeTAIL(SymbolTable *venv,TAIL *e){
}
void typeTYPE(SymbolTable *venv,TYPE *e){
	SYMBOL *tempS;
switch (e->kind) {
    case TidK:
		tempS = getSymbol(venv, e->val.id);
		if (tempS->var == 1){
			EM_error(e->lineno, EM_NULLPOINTER);
		}
         break;
    case TintK:
         break;
    case TboolK:
         break;
    case TarrayK:
		typeTYPE(venv,e->val.type);
        break;
    case TrecordK:
		typeVARDL(venv,e->val.vardl);
         break;
    }
}

void typePARDL(SymbolTable *venv,PARDL *e){
	typeVARDL(venv,e->vardl);
}
void typeVARDL(SymbolTable *venv,VARDL *e){
switch (e->kind) {
    case vartK:
		typeVART(venv,e->val.vart);
         break;
    case varcl_vartK:
		typeVARDL(venv,e->val.s.vardl);
		typeVART(venv,e->val.s.vart);
         break;
    }
}
void typeVART(SymbolTable *venv,VART *e){
	typeTYPE(venv,e->type);
}
void typeBODY(SymbolTable *venv,BODY *e){
    if (e->decll != NULL){   
		typeDECL(venv,e->decll);
    }
    typeSTATEL(venv,e->statel);
}
void typeDECL(SymbolTable *venv,DECL *e){	
    if (e->next != NULL){
		typeDECL(venv,e->next);
         }
switch (e->kind) {
    case typeK:
		typeTYPE(venv,e->val.s.type);
         break;
    case funcK:
		typeFUNC(e->val.func);
         break;
    case varK:
		typeVARDL(venv,e->val.vardl);
         break;
    }
}
void typeSTATEL(SymbolTable *venv,STATEL *e){	
	switch (e->kind) {
    case statel_stateK: 
		typeSTATEL(venv,e->val.s.statel);
		typeSTATE(venv,e->val.s.state);
         break;
    case stateK:
		typeSTATE(venv,e->val.state);
         break;
    }
}
/*
 * Case an error if the statements recieve an incorrect expression
 * or variable type. Fx. Allocate only expects records and arrays, and
 * write does not accept those.
 * 
 * the case EqualK has a special function for evaluating variable 
 * and expression equality.
 * 
 * 
 */
void typeSTATE(SymbolTable *venv,STATE *e){
	EXP *exp;
switch (e->kind) {
    case returnK: 
		typeEXP(venv,e->val.exp);
         break;
    case writeK:
		typeEXP(venv,e->val.exp);
		if (e->val.exp->type->kind == Ty_void) {
			EM_error(e->lineno, EM_NULLPOINTER);
		}
		if (e->val.exp->type->kind == Ty_record || 
e->val.exp->type->kind == Ty_array) {
			EM_error(e->lineno, EM_IDINTBOOL);
		}
         break;
    case allocateK:
		
		typeVARI(venv,e->val.a.vari);
		if(e->val.a.vari->type->kind != Ty_array && 
e->val.a.vari->type->kind != Ty_record){
			EM_error(e->lineno, EM_ARRAYRECORD);
		}
		if(e->val.a.optl == NULL && e->val.a.vari->type->kind == Ty_array){
		  EM_error(e->lineno, EM_OPTL);
		}
		if (e->val.a.optl != NULL) {
			typeOPTL(venv,e->val.a.optl);
		}
         break;
    case equalK:
		typeVARI(venv,e->val.e.vari);
		typeEXP(venv,e->val.e.exp);
		int check;
		check = compareTypes(e->val.e.vari->type, e->val.e.exp->type);
		
		if(check != 1){
		  EM_error(e->lineno,EM_TYPEMISS);
		}
         break;
    case ifK:
		exp = e->val.i.exp;
		typeEXP(venv,e->val.i.exp);
		if(exp->type->kind != Ty_bool){
			EM_error(e->lineno, EM_BOOL);
		}
		typeSTATE(venv,e->val.i.state);
         if (e->val.i.opte != NULL){
			 typeOPTE(venv,e->val.i.opte);
         }
         break;
    case whileK:		
		typeEXP(venv,e->val.w.exp);
		if(e->val.w.exp->type->kind != Ty_bool){
			EM_error(e->lineno, EM_BOOL);
		}
		typeSTATE(venv,e->val.w.state);
         break;
    case statelK:
		typeSTATEL(venv,e->val.statel);
         break;
	case breakK:
		break;
	case continueK:
		break;
    case forK:
		typeVART(e->table,e->val.f.vart);
		if(e->val.f.vart->type->kind != Ty_int){
			EM_error(e->lineno, EM_INT);
		}
		typeSTATE(e->table,e->val.f.state1);
		if(e->val.f.state1->kind != equalK){
			EM_error(e->lineno, EM_NOTEQUAL);
		}
		typeEXP(e->table,e->val.f.exp);
		if(e->val.f.exp->type->kind != Ty_bool){
			EM_error(e->lineno, EM_BOOL);
		}
		typeSTATE(e->table,e->val.f.state2);
		typeSTATE(e->table,e->val.f.state3);
		break;
	case incK:
		 typeVARI(venv, e->val.vari);
		 if(e->val.vari->type->kind != Ty_int){
			EM_error(e->lineno, EM_INT);
		 }
		 break;
	case decK:
		 typeVARI(venv, e->val.vari);
		 if(e->val.vari->type->kind != Ty_int){
			EM_error(e->lineno, EM_INT);
		 }
		 break;
    }
}
/*
 * only integer expressions expected
 */
void typeOPTL(SymbolTable *venv,OPTL *e){
    if (e->exp == NULL){            
        return;
    }
    if(e->exp->type->kind != Ty_int){
		EM_error(e->lineno, EM_INT);
	}
    typeEXP(venv,e->exp);
}

void typeOPTE(SymbolTable *venv,OPTE *e){
    if (e->state == NULL){
        return;
    }
    typeSTATE(venv,e->state);
}

void typeVARI(SymbolTable *venv,VARI *e){
switch (e->kind) {
    case idK:
        break;
    case vari_expK:
		typeVARI(venv,e->val.e.vari);
		typeEXP(venv,e->val.e.exp);
        break;
    case vari_idK:
		typeVARI(venv,e->val.i.vari);
        break;
    }
}

void typeEXP(SymbolTable *venv,EXP *e){	
 switch (e->kind) {
    case timesK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
    case divK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break; 
    case plusK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;    
    case minusK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;    
    case termK:
		typeTERM(venv,e->val.term);
        break;
    case cmpK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
    case ncmpK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break; 
    case highK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;    
    case lowK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;    
    case higheqK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
    case loweqK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
    case andK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
    case orK:
		typeEXP(venv,e->val.s.left);
		typeEXP(venv,e->val.s.right);
         break;
  } 
} 
/*
 * We check that, if input to a function is given, that it corresponds
 * in number of parameters and types of parameters. The check for 
 * parameter types is done in assemALIST.
 * 
 * Only integer and array types are expected when taking the absolute 
 * value of.
 */
void typeTERM(SymbolTable *venv,TERM *e){
	Ty_field *tempF;
  switch (e->kind) {
    case variK:
		typeVARI(venv,e->val.vari);
        break;
    case id_actK:
		tempF = getSymbol(venv,e->val.s.id)->type->val.record;
		if (e->val.s.alist != NULL) {
			if (tempF->next == NULL){
				EM_error(e->lineno, EM_MANYARGS);
			} else {
			typeALIST(venv, e->val.s.alist, tempF);
			}
		} else {
			if (tempF->next != NULL){
				EM_error(e->lineno, EM_FEWARGS);
			}
		}
        break;
    case numconstK: 
        break;
    case booleanconstK:
        break; 
    case nullK:
        break; 
    case notK:
		typeTERM(venv,e->val.term);
		if(e->val.term->type->kind != Ty_bool){
			EM_error(e->lineno, EM_BOOL);
		}
        break;
    case parK:
		typeEXP(venv,e->val.exp);
        break;
    case barK:
		typeEXP(venv,e->val.exp);
		if(e->val.exp->type->kind != Ty_array && 
e->val.exp->type->kind != Ty_int){
			EM_error(e->lineno, EM_ARRAY);	
			EM_error(e->lineno, EM_INT);	
		}
        break;
  }
}
/*
 * With recursion through the function fields and the input
 * fields, check that their types are equal.
 */
void typeALIST(SymbolTable *venv, ALIST *e, Ty_field *field){
	typeELIST(venv,e->elist);
	
	recursiveCheck(e->elist, field);
}

Ty_field *recursiveCheck(ELIST *e,Ty_field *field){
	Ty_field *myField;
	if (e->kind == elist_expK ){
		myField = recursiveCheck(e->val.s.elist,field->next);
		if (myField->type->kind != e->val.s.exp->type->kind){
			EM_error(e->lineno, EM_TYPEMISS);
		} 
		return field;
	} else {
		if (field->next == NULL){
			EM_error(e->lineno, EM_MANYARGS);
		} else	if (field->next->next != NULL){
			EM_error(e->lineno, EM_FEWARGS);
		} else	if (field->next->type->kind != e->val.exp->type->kind){
			EM_error(e->lineno, EM_TYPEMISS);
		} 
		return field;
	}
}


void typeELIST(SymbolTable *venv,ELIST *e){
  switch (e->kind) {
    case expK:
		typeEXP(venv,e->val.exp);
        break;
    case elist_expK: 
		typeELIST(venv,e->val.s.elist);
		typeEXP(venv,e->val.s.exp);
        break;
  }
}


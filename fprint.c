#include "fprint.h"


FILE *pFile;
char *stringNum;
char *tab;

int fPrint(char *name) {
	
    fprintf(pFile, tab);
	sprintf(stringNum,"%s \n",name);
    fprintf(pFile, stringNum);
	return 0;
}


int fPrint1(char *name, VAL *first) {
	fprintf(pFile, tab);
	fprintf(pFile, name);
	switch (first->kind) {
		case TrueK:
			sprintf(stringNum," $formTrue\n");
			break;
		case FalseK:
			sprintf(stringNum," $formFalse\n");
			break;
		case NewlineK:
			sprintf(stringNum," $form\n");
			break;
		case RegK:
		  printf("in reg \n");
			sprintf(stringNum," T%d \n",first->reg->number);
			break;
		case ConstK:
			sprintf(stringNum," $%d \n",first->Const);
			break;
		case LabelK:
			if (first->label->number != -1) {
				sprintf(stringNum," %s%d \n",first->label->name, 
first->label->number);
			} else {
				sprintf(stringNum," %s \n",first->label->name); 
			}
			break;
		case RRegK:
			if (first->rreg->offset == NULL && first->rreg->isOffset != 1 ) {
				sprintf(stringNum," %s \n",fGetReg(first->rreg));
			} else if (first->rreg->offset == 0 && first->rreg->isOffset == 1){
				sprintf(stringNum," (%s) \n", fGetReg(first->rreg));
				
			} else {
				sprintf(stringNum," %d(%s) \n",first->rreg->offset, 
fGetReg(first->rreg));
			}
			break;
	}
	fprintf(pFile, stringNum);
	return 0;
}


int fPrint2(char *name, VAL *first, VAL *second) {
	
    fprintf(pFile, tab);
	fprintf(pFile, name);
	switch (first->kind) {
		case RegK:
		sprintf(stringNum," T%d,",first->reg->number);
			break;
		case ConstK:
			sprintf(stringNum," $%d,",first->Const);
			break;
		case LabelK:
			if (first->label->number != -1) {
				sprintf(stringNum," %s%d,",first->label->name, 
first->label->number);
			} else {
				sprintf(stringNum," %s,",first->label->name);
			}
			break;
		case RRegK:
			if (first->rreg->offset == NULL && first->rreg->isOffset != 1 ) {
				sprintf(stringNum," %s,",fGetReg(first->rreg));
			} else if (first->rreg->offset == 0 && first->rreg->isOffset == 1){
				sprintf(stringNum," (%s),", fGetReg(first->rreg));
			} else {
				sprintf(stringNum," %d(%s),",first->rreg->offset, 
fGetReg(first->rreg));
			}
			break;
	}
	fprintf(pFile, stringNum);
	switch (second->kind) {
		case RegK:
			sprintf(stringNum,"T%d \n",second->reg->number);
			break;
		case ConstK:
			sprintf(stringNum,"$%d \n",second->Const);
			break;
		case LabelK:
			if (second->label->number != -1) {
				sprintf(stringNum,"%s%d \n",second->label->name, 
second->label->number);
			} else {
				sprintf(stringNum,"%s \n",second->label->name);
			}
			break;
		case RRegK:
			if (second->rreg->offset == NULL && second->rreg->isOffset != 1 ) {
				sprintf(stringNum,"%s \n",fGetReg(second->rreg));
			} else if (second->rreg->offset==0 && second->rreg->isOffset == 1){
				sprintf(stringNum,"(%s) \n", fGetReg(second->rreg));
				
			} else {
				sprintf(stringNum,"%d(%s) \n",second->rreg->offset, 
fGetReg(second->rreg));
			}
			break;
	}
	fprintf(pFile, stringNum);
	return 0;
}

int fPrintL(LABEL *lbl){
	
	if (lbl->number != -1) {
		sprintf(stringNum, "%s%d: \n", lbl->name, lbl->number);
	} else {
		sprintf(stringNum, "%s: \n", lbl->name);
	}
	fprintf(pFile, stringNum);
	return 0;
}

int fPrintM() {
	sprintf(stringNum, ".globl main \n");
	fprintf(pFile, stringNum);
	sprintf(stringNum, "main: \n");
	fprintf(pFile, stringNum);
	return 0;
}

int fPrintStart(ASSEM *list) {
	pFile = NEW(FILE);
	pFile = fopen ("text.s","w");
	stringNum = (char*)malloc(sizeof(char) * 30);
	tab = "        ";
	
	fPrint(".text\n");
	
	LABEL *form = NEW(LABEL);
	form->name = "form";
	form->number = -1;
	fPrintL(form);
	fPrint(".string \"%%d\\n\" \n");
	
	LABEL *false = NEW(LABEL);
	false->name = "formFalse";
	false->number = -1;
	fPrintL(false);
	fPrint(".string \"false\\n\"\n");
	
	LABEL *true = NEW(LABEL);
	true->name = "formTrue";
	true->number = -1;
	fPrintL(true);
	fPrint(".string \"true\\n\"\n");
	
	fPrint(".data\n");
	
	LABEL *heap = NEW(LABEL);
	heap->name = "heap";
	heap->number = -1;
	fPrintL(heap);
	fPrint(".long 0\n");
	
	LABEL *retV = NEW(LABEL);
	retV->name = "retVal";
	retV->number = -1;
	fPrintL(retV);
	fPrint(".long 0\n");	
	
	
	
	
	while (list != NULL) {
		
		switch (list->kind) {
			
			case CallK:
				fPrint1("call", list->left);
				break;
			case MovK:
				fPrint2("movl", list->left, list->right);
				break;
			case DivK:
				fPrint1("divl", list->left);
				break;
			case MulK:
				fPrint2("imul", list->left, list->right);
				break;
			case AddK:
				fPrint2("addl", list->left, list->right);				
				break;
			case SubK:
				fPrint2("subl", list->left, list->right);
				break;
			case CmpK:
				fPrint2("cmp", list->left, list->right);
				break;
			case NotK:
				fPrint1("not", list->left);
				break;
			case AndK:
				fPrint2("and", list->left, list->right);
				break;
			case OrK:
				fPrint2("or", list->left, list->right);
				break;
			case XOrK:
				fPrint2("xor", list->left, list->right);
				break;
			case LShiftK:
				fPrint2("lshift", list->left, list->right);
				break;
			case RShiftK:
				fPrint2("rshift", list->left, list->right);
				break;
				
			case JmpK:
				fPrint1("jmp", list->left);
				break;
			case JmpleqK:
				fPrint1("jle", list->left);
				break;
			case JmpgrqK:
				fPrint1("jge", list->left);
				break;
			case JmpleK:
				fPrint1("jl", list->left);
				break;
			case JmpgrK:
				fPrint1("jg", list->left);
				break;
			case JmpneqK:
				fPrint1("jne", list->left);
				break;
			case JmpeqK:
				fPrint1("je", list->left);
				break;

			case LblK:
				fPrintL(list->label);
				break;
			case PushK:
				fPrint1("pushl", list->left);
				break;
			case PopK:
				fPrint1("popl", list->left);
				break;
			case RetK:
				fPrint("ret");
				break;
			case LeaveK:
				fPrint("leave");
				break;
			case MainK:
				fPrintM();
				break;
			case IncK:
				fPrint1("incl", list->left);
				break;
			case DecK:
				fPrint1("decl", list->left);
				break;
		}	
		list = list->next;
	}
	fclose(pFile);
	return 0;
}

char *fGetReg(REG *reg) {
	
	char *name = (char*)malloc(sizeof(char) * 6);
	switch (reg->kind) {
		case eaxK:
			name = "%%eax";
			break;
		case ebxK:
			name = "%%ebx";
			break;
		case ecxK:
			name = "%%ecx";
			break;
		case edxK:
			name = "%%edx";
			break;
		case espK:
			name = "%%esp";
			break;
		case ebpK:
			name = "%%ebp";
			break;
		case esiK:
			name = "%%esi";
			break;
		case ediK:
			name = "%%edi";
			break;
	}
	return name;
}

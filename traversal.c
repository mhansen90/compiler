#include <stdio.h>
#include "traversal.h"



void traversalFUNC(FUNC *e){
	traversalHEAD(e->head);
	traversalBODY(e->body);
	traversalTAIL(e->tail);
}
void traversalHEAD(HEAD *e){
	if (e->pardl != NULL) {
		traversalPARDL(e->pardl);
	}
	traversalTYPE(e->type);
}
void traversalTAIL(TAIL *e){

}
void traversalTYPE(TYPE *e){
	switch (e->kind) {
		case TidK:

			break;
		case TintK:

			break;
		case TboolK:

			break;
		case TarrayK:

			traversalTYPE(e->val.type);

			break;
		case TrecordK:

			traversalVARDL(e->val.vardl);
			break;
	}
}

void traversalPARDL(PARDL *e){
	traversalVARDL(e->vardl);
}
void traversalVARDL(VARDL *e){
	switch (e->kind) {
		case vartK:
			traversalVART(e->val.vart);
			break;
		case varcl_vartK:
			traversalVARDL(e->val.s.vardl);
			traversalVART(e->val.s.vart);
			break;
	}
}
void traversalVART(VART *e){

	traversalTYPE(e->type);
}
void traversalBODY(BODY *e){

	if (e->decll != NULL){        
		traversalDECL(e->decll);
	}
	traversalSTATEL(e->statel);
}
void traversalDECL(DECL *e){
	if (e->next != NULL){
		traversalDECL(e->next);
	}
	switch (e->kind) {
		case typeK:
			
			traversalTYPE(e->val.s.type);

			
			break;
		case funcK:
			traversalFUNC(e->val.func);
			break;
		case varK:

			
			traversalVARDL(e->val.vardl);

			break;
	}
}
void traversalSTATEL(STATEL *e){
	switch (e->kind) {
		case statel_stateK: 
			traversalSTATEL(e->val.s.statel);
			traversalSTATE(e->val.s.state);
			break;
		case stateK:
			traversalSTATE(e->val.state);
			break;
	}
}
void traversalSTATE(STATE *e){
	switch (e->kind) {
		case returnK: ;
		
		
		traversalEXP(e->val.exp);
		
		break;
		case writeK:
		
			traversalEXP(e->val.exp);
			
			break;
		case allocateK:
			
			traversalVARI(e->val.a.vari);
			if (e->val.a.optl != NULL) {
				traversalOPTL(e->val.a.optl);
			}
			
			break;
		case equalK:
			
			traversalVARI(e->val.e.vari);
			
			traversalEXP(e->val.e.exp);
			
			break;
		case ifK:
			
			traversalEXP(e->val.i.exp);
			
			
			traversalSTATE(e->val.i.state);
			
			if (e->val.i.opte != NULL){
				traversalOPTE(e->val.i.opte);
			}
			break;
		case whileK:
			
			traversalEXP(e->val.w.exp);
			
			traversalSTATE(e->val.w.state);
			
			break;
		case statelK:
			
			traversalSTATEL(e->val.statel);
			
			break;
	}
}

void traversalOPTL(OPTL *e){

	traversalEXP(e->exp);
}

void traversalOPTE(OPTE *e){

	traversalSTATE(e->state);

	
}

void traversalVARI(VARI *e){
	switch (e->kind) {
		case idK:
	
			break;
		case vari_expK:
			traversalVARI(e->val.e.vari);
			
			traversalEXP(e->val.e.exp);
			
			break;
		case vari_idK:
			traversalVARI(e->val.i.vari);
			
			break;
	}
}

void traversalEXP(EXP *e){
	switch (e->type->kind) {
		case Ty_record:
			
			break;
		case Ty_int:
			
			break; 
		case Ty_bool:
			
			break;    
		case Ty_error:
			
			break;
		case Ty_array:
			
			break;
		case Ty_void:
			
			break; 
	} 
} 
void traversalTERM(TERM *e){
	switch (e->kind) {
		case variK:
			traversalVARI(e->val.vari);
			break;
		case id_actK:
			
			if (e->val.s.alist != NULL){
				traversalALIST(e->val.s.alist);
			}
			break;
		case numconstK: 
			
			break;
		case booleanconstK:
			
			break; 
		case nullK:
			
			break; 
		case notK:
			
			traversalTERM(e->val.term);
			break;
		case parK:
			
			traversalEXP(e->val.exp);
			
			break;
		case barK:
			
			traversalEXP(e->val.exp);
			
			break;
	}
}
void traversalALIST(ALIST *e){
	
	traversalELIST(e->elist);
}
void traversalELIST(ELIST *e){
	switch (e->kind) {
		case expK:
			traversalEXP(e->val.exp);
			break;
		case elist_expK: 
			traversalELIST(e->val.s.elist);
			
			traversalEXP(e->val.s.exp);
			break;
	}
}


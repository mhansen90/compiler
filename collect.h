#include "tree.h"
#include "error.h"

SymbolTable *currentTable;

SymbolTable *collectINIT(BODY *e);

void collectFUNC(FUNC *e);
Ty_ty *collectHEAD(HEAD *e);
void collectTAIL(TAIL *e);
Ty_ty *collectTYPE(TYPE *e);

Ty_field *collectPARDL(PARDL *e);
Ty_field *collectVARDL(VARDL *e);
Ty_field *collectVART(VART *e);
void collectBODY(BODY *e);

void collectDECL(DECL *e);

void collectSTATEL(STATEL *e);
void collectSTATE(STATE *e);

void collectOPTL(OPTL *e);
void collectOPTE(OPTE *e);
void collectVARI(VARI *e);

void collectEXP(EXP *e);
void collectTERM(TERM *e);
void collectALIST(ALIST *e);
void collectELIST(ELIST *e);







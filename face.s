        .text
 
form: 
        .string "%d\n" 
 
formFalse: 
        .string "false\n"
 
formTrue: 
        .string "true\n"
 
        .data
 
heap: 
        .long 0
 
retVal: 
        .long 0
 
F0: 
        pushl %ebp 
        movl %esp,%ebp 
        subl $92,%esp 
        pushl %eax 
        movl 12(%ebp),%eax 
        movl %eax,-4(%ebp) 
        popl %eax 
        pushl %eax 
        movl -4(%ebp),%eax 
        movl %eax,-8(%ebp) 
        popl %eax 
        movl $0,%eax 
        cmp -8(%ebp),%eax 
        jne neq3 
        movl $1,-12(%ebp) 
        jmp end4 
neq3: 
        movl $0,-12(%ebp) 
end4: 
        pushl %eax 
        movl -12(%ebp),%eax 
        movl %eax,-8(%ebp) 
        popl %eax 
        pushl %eax 
        movl 12(%ebp),%eax 
        movl %eax,-16(%ebp) 
        popl %eax 
        pushl %eax 
        movl -16(%ebp),%eax 
        movl %eax,-20(%ebp) 
        popl %eax 
        movl $1,%eax 
        cmp -20(%ebp),%eax 
        jne neq5 
        movl $1,-24(%ebp) 
        jmp end6 
neq5: 
        movl $0,-24(%ebp) 
end6: 
        pushl %eax 
        movl -24(%ebp),%eax 
        movl %eax,-20(%ebp) 
        popl %eax 
        movl -8(%ebp),%ecx 
        or -20(%ebp),%ecx 
        movl %ecx,%eax 
        cmp $1,%eax 
        jne elsepart2 
        movl $1,-28(%ebp) 
        movl $1,%eax 
        leave 
        ret 
elsepart2: 
        pushl %eax 
        movl 12(%ebp),%eax 
        movl %eax,-32(%ebp) 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        pushl %ecx 
        pushl %edx 
        pushl %eax 
        movl 12(%ebp),%eax 
        movl %eax,-36(%ebp) 
        popl %eax 
        movl $1,-40(%ebp) 
        pushl %eax 
        movl -36(%ebp),%eax 
        subl -40(%ebp),%eax 
        movl %eax,-36(%ebp) 
        popl %eax 
        pushl -36(%ebp) 
        pushl %eax 
        movl 8(%ebp),%eax 
        movl %eax,-44(%ebp) 
        popl %eax 
        pushl -44(%ebp) 
        call F0 
        addl $8,%esp 
        movl %eax,-48(%ebp) 
        popl %edx 
        popl %ecx 
        popl %ebx 
        popl %eax 
        pushl %eax 
        pushl %ebx 
        movl -32(%ebp),%eax 
        movl -48(%ebp),%ebx 
        imul %ebx,%eax 
        movl %eax,-48(%ebp) 
        popl %ebx 
        popl %eax 
        movl -48(%ebp),%eax 
        leave 
        ret 
endif1: 
Fend0: 
.globl main 
main: 
        movl %esp,retVal 
        pushl %ebp 
        movl %esp,%ebp 
        movl $4000000,(%esp) 
        call malloc 
        movl %eax,heap 
        movl %ebp,%esp 
        subl $132,%esp 
        pushl %eax 
        pushl %ebx 
        pushl %ecx 
        pushl %edx 
        movl $5,-4(%ebp) 
        pushl -4(%ebp) 
        pushl %ebp 
        call F0 
        addl $8,%esp 
        movl %eax,-8(%ebp) 
        popl %edx 
        popl %ecx 
        popl %ebx 
        popl %eax 
        pushl -8(%ebp) 
        pushl $form
        call printf 
        addl $8,%esp 
        movl $0,%eax 
        leave 
        ret 

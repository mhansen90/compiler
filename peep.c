#include "peep.h"

int lines = 0;

/*
Goes trough the whole list from the top and then from the bottom.
On the way down it calls checking if lines can be altered or destroyed. 
On the way up it checks again for lines to destroy.
*/
ASSEM *peepAndDestroy(ASSEM *assem) {
	ASSEM *start = assem;
	ASSEM *temp;
	int again;
	while (assem != NULL) {
		int again = 1;
		while (again == 1) {
			again = checkAssem(assem);
		}
		temp = assem;
		assem = assem->next;
	}
	assem = temp;
	while (assem != NULL) {
		again = checkNeeded(assem);
		temp = assem;
		assem = assem->previous;
		if (again==1) {
			removeAssem(temp, 0);
		}
	}
	printf("%d lines was destroyed\n", lines);
	return start;
}

/*
Checks if the assem is a return so it can delete the next lines if not a label.
Also checks if next lines are important or a value already is in the register.
*/

int checkAssem(ASSEM *assem) {
	if (assem->kind == RetK) {
		if (assem->next != NULL) {
			if (assem->next->kind != LblK) {
				return removeAssem(assem, 1);
			}
		}
	}
	
	if (assem->kind == MovK) {
		if (assem->next != NULL) {
			int a = alreadyInReg(assem, assem->next);
			if (a == 1) {
				return 1;
			}
		}
	}
	return 0;
}

/*
Removes the line, i lines after the current line.
*/
int removeAssem(ASSEM *assem, int i) {
	ASSEM *temp = assem;
	ASSEM *before;
	ASSEM *after;
	while (i != 0) {
		temp = temp->next;
		i--;
	}
	before = temp->previous;
	after = temp->next;
	before->next = after;
	after->previous = before;
	lines++;
	return 1;
}

/*
Checks if 2 registers or constant have same values. If true return 1.
If they have the same register, but the left isn't offset and the right is, 
then it will return 2.
*/
int checkSameVal(VAL *left, VAL *right) {
	if (left->kind != right->kind) {
		return 0;
	}
	if (left->kind == RRegK) {
		if (left->rreg->kind == right->rreg->kind) {
			if (left->rreg->isOffset == right->rreg->isOffset) {
				if (left->rreg->offset == right->rreg->offset) {
					return 1;
				}
			}
			if (left->rreg->isOffset == 0 && left->rreg->offset == NULL) {
				return 2;
			}
		}
	}
	if (left->kind == ConstK) {
		if (left->Const == right->Const) {
			return 1;
		}
	}
	return 0;
}
/*
Return a copied register or constant.
*/
VAL *copyVal(VAL *val) {
	VAL *a = NEW(VAL);
	a->kind = val->kind;
	a->Const = val->Const;
	if (val->rreg != NULL) {
		memcpy(a->rreg, val->rreg, sizeof(REG));
	}
	return a;
}

/*
Checks if a register is overwritten before it is used, and therefore can be
deleted.
*/
int checkNeeded(ASSEM *assem) {
	VAL *val;
	VAL *val2;
	ASSEM *temp = assem;
	int a;
	if (assem->kind == MovK || assem->kind == AddK || assem->kind == SubK || 
assem->kind == MulK || assem->kind == AndK || assem->kind == OrK || 
assem->kind == XOrK) {
		val = assem->right;
	} else if (assem->kind == IncK || assem->kind == DecK || 
assem->kind == NotK) {
		val = assem->left;
	} else if (assem->kind == DivK) {
		val = makeVALrreg( );
		val->rreg->kind == eaxK;
	} else {
		return 0;
	}
	while (1==1) {
		if (temp->next != NULL) {
			temp = temp->next;
		} else {
			return 1;
		}
		if (temp->kind == MovK || temp->kind == AddK || temp->kind == SubK || 
temp->kind == MulK || temp->kind == AndK || temp->kind == OrK || 
temp->kind == XOrK || temp->kind == CmpK || temp->kind == LShiftK || 
temp->kind == RShiftK ) {
			a = checkSameVal(val, temp->left);
			if (a > 0) {
				return 0;
			}
			a = checkSameVal(val, temp->right);
			if (a > 0) {
				if (temp->kind == MovK && a == 1) {
					return 1;
				}
				return 0;
			}
		} else if (temp->kind == IncK || temp->kind == DecK || 
temp->kind == NotK) {
			a = checkSameVal(val, temp->left);
			if (a > 0) {
				return 0;
			}
		} else if (temp->kind == DivK) {
			return 0;
		} else if (temp->kind == LblK || temp->kind == MainK ) {
			a = 0;
		} else {
			return 0;		
		}
	}
}
/*
Checks if a movl is written from one destination to another and aftwerwards
another movl are moving them without they have changed.
*/

int alreadyInReg(ASSEM *assem, ASSEM *next) {
	if (next->kind == MovK) {
		int a = checkSameVal(assem->left, next->left);
		int b = checkSameVal(assem->right, next->right);
		if (a == 1 && b == 1) {
			return removeAssem(next, 0);
		}
		a = checkSameVal(assem->left, next->right);
		b = checkSameVal(assem->right, next->left);
		if (a == 1 && b == 1) {
			return removeAssem(next, 0);
		}
		if (b == 1) {
			if (assem->left->kind == ConstK) {
				next->left = copyVal(assem->left);
				return 1; 
			}
		}
		a = checkSameVal(next->right, assem->right);
		b = checkSameVal(next->right, assem->left);
		if (a == 0 && b == 0 && next->next != NULL) {
			return alreadyInReg(assem, next->next);
		}
	}
	return 0;
}


#include <stdio.h>
#include "collect.h"
#include "mem.h"

int length;
int parnum;

/*
Starts collect by making a rootTable
*/

SymbolTable *collectINIT(BODY *e){
	SymbolTable *rootTable = initSymbolTable();
	currentTable = rootTable;
	collectBODY(e);
	return rootTable;
}	

/*
A function is called, it is scoped and is inserted in the scope outside
the one it makes, so the function can be called.
*/

void collectFUNC(FUNC *e){	
	SymbolTable *newTable = scopeSymbolTable(currentTable);
	currentTable = newTable;
	e->table = currentTable;
	
	Ty_ty *retur = collectHEAD(e->head);
	
	collectBODY(e->body);
	collectTAIL(e->tail);
	currentTable = currentTable->next;
	
	putSymbol(currentTable, e->head->id, retur,1);
}

/*
Inserts the parameters inside the table
*/
Ty_ty *collectHEAD(HEAD *e){
	Ty_field *myField = Ty_Field(e->id, collectTYPE(e->type));
	Ty_ty *myRecord = Ty_Record(myField);
	SYMBOL *tmp;
	if (e->pardl != NULL) {
		Ty_field *temp = collectPARDL(e->pardl);
		myRecord->val.record->next = temp;
		while (temp != NULL) {
			putSymbol(currentTable, temp->name, temp->type, 1);
			temp = temp->next;	
		}
	}
	return myRecord;
}
void collectTAIL(TAIL *e){
}

/*
Returns the type. Will go through all arrays and records to get the exact type.
*/
Ty_ty *collectTYPE(TYPE *e){
	Ty_ty *type = NEW(Ty_ty);
	SYMBOL *temp; 
	switch (e->kind) {
		case TidK:
			temp = getSymbol(currentTable, e->val.id);
			if (temp == NULL) {
				EM_error3(e->lineno, EM_MISSID, e->val.id);
			} else {
				type = temp->type;
			}
			return type; 
		case TintK:
			type = Ty_Int();
			return type;
		case TboolK:
			type = Ty_Bool();
			return type;
		case TarrayK:
			type = Ty_Array(collectTYPE(e->val.type));
			return type;
		case TrecordK:
			type = Ty_Record(collectVARDL(e->val.vardl));
			return type;
	}
}
/*
Will return a linked list of variables or nothing.
*/
Ty_field *collectPARDL(PARDL *e){
	parnum = 0;
	Ty_field *t = collectVARDL(e->vardl);
	e->length = parnum;
	return t;
}

/*
Will return a linked list of collected variables.
*/
Ty_field *collectVARDL(VARDL *e){
	Ty_field *field = NEW(Ty_field);
	Ty_field *list;
	Ty_field *list2;
	switch (e->kind) {
		case vartK:
			field = collectVART(e->val.vart);
			return field;
		case varcl_vartK:
			list = collectVARDL(e->val.s.vardl);
			list2 = list;
			field = collectVART(e->val.s.vart);
			while (list2->next != NULL) {
				list2 = list2->next;
			}
			list2->next = field;
			return list;
	}
}
/*
Will return a variable.
*/
Ty_field *collectVART(VART *e){
	
	Ty_field *field = NEW(Ty_field);
	field->name = e->id;
	field->type = collectTYPE(e->type);
	parnum = parnum+1;
	return field;
}
void collectBODY(BODY *e){
	if (e->decll != NULL){        
		collectDECL(e->decll);
	}
	collectSTATEL(e->statel);
}
/*
Will insert types and variables into the symboltable.
*/
void collectDECL(DECL *e){
	if (e->next != NULL){
		collectDECL(e->next);
	}
	Ty_field *list;
	switch (e->kind) {
		case typeK:
			putSymbol(currentTable, e->val.s.id, 
			collectTYPE(e->val.s.type),0);
			break;
		case funcK:
			collectFUNC(e->val.func);
			break;
		case varK:
			list = collectVARDL(e->val.vardl);
			while (list != NULL) {
				putSymbol(currentTable, list->name, list->type,1);
				list = list->next;	
			}
			break;
	}
}
void collectSTATEL(STATEL *e){
	switch (e->kind) {
		case statel_stateK: 
			collectSTATEL(e->val.s.statel);
			collectSTATE(e->val.s.state);
			break;
		case stateK:
			collectSTATE(e->val.state);
			break;
	}
}
void collectSTATE(STATE *e){
	SymbolTable *newTable;
	Ty_field *field;
	switch (e->kind) {
		case returnK: 
			collectEXP(e->val.exp);
			break;
		case writeK:
			collectEXP(e->val.exp);
			break;
		case allocateK:
			collectVARI(e->val.a.vari);
			if (e->val.a.optl != NULL) {
				collectOPTL(e->val.a.optl);
			}
			break;
		case equalK:
			collectVARI(e->val.e.vari);
			collectEXP(e->val.e.exp);
			break;
		case ifK: 
			collectEXP(e->val.i.exp);
			collectSTATE(e->val.i.state);
			if (e->val.i.opte != NULL){
				collectOPTE(e->val.i.opte);
			}
			break;
		case whileK: ;
			collectEXP(e->val.w.exp);
			collectSTATE(e->val.w.state);
			break;
		case statelK:
			collectSTATEL(e->val.statel);
			break;
		case breakK:
			break;
		case continueK:
			break;
		case forK:
			newTable = scopeSymbolTable(currentTable);
			currentTable = newTable;
			e->table = currentTable;
			field = collectVART(e->val.f.vart);
			putSymbol(currentTable, field->name, field->type, 1);
			collectSTATE(e->val.f.state1);
			collectEXP(e->val.f.exp);
			collectSTATE(e->val.f.state2);
			collectSTATE(e->val.f.state3);
			currentTable = currentTable->next;
		case incK:
			 collectVARI(e->val.vari);
			 break;
		case decK:
			 collectVARI(e->val.vari);
			 break;	
	}
}

void collectOPTL(OPTL *e){
	if (e->exp == NULL){            
		return;
	}
	collectEXP(e->exp);
}

void collectOPTE(OPTE *e){
	if (e->state == NULL){
		return;
	}
	collectSTATE(e->state);
}

void collectVARI(VARI *e){
	switch (e->kind) {
		case idK:
			break;
		case vari_expK:
			collectVARI(e->val.e.vari);
			collectEXP(e->val.e.exp);
			break;
		case vari_idK:
			collectVARI(e->val.i.vari);
			break;
	}
}
void collectEXP(EXP *e){
	switch (e->kind) {
		case timesK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
		case divK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break; 
		case plusK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;    
		case minusK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;    
		case termK:
			collectTERM(e->val.term);
			break;
		case cmpK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
		case ncmpK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break; 
		case highK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;    
		case lowK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;    
		case higheqK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
		case loweqK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
		case andK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
		case orK:
			collectEXP(e->val.s.left);
			collectEXP(e->val.s.right);
			break;
	} 
} 
void collectTERM(TERM *e){
	switch (e->kind) {
		case variK:
			collectVARI(e->val.vari);
			break;
		case id_actK:
			if (e->val.s.alist != NULL) {
				collectALIST(e->val.s.alist);
			}
			break;
		case numconstK:
			break;
		case booleanconstK:
			if (e->val.num==1) {
			} else {
			}
			
			break; 
		case nullK:
			break; 
		case notK:
			collectTERM(e->val.term);
			break;
		case parK:
			collectEXP(e->val.exp);
			break;
		case barK:
			collectEXP(e->val.exp);
			break;
	}
}
void collectALIST(ALIST *e){
	length = 0;
	collectELIST(e->elist);
	e->length = length;
}
void collectELIST(ELIST *e){
	switch (e->kind) {
		case expK:
			collectEXP(e->val.exp);
			length+=1;
			break;
		case elist_expK: 
			collectELIST(e->val.s.elist);
			length+=1;
			collectEXP(e->val.s.exp);
			break;
	}
}

